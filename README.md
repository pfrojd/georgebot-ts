## Conversion TODO:

DONE:

- Talisman
  - talisman
  - talisman latest
  - talisman (add)
- Quote
  - quote
  - latest
  - random
  - edit
  - remove
- Memes
  - they-call-me
  - wont-like-this
  - roll
- Tasks
- Build scripts

UNFINISHED:

- InternList-related

### TODO:

- Check imported images for duplicates (pixelmatch?)
- Add new columns to screenshots
  - width
  - height
  - imported y/n
  - original_url (only for new ones)
- Run through all images to get image sizes (buffer-image-size?)
- Run through all imported images to get URLs
- Re-run the fetch-script (only for the "old-new" screenshots)
- Figure out if the import script didn't import the newest screenshots.
- Make a backup (somewhere) of all attachments in channel, as JSON.
- Adjust image-download script to use less delay

Oh no, I didn't filter all the attachments that were made by the bot, every single meme image is here..

- Fix the import flag, perhaps fix so the !cursed command only takes non-imported ones?
