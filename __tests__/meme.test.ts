import { selectMemeTemplate } from "../lib/meme";
import dotenv from "dotenv";

dotenv.config({ path: "__tests__/.env.test" });

it("can pick drake", async () => {
  const result = selectMemeTemplate(["1"]);
  expect(result).not.toBe(null);
  expect(result.type).toBe("DRAKE");
});

it("can pick pooh", async () => {
  const result = selectMemeTemplate(["2"]);
  expect(result).not.toBe(null);
  expect(result.type).toBe("POOH");
});

it("can pick seagull", async () => {
  const result = selectMemeTemplate(["3"]);
  expect(result).not.toBe(null);
  expect(result.type).toBe("SEAGULL");
});

it("can pick mind", async () => {
  const result = selectMemeTemplate(["4"]);
  expect(result).not.toBe(null);
  expect(result.type).toBe("MIND");
});

it("can pick wolverine", async () => {
  const result = selectMemeTemplate(["5"]);
  expect(result).not.toBe(null);
  expect(result.type).toBe("WOLVERINE");
});

it("can pick bobby", async () => {
  const result = selectMemeTemplate(["6"]);
  expect(result).not.toBe(null);
  expect(result.type).toBe("BOBBY");
});

it("can pick finally", async () => {
  const result = selectMemeTemplate(["7"]);
  expect(result).not.toBe(null);
  expect(result.type).toBe("FINALLY");
});

it("should have random results", async () => {
  const iterations = 1000;
  let current = 0;
  const seen = new Set();
  while (current <= iterations) {
    const result = selectMemeTemplate([]);
    seen.add(result.type);
    current++;
  }

  const listOfMemes = Array.from(seen);

  expect(listOfMemes).toContain("DRAKE");
  expect(listOfMemes).toContain("POOH");
  expect(listOfMemes).toContain("SEAGULL");
  expect(listOfMemes).toContain("MIND");
  expect(listOfMemes).toContain("WOLVERINE");
  expect(listOfMemes).toContain("BOBBY");
  expect(listOfMemes).toContain("FINALLY");
});
