import dotenv from "dotenv";

dotenv.config({ path: "__tests__/.env.test" });

import {
  getAllQuotes,
  getQuoteById,
  getRandomQuote,
  getLatestQuote,
  searchForQuote,
  getActuallyRandomQuote,
  getLowestViewOfQuotes,
} from "../lib/db/quote";

it("can get all quotes", async () => {
  expect.assertions(1);
  const quotes = await getAllQuotes();
  expect(quotes.length).toBeGreaterThan(1);
});

it("can get quote by id", async () => {
  expect.assertions(1);
  const quote = await getQuoteById(59);
  expect(quote).not.toBeFalsy();
});

it("can get random quote", async () => {
  expect.assertions(1);
  const quote = await getRandomQuote();
  expect(quote).not.toBeFalsy();
});

it("can get latest quote", async () => {
  expect.assertions(1);
  const quote = await getLatestQuote();
  expect(quote).not.toBeFalsy();
});

it("can search for quote quote", async () => {
  expect.assertions(2);
  const quote = await searchForQuote("bee");
  expect(quote).not.toBeFalsy();
  expect(quote?.content).toMatch("bee");
});

it.skip("can get lowest view count", async () => {
  expect.assertions(1);
  const { min } = await getLowestViewOfQuotes();
  console.log(min);
  expect(min).not.toBeFalsy();
});

it.only("can get random quote and increase count", async () => {
  expect.assertions(3);
  const quote = await getActuallyRandomQuote();
  expect(quote).not.toBeFalsy();
  const { id, views } = quote;
  const { min } = await getLowestViewOfQuotes();
  expect(min).toBe(views);
  const _quote = await getQuoteById(id);
  expect(_quote?.views).toBe(views);
});
