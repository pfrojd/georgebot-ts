import dotenv from "dotenv";

dotenv.config({ path: "__tests__/.env.test" });

import {
  getListOfGames,
  getGame,
  getRandomGame,
  getListOfNonPlayedGames,
  getListOfReleasedGames,
  getListOfPlayedGames,
  getGamesWithFilter,
  getAllGenres,
  getRandomSteamLibraryGame,
} from "../lib/db/steam";

// it("can get all games", async () => {
//   expect.assertions(1);
//   const games = await getListOfGames();
//   expect(games.length).toBeGreaterThan(1);
// });

// it("can get a game", async () => {
//   expect.assertions(2);
//   const game = await getGame(238960);
//   expect(game).toBeTruthy();
//   expect(game?.steam_id).toEqual(238960);
// });

// it("can get random game", async () => {
//   expect.assertions(1);
//   const game = await getRandomGame();
//   expect(game).toBeTruthy();
// });

// it("can get list of non played games", async () => {
//   const games = await getListOfNonPlayedGames();
//   games.forEach((game) => {
//     expect(game.played).toBeFalsy();
//   });
// });

// it("can get list of released games", async () => {
//   const games = await getListOfReleasedGames();
//   games.forEach((game) => {
//     expect(game.is_released).toBeTruthy();
//   });
// });

// it("can get list of played games", async () => {
//   const games = await getListOfPlayedGames();
//   games.forEach((game) => {
//     expect(game.played).toBeTruthy();
//   });
// });

// it("can get filtered list of games", async () => {
//   const filter = {
//     categories: ["Single-player"],
//     genres: ["RPG"],
//   };
//   const games = await getGamesWithFilter(filter);
//   expect(games?.length).toBeGreaterThan(1);
// });

// it("can get a random steam library game", async () => {
//   expect.assertions(1);
//   const game = await getRandomSteamLibraryGame();
//   expect(game).toBeTruthy();
// });
