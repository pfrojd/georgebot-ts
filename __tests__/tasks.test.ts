import dotenv from "dotenv";
dotenv.config({ path: "__tests__/.env.test" });
import { updateSteamLibrary } from "../lib/db/steam";
import { updateAllStocks } from "../lib/stock";

it("can run the update steam library task", async () => {
  await updateSteamLibrary();
}, 600000);

it.only("can run the update stock prices task", async () => {
  await updateAllStocks();
}, 600000);
