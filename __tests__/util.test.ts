import dotenv from "dotenv";
dotenv.config({ path: "__tests__/.env.test" });

import { substituteWord, substituteWords } from "../lib/util";

import { getAllReplacementWords } from "../lib/db/replacement_word";
import { getStockSummary } from "../lib/stock";
import { getRandomFactForDate, getRandomFactForToday } from "../lib/db/fact";

it("can substitute words", async () => {
  const sentence = "AirGun";
  const replacementWords = await getAllReplacementWords();
  const wordArray = replacementWords.map((rw) => rw.word);

  const newSentence = substituteWord(sentence, wordArray);
});

it("can add multiple substitute words", async () => {
  const sentence = "1993 - A short sentence";
  const replacementWords = await getAllReplacementWords();
  const wordArray = replacementWords.map((rw) => rw.word);

  const newSentence = substituteWords(sentence, wordArray);
  console.log(newSentence);
});

it("can add multiple substitute words", async () => {
  const sentence = "1997 -      A";
  const replacementWords = await getAllReplacementWords();
  const wordArray = replacementWords.map((rw) => rw.word);

  const newSentence = substituteWords(sentence, wordArray);
  console.log(newSentence);
});

it("should see if we can find some new stopwords", async () => {
  let iterations = 0;

  const replacementWords = await getAllReplacementWords();
  const wordArray = replacementWords.map((rw) => rw.word);

  while (iterations <= 500) {
    const fact = await getRandomFactForToday();
    substituteWords(fact.content, wordArray);

    iterations++;
  }
});

it("can remember how to modulo", async () => {
  const sentence = "A mighty fine sentence";
  const replacementWords = await getAllReplacementWords();
  const words = replacementWords.map((rw) => rw.word);

  const newSentence = substituteWords(sentence, words);
  console.log(newSentence);
});

it("should always replace a word", () => {
  const replacementWords = ["REPLACEMENT"];
  const sentence = "Eidols a long sentence is 10923 asd womething sa msoe";

  const newSentence = substituteWords(sentence, replacementWords, 6);
  expect(newSentence).toContain("REPLACEMENT");
});

it("should always replace a word", () => {
  const replacementWords = ["REPLACEMENT"];
  const sentence = "What?! Yeah. Oh, yeah. Yeah.";

  const newSentence = substituteWords(sentence, replacementWords, 6);
  console.log(newSentence);
  expect(newSentence).toContain("REPLACEMENT");
});
