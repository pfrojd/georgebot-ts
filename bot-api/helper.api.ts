import { ServerResponse } from "http";

export function sleep(ms: number): any {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function parseAndValidateId(id: string): any {
  try {
    const _id = Number.parseInt(id);
    if (!Number.isNaN(_id)) {
      return _id;
    }
  } catch (e) {
    return null;
  }
}

export function Ok(
  res: ServerResponse,
  message: string,
  obj: Record<any, any>
): any {
  const response = JSON.stringify({
    status: 200,
    message: message,
    data: obj,
    error: null,
  });

  res.setHeader("Content-Type", "application/json; charset=utf-8");
  res.statusCode = 200;
  res.end(response);
}

export function badRequest(
  res: ServerResponse,
  incorrectProperty: string
): any {
  const response = JSON.stringify({
    status: 400,
    message: `[${incorrectProperty}] is invalid for this request`,
  });

  res.setHeader("Content-Type", "application/json; charset=utf-8");
  res.statusCode = 400;
  res.end(response);
}

export function notFound(res: ServerResponse, resourceId: string): any {
  const response = JSON.stringify({
    status: 404,
    message: `Resource with id [${resourceId}] does not exist`,
  });

  res.setHeader("Content-Type", "application/json; charset=utf-8");
  res.statusCode = 404;
  res.end(response);
}

export function notAuthenticated(res: ServerResponse): any {
  const response = JSON.stringify({
    status: 401,
    message: `Missing token in [Authorization] header`,
  });

  res.setHeader("Content-Type", "application/json; charset=utf-8");
  res.statusCode = 401;
  res.end(response);
}
