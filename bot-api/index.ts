import * as dotenv from "dotenv";
dotenv.config();

import polka from "polka";
import {
  authenticationHandler,
  registerTimeHandler,
  registerUsageHandler,
} from "./middleware.api";
import quoteRouter from "./quote.api";
import screenshotRouter from "./screenshot.api";

const port = process.env.WEB_API_PORT || 3001;

export interface APIResponse {
  status: number;
  message: string;
  data?: string;
  error: any;
}

const app = polka();

// @ts-ignore
app.use(registerTimeHandler);
// @ts-ignore
app.use(authenticationHandler);
// @ts-ignore
app.use("/quote", quoteRouter);
// @ts-ignore
app.use("/screenshot", screenshotRouter);

app.listen(port, (err: Error) => {
  if (err) {
    throw err;
  }
  console.log("Georgebotts API running");
});
