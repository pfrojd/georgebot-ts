import { authenticate, reportTokenUsage } from "../lib/db/token";
import { IncomingMessage, ServerResponse } from "http";
import { notAuthenticated } from "./helper.api";

export async function authenticationHandler(
  req: IncomingMessage,
  res: ServerResponse,
  next: any
) {
  const token = req.headers["authorization"];
  if (!token) {
    return notAuthenticated(res);
  }

  const authenticated = await authenticate(token);
  if (!authenticated) {
    return notAuthenticated(res);
  }

  next();
}

export async function registerUsageHandler(req: IncomingMessage) {
  const now = Date.now();
  // @ts-ignore
  console.log(`Finished request in ${now - req.request_started}ms`);
  const token = req.headers["authorization"];
  if (token && !Array.isArray(token)) {
    await reportTokenUsage(token);
  }
}

export async function registerTimeHandler(
  req: IncomingMessage,
  res: ServerResponse,
  next: any
) {
  // @ts-ignore
  req.request_started = Date.now();
  next();
}
