import { IncomingMessage, ServerResponse } from "http";
import polka from "polka";
import {
  getActuallyRandomQuote,
  getAllQuotes,
  getLatestQuote,
  getQuoteById,
  getRandomQuote,
} from "../lib/db/quote";
import { parseAndValidateId, badRequest, notFound, Ok } from "./helper.api";
import { registerUsageHandler } from "./middleware.api";

const app = polka();

app.get("/:id", async (req: IncomingMessage, res: ServerResponse) => {
  // @ts-ignore
  const { id } = req.params;
  const quoteId = parseAndValidateId(id);
  if (!quoteId) {
    return badRequest(res, "id");
  }

  const quote = await getQuoteById(quoteId);
  if (!quote) {
    return notFound(res, id);
  }

  Ok(res, `Fetched resource with id ${quoteId}!`, quote);
  await registerUsageHandler(req);
});

app.get("/", async (req: IncomingMessage, res: ServerResponse) => {
  const quotes = await getAllQuotes();
  Ok(res, "Fetched all quotes!", quotes);
  await registerUsageHandler(req);
});

app.get("/random", async (req: IncomingMessage, res: ServerResponse) => {
  const quote = await getActuallyRandomQuote();
  Ok(res, "Fetched random quote!", quote);
  await registerUsageHandler(req);
});

app.get("/latest", async (req: IncomingMessage, res: ServerResponse) => {
  const quote = await getLatestQuote();
  Ok(res, "Fetched latest quote!", quote);
  await registerUsageHandler(req);
});

export default app;
