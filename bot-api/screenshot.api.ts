import polka from "polka";
import { IncomingMessage, ServerResponse } from "http";
import { getRandomScreenshot } from "../lib/db/screenshots";
import { Ok } from "./helper.api";
import { registerUsageHandler } from "./middleware.api";

const app = polka();

app.get("/random", async (req: IncomingMessage, res: ServerResponse) => {
  const screenshot = await getRandomScreenshot();
  Ok(res, "Fetched random screenshot!", screenshot);
  await registerUsageHandler(req);
});

export default app;
