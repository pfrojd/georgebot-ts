import polka from "polka";
import { IncomingMessage, ServerResponse } from "http";
import { getRandomImage } from "../lib/db/talisman";
import { Ok } from "./helper.api";
import { registerUsageHandler } from "./middleware.api";

const app = polka();

app.get("/random", async (req: IncomingMessage, res: ServerResponse) => {
  const talisman = await getRandomImage();
  Ok(res, "Fetched random talisman meme!", talisman);
  await registerUsageHandler(req);
});

export default app;
