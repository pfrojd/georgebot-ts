import { fetchImage } from "../lib/image";
import * as Discord from "discord.js";
import path from "path";
import fs from "fs";
import { nanoid } from "nanoid";
import { insertScreenshot } from "../lib/db/screenshots";
import { handlePossibleTalismanMessage } from "./commands/talisman.command";

const { writeFile, access, unlink } = fs.promises;

const imageFolder = process.env.IMAGE_PATH || process.cwd();
const imagePath = path.resolve(imageFolder, "screenshots");
const channelId = process.env.DISCORD_SCREENSHOT_CHANNEL;

const buildPath = (name: string) => {
  const ext = path.extname(name);
  return `${imagePath}${path.sep}${nanoid()}${ext}`;
};

const fileExists = async (filePath: string) => {
  try {
    await access(filePath, fs.constants.F_OK);
    return true;
  } catch (e) {
    return false;
  }
};

export async function writeFileToDisk(
  buffer: Buffer,
  name: string
): Promise<string> {
  const filePath = buildPath(name);
  await writeFile(filePath, buffer);
  return filePath;
}

export async function removeFileFromDisk(filePath: string): Promise<void> {
  await unlink(filePath);
}

export interface Attachment {
  url: string;
  name: string;
}

export interface AttachmentMessage {
  id: string;
  username: string;
  content: string;
}

export async function removeScreenshot(fileName: string): Promise<void> {
  try {
    const exists = await fileExists(fileName);
    if (!exists) {
      console.log("File does not exist in image folder");
    }

    await removeFileFromDisk(fileName);
    await removeScreenshot(fileName);
  } catch (e) {
    console.error(e);
    console.error("Failed to remove screenshot", fileName);
  }
}

export async function saveScreenshot(
  attachment: Discord.MessageAttachment,
  message: AttachmentMessage
): Promise<void> {
  let imageBuffer;
  let filePath;
  const extension = path.extname(attachment.url);

  if (![".jpg", ".jpeg", ".png"].includes(extension.toLowerCase())) {
    console.error("Attachment has bad extension", attachment.url);
    return;
  }
  try {
    imageBuffer = await fetchImage(attachment.url);
  } catch (e) {
    console.error(e);
    console.error("Failed to fetch image");
    return;
  }

  if (!imageBuffer || !attachment) {
    console.error("No imagebuffer found, fetching image probably failed");
    return;
  }

  try {
    filePath = await writeFileToDisk(imageBuffer, attachment.name || "temp");
  } catch (e) {
    console.error(e);
    console.error("Failed to write image to disk");
    return;
  }

  try {
    if (!filePath) {
      console.error("No filepath was found");
      return;
    }

    await insertScreenshot({
      discord_id: message.id,
      discord_username: message.username,
      path: filePath,
      comment: message.content,
      width: attachment.width || 0,
      height: attachment.height || 0,
      original_url: attachment.url,
    });
  } catch (e) {
    console.error(e);
    console.error("Failed to save screenshot to db", filePath);
  }
}

export const handleIncomingAttachment = async (
  message: Discord.Message
): Promise<void> => {
  const { content, channel, attachments } = message;

  const isTalisman = content.toLowerCase().includes("talisman");
  if (isTalisman && attachments.size > 0) {
    await handlePossibleTalismanMessage(message);
    return;
  }

  if (!channelId) {
    console.error("Not configured properly to mine attachments");
    return;
  }

  if (channel.id != channelId) {
    console.error("Attachment found in wrong configured channel", channel.id);
    return;
  }

  const attachment = attachments.first();
  if (!attachment) {
    return;
  }

  await saveScreenshot(attachment, {
    id: message.id,
    username: message.author.username,
    content: message.content,
  });
};
