import { insertToken } from "../../lib/db/token";
import Discord from "discord.js";

export async function commandGetToken(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  try {
    const comment = args[0];
    const { id } = message.author;
    const token = await insertToken(id, comment);
    message.author.send(`Your token is: ${token.token}`);
  } catch (e) {
    console.error(e);
    message.channel.send("Something went wrong with this command :(");
  }
}
