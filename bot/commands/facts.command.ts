import Discord from "discord.js";
import { getRandomFactForDate } from "../../lib/db/fact";
import { getHistoryFact } from "../../bot/helper";
import { getAllReplacementWords } from "../../lib/db/replacement_word";
import { substituteWords } from "../../lib/util";

export async function commandGetRandomTodaysHistory(
  message: Discord.Message
): Promise<void> {
  try {
    const today = new Date();

    const fact = await getRandomFactForDate(
      today.getDate(),
      today.getMonth() + 1
    );
    if (fact) {
      message.channel.send(getHistoryFact(fact));
    } else {
      console.error(today.getDay(), today.getMonth() + 1);
      throw new Error("Couldnt find a fact for this day");
    }
  } catch (e) {
    message.channel.send("Something went wrong with this command :(");
  }
}

export async function commandGetNotToday(
  message: Discord.Message
): Promise<void> {
  try {
    const today = new Date();

    const fact = await getRandomFactForDate(
      today.getDate(),
      today.getMonth() + 1
    );
    if (fact) {
      const words = await getAllReplacementWords();
      const notTodayFact = substituteWords(
        fact.content,
        words.map((w) => w.word)
      );
      fact.message = notTodayFact;

      message.channel.send(getHistoryFact(fact));
    } else {
      console.error(today.getDay(), today.getMonth() + 1);
      throw new Error("Couldnt find a fact for this day");
    }
  } catch (e) {
    message.channel.send("Something went wrong with this command :(");
  }
}
