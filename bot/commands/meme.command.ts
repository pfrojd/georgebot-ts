import { getStockPrice } from "../../bot/helper";
import Discord from "discord.js";
import * as Random from "random-js";
import fin from "yahoo-finance2";
import fs from "fs";
import path from "path";
import { getRandomSteamLibraryGame } from "../../lib/db/steam";
import {
  getRandomAscendancy,
  getRandomKeystones,
  getRandomSkillGems,
} from "../../lib/poe";
import { getSteamGame, getSteamLink } from "../../lib/steam";
import { createWhatAWeekCanvas } from "../../lib/canvas/week";
import { getRandomCage } from "../../lib/db/cage";
import { getRandomMeme } from "../../lib/meme";
import { askDuckDuckGo } from "../../lib/ask";
import {
  addReplacementWord,
  disableReplacementWord,
  getAllReplacementWords,
} from "lib/db/replacement_word";

const engine = Random.nodeCrypto;

const { readFile } = fs.promises;
const imageFolder = process.env.IMAGE_PATH || process.cwd();

interface RollParticipant {
  id: string;
  username: string;
  roll: number;
}

export async function commandRollDice(message: Discord.Message): Promise<void> {
  const rnd = Random.integer(1, 100);
  let rolls: RollParticipant[];

  if (message.mentions.users.size) {
    rolls = message.mentions.users.map((user) => {
      return {
        id: user.id,
        username: user.username,
        roll: rnd(engine),
      };
    });
    const msg = getRollFormatting(rolls);
    message.channel.send(msg);
    return;
  }
  const voiceChannels = message?.guild?.channels.cache.filter(
    (chan) => chan.type === "voice"
  );

  if (voiceChannels) {
    const voiceChannelMembersWithAuthor = voiceChannels
      .map((chan) => {
        if (chan.members.size > 0) {
          // Found a voice channel with members
          const authorExists = chan.members.get(message.author.id);
          if (authorExists) {
            return chan;
          }
        }
        return null;
      })
      .filter(Boolean)[0];

    if (
      !voiceChannelMembersWithAuthor ||
      voiceChannelMembersWithAuthor.members.size <= 0
    ) {
      return;
    }

    rolls = voiceChannelMembersWithAuthor.members.map((user) => {
      return {
        id: user.id,
        username: user.user.username,
        roll: rnd(engine),
      };
    });

    const msg = getRollFormatting(rolls);
    message.channel.send(msg);
    return;
  }
}

export const getRollFormatting = (rolls: RollParticipant[]): string => {
  const rows = rolls.map(
    (item: RollParticipant) => `> ${item.username} rolled ${item.roll}`
  );
  rows.unshift("Rolling 1 - 100");
  const winner = rolls.reduce(
    (prev: RollParticipant, current: RollParticipant) => {
      return prev.roll > current.roll ? prev : current;
    }
  );
  rows.push(`<@${winner.id}> won! :partying_face:`);
  return rows.join("\n");
};

export async function commandWontLikeThis(
  message: Discord.Message
): Promise<void> {
  const game = await getRandomSteamLibraryGame();
  console.log(game);
  const msg = `I bet Ryan won't like **${game.name}** - <${getSteamLink(
    game.steam_id
  )}>`;
  message.channel.send(msg);
}

export async function commandRandomPoeBuild(
  message: Discord.Message
): Promise<void> {
  const ascendancy = await getRandomAscendancy();
  const keystones = await getRandomKeystones();
  const gems = await getRandomSkillGems();

  message.channel.send(`
  Ascendancy: ${ascendancy.id}
Keystones: ${keystones.map((key: any) => key.name).join(" - ")}
Skill gems: ${gems.map((key: any) => key.display_name).join(" - ")}`);
}

export async function commandGetCheggStock(
  message: Discord.Message
): Promise<void> {
  const results = await fin.quoteSummary("CHGG", {
    modules: ["price", "summaryDetail"],
  });
  const stock = getStockPrice(results, ":egg:");
  if (!stock) {
    await message.channel.send("Could not get chegg prices :(");
    return;
  }
  await message.channel.send(stock);
}

export async function commandGetStock(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  try {
    if (!args || !args.length) {
      message.channel.send("You have to pass the identifier for the stock.");
      return;
    }
    const symbol = args[0];
    let result;
    try {
      result = await fin.search(symbol, {
        lang: "en-US",
        region: "US",
        quotesCount: 2,
        newsCount: 0,
        enableFuzzyQuery: true,
      });
    } catch (e) {
      if ((e as any)?.name != "FailedYahooValidationError") {
        throw e;
      }
    }

    if (!result || result.count <= 0 || !result.quotes.length) {
      message.channel.send("Couldnt find a stock with that name :(");
      return;
    }

    // @ts-ignore
    const quote = await fin.quoteSummary(result.quotes[0].symbol, {});

    if (!quote) {
      message.channel.send(
        "Something went wrong trying to get the summary of that stock :("
      );
      return;
    }
    const stock = getStockPrice(quote);

    message.channel.send(stock ?? "");
  } catch (e) {
    console.error("an error happened", Object.keys(e as any));
    console.log("name is", (e as any).name);
    message.channel.send((e as any).message);
    return;
  }
}

export async function commandIsSonicOnSaleYet(
  message: Discord.Message
): Promise<void> {
  const TEAM_SONIC_RACING = 785260;
  try {
    const sonicGame = await getSteamGame(TEAM_SONIC_RACING);
    if (!sonicGame) {
      throw new Error(
        `Couldnt find the game with ID: ${TEAM_SONIC_RACING}, did they remove it from Steam?`
      );
    }

    if (sonicGame.price_overview?.discount_percent > 0) {
      message.channel.send(
        `${sonicGame.name} is on sale!!! (${
          sonicGame.price_overview.final_formatted
        })\n${getSteamLink(TEAM_SONIC_RACING)}
        `
      );
    } else {
      message.channel.send(
        `${sonicGame.name} is not on sale and still costs ${sonicGame.price_overview.final_formatted} :(`
      );
    }
  } catch (e) {
    message.channel.send((e as any).message);
    return;
  }
}

export async function commandWhatAWeek(
  message: Discord.Message
): Promise<void> {
  const weekdayFormatter = new Intl.DateTimeFormat("en-GB", {
    weekday: "long",
  });
  const timeFormatter = new Intl.DateTimeFormat("en-GB", {
    minute: "numeric",
    hour: "numeric",
  });

  const date = new Date();

  if (message.author.id != "118486265690718215") {
    date.setHours(date.getHours() - 1);
  }

  const dateString = `- me at ${timeFormatter.format(
    date
  )} on a ${weekdayFormatter.format(date)}`;

  const canvas = await createWhatAWeekCanvas(dateString);
  const attachment = new Discord.MessageAttachment(
    canvas.toBuffer(),
    `what-a-week.png`
  );

  message.channel.send(attachment);
  return;
}

export async function commandGetRandomCage(
  message: Discord.Message
): Promise<void> {
  const cage = await getRandomCage();

  if (!cage) {
    message.channel.send("Failed to get a random cage");
    return;
  }

  const buffer = await readFile(cage?.path);
  const attachment = new Discord.MessageAttachment(
    buffer,
    `${cage.filename}.jpg`
  );

  message.channel.send(`**${cage.alt}**`, attachment);

  return;
}

export async function commandGetCursedMeme(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  const msg = await message.channel.send(
    "Creating a really cursed meme, I'm basically an AI!"
  );
  const canvas = await getRandomMeme(message, args);

  if (!canvas) {
    message.channel.send("Failed to get random meme");
    return;
  }

  const attachment = new Discord.MessageAttachment(
    canvas.toBuffer(),
    "what-a-meme.png"
  );

  await msg.delete();
  message.channel.send(attachment);

  return;
}

export async function commandAskDuck(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  const msg = await message.channel.send(
    "Asking the great mind of ducks in motion!"
  );
  const query = args.join(" ");
  try {
    await askDuckDuckGo({ query });
  } catch (e) {
    await msg.edit((e as any).message);
  }

  return;
}

export async function commandHandleReplacementWords(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  const firstCommand = args[0];
  const removedCommandArgs = args.slice(1);
  try {
    switch (firstCommand) {
      case "add":
        await commandAddReplacementWord(message, removedCommandArgs);
        return;
      case "disable":
        await commandDeleteReplacementWord(message, removedCommandArgs);
        return;
      case "list":
        await commandGetAllReplacementWords(message);
        return;
      default:
        await message.channel.send(
          "Failed to parse replacement command, use one of [add,disable,list]"
        );
    }
  } catch (e) {
    console.error(e);
    await message.channel.send("Failed to parse command");
  }
}

export async function commandAddReplacementWord(
  message: Discord.Message,
  args: string[] | undefined
): Promise<void> {
  try {
    const newReplacementWord = args?.join(" ");

    if (!newReplacementWord) {
      await message.channel.send("Empty replacement word, try again");
      return;
    }

    await addReplacementWord({
      word: newReplacementWord,
      discord_id: message.author.id,
      discord_username: message.author.username,
    });

    await message.channel.send(
      `Successfully added the word: **${newReplacementWord}** to the list`
    );
  } catch (e) {
    console.error(e);
    await message.channel.send("Failed to add new replacement word");
  }
}

export async function commandDeleteReplacementWord(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  try {
    const possibleIdentifier = Number.parseInt(args[0]);

    if (!Number.isNaN(possibleIdentifier)) {
      const success = await disableReplacementWord({ id: possibleIdentifier });
      await message.channel.send(success);
      return;
    }

    await message.channel.send(
      "Couldnt parse the identifier into a valid id, try again (with a number)"
    );
    return;
  } catch (e) {
    console.error(e);
    await message.channel.send("Failed to disable a replacement word");
  }
}

export async function commandGetAllReplacementWords(
  message: Discord.Message
): Promise<void> {
  try {
    const replacementWords = await getAllReplacementWords();
    const chatMessage = replacementWords
      .map((rw) => {
        return `**${rw.word}** (${rw.id})`;
      })
      .join(", ");

    await message.channel.send(chatMessage);
  } catch (e) {
    console.error(e);
    await message.channel.send(
      "Failed to fetch a list of the replacement words"
    );
  }
}
