import Discord from "discord.js";
import {
  getRandomQuote,
  getLatestQuote,
  disableQuote,
  editQuote,
  addQuote,
  getQuoteById,
  getActuallyRandomQuote,
  increaseViewOnQuote,
  searchForQuoteWithFullText,
  getRandomInspirationalQuote,
} from "../../lib/db/quote";
import {
  createQuoteCanvas,
  createMemeCanvas,
  createCursedCanvas,
  createSunnyCanvas,
} from "../../lib/canvas/server";
import {
  getContent,
  readArguments,
  KeywordEnum,
  printWhoWrote,
} from "../helper";
import path from "path";
import { getRandomScreenshot } from "../../lib/db/screenshots";
import { getRandomSunnyImage, getRandomSunnyQuote } from "../../lib/db/sunny";
import { getAllReplacementWords } from "../../lib/db/replacement_word";
import { leadingZero, substituteWords } from "../../lib/util";

const IS_DEV = process.env.IS_DEV ? true : false;

export async function commandGetRandomQuote(
  message: Discord.Message
): Promise<void> {
  const quote = await getRandomQuote();
  const msg = getContent(quote);
  message.channel.send(msg);
}

export async function commandGetLatestQuote(
  message: Discord.Message
): Promise<void> {
  const quote = await getLatestQuote();
  const msg = getContent(quote);
  message.channel.send(msg);
}

export async function commandAddQuote(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  const keywords = readArguments(args);
  const quoteContent = keywords.value.join(" ");
  if (!quoteContent) {
    message.channel.send("I didnt understand the quote :(");
    return;
  }
  const quote = await addQuote(quoteContent);
  message.channel.send(`Successfully added a quote! (${quote.id})`);
}

export async function commandEditQuote(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  const keywords = readArguments(args);
  if (keywords.type !== KeywordEnum.Specific || !keywords.additional) {
    message.channel.send("I couldnt parse your edit command :(");
    return;
  }

  const quote = await getQuoteById(keywords.value);
  if (!quote) {
    message.channel.send("I couldnt find the quote you wanted to edit :(");
    return;
  }

  quote.content = keywords.additional?.join(" ");
  await editQuote(quote);
  message.channel.send("Successfully edited quote!");
}

export async function commandDisableQuote(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  const keywords = readArguments(args);
  if (keywords.type !== KeywordEnum.Specific) {
    message.channel.send("I couldnt parse your disable command :(");
    return;
  }

  const msg = await disableQuote(keywords.value);
  message.channel.send(msg);
}

export async function commandGetImageQuote(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  let quote;
  let msg;
  try {
    const keyword = readArguments(args);

    switch (keyword.type) {
      case KeywordEnum.None:
      case KeywordEnum.Random: {
        msg = await message.channel.send(
          "Generating meme from random quote, please wait!"
        );
        quote = await getActuallyRandomQuote();
        if (!IS_DEV) {
          await increaseViewOnQuote(quote);
        }
        break;
      }
      case KeywordEnum.Latest: {
        msg = await message.channel.send(
          "Generating meme from latest quote, please wait!"
        );
        quote = await getLatestQuote();
        break;
      }
      case KeywordEnum.Specific: {
        msg = await message.channel.send(
          "Generating meme from specific quote, please wait!"
        );
        quote = await getQuoteById(keyword.value);
        break;
      }
      case KeywordEnum.Multiple:
      case KeywordEnum.Word: {
        msg = await message.channel.send(
          "Searching for meme quote, please wait!"
        );
        const escapedQuery = keyword.value.filter((value: string) => {
          const wordRe = new RegExp(/\w/, "g");
          if (wordRe.test(value)) {
            return value;
          }
          return null;
        });

        quote = await searchForQuoteWithFullText(escapedQuery);

        if (!quote) {
          throw new Error("Couldnt find a meme quote containing that string!");
        }
        break;
      }
    }

    if (!quote) {
      throw new Error("Failed to fetch a meme quote, try again.");
    }

    const canvas = await createMemeCanvas(quote);

    const attachment = new Discord.MessageAttachment(
      canvas.toBuffer(),
      "random-image.png"
    );

    msg?.delete();
    message.channel.send(attachment);
  } catch (e) {
    msg?.delete();

    await message.channel.send((e as any).message);
    return;
  }
}

export async function commandGetOldImageQuote(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  let quote;
  let msg;
  try {
    const keyword = readArguments(args);

    switch (keyword.type) {
      case KeywordEnum.None:
      case KeywordEnum.Random: {
        msg = await message.channel.send(
          "Generating image from random quote, please wait!"
        );
        quote = await getActuallyRandomQuote();
        if (!IS_DEV) {
          await increaseViewOnQuote(quote);
        }
        break;
      }
      case KeywordEnum.Latest: {
        msg = await message.channel.send(
          "Generating image from latest quote, please wait!"
        );
        quote = await getLatestQuote();
        break;
      }
      case KeywordEnum.Specific: {
        msg = await message.channel.send(
          "Generating image from specific quote, please wait!"
        );
        quote = await getQuoteById(keyword.value);
        break;
      }
      case KeywordEnum.Multiple:
      case KeywordEnum.Word: {
        msg = await message.channel.send("Searching for quote, please wait!");
        const escapedQuery = keyword.value.filter((value: string) => {
          const wordRe = new RegExp(/\w/, "g");
          if (wordRe.test(value)) {
            return value;
          }
          return null;
        });

        quote = await searchForQuoteWithFullText(escapedQuery);

        if (!quote) {
          throw new Error("Couldnt find a quote containing that string!");
        }
        break;
      }
    }

    if (!quote) {
      throw new Error("Failed to fetch a quote, try again.");
    }

    const canvas = await createQuoteCanvas(quote);

    const attachment = new Discord.MessageAttachment(
      canvas.toBuffer(),
      "random-image.png"
    );

    msg?.delete();
    message.channel.send(attachment);
  } catch (e) {
    msg?.delete();

    await message.channel.send((e as any).message);
    return;
  }
}

export async function commandGetCursedQuote(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  let quote;
  let msg;
  try {
    const keyword = readArguments(args);

    switch (keyword.type) {
      case KeywordEnum.None:
      case KeywordEnum.Random: {
        msg = await message.channel.send(
          "Generating cursed image from random quote, please wait!"
        );
        quote = await getActuallyRandomQuote();
        if (!IS_DEV) {
          await increaseViewOnQuote(quote);
        }

        break;
      }
      case KeywordEnum.Latest: {
        msg = await message.channel.send(
          "Generating cursed image from latest quote, please wait!"
        );
        quote = await getLatestQuote();
        break;
      }
      case KeywordEnum.Specific: {
        msg = await message.channel.send(
          "Generating cursed image from specific quote, please wait!"
        );
        quote = await getQuoteById(keyword.value);
        break;
      }
      case KeywordEnum.Multiple:
      case KeywordEnum.Word: {
        msg = await message.channel.send(
          "Searching for cursed quote, please wait!"
        );
        const escapedQuery = keyword.value.filter((value: string) => {
          const wordRe = new RegExp(/\w/, "g");
          if (wordRe.test(value)) {
            return value;
          }
          return null;
        });

        quote = await searchForQuoteWithFullText(escapedQuery);

        if (!quote) {
          throw new Error(
            "Couldnt find a cursed quote containing that string!"
          );
        }
        break;
      }
    }

    if (!quote) {
      throw new Error("Failed to fetch a cursed quote, try again.");
    }

    const screenshot = await getRandomScreenshot();
    const canvas = await createCursedCanvas(quote, screenshot);

    const attachment = new Discord.MessageAttachment(
      canvas.toBuffer(),
      `${screenshot.id}.png`
    );

    msg?.delete();
    message.channel.send(attachment);
  } catch (e) {
    console.error(e);
    msg?.delete();

    await message.channel.send((e as any).message);
    return;
  }
}

export async function commandGetSunnyQuote(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  let quote;
  let msg;
  try {
    const keyword = readArguments(args);

    switch (keyword.type) {
      case KeywordEnum.None:
      case KeywordEnum.Random: {
        msg = await message.channel.send(
          "Generating sunny image from random quote, please wait!"
        );
        quote = await getActuallyRandomQuote();
        if (!IS_DEV) {
          await increaseViewOnQuote(quote);
        }

        break;
      }
      case KeywordEnum.Latest: {
        msg = await message.channel.send(
          "Generating sunny image from latest quote, please wait!"
        );
        quote = await getLatestQuote();
        break;
      }
      case KeywordEnum.Specific: {
        msg = await message.channel.send(
          "Generating sunny image from specific quote, please wait!"
        );
        quote = await getQuoteById(keyword.value);
        break;
      }
      case KeywordEnum.Multiple:
      case KeywordEnum.Word: {
        msg = await message.channel.send(
          "Searching for sunny quote, please wait!"
        );
        const escapedQuery = keyword.value.filter((value: string) => {
          const wordRe = new RegExp(/\w/, "g");
          if (wordRe.test(value)) {
            return value;
          }
          return null;
        });

        quote = await searchForQuoteWithFullText(escapedQuery);

        if (!quote) {
          throw new Error("Couldnt find a sunny quote containing that string!");
        }
        break;
      }
    }

    if (!quote) {
      throw new Error("Failed to fetch a sunny quote, try again.");
    }

    const sunny_shot = await getRandomSunnyImage();
    const filename = path.basename(sunny_shot.path);
    const canvas = await createSunnyCanvas(quote, sunny_shot.path);

    const attachment = new Discord.MessageAttachment(
      canvas.toBuffer(),
      `${filename}.png`
    );

    msg?.delete();
    message.channel.send(attachment);
  } catch (e) {
    console.error(e);
    msg?.delete();

    await message.channel.send((e as any).message);
    return;
  }
}

export async function commandWhoWrote(message: Discord.Message): Promise<void> {
  try {
    const inspiration = await getRandomInspirationalQuote();
    const quote = await getRandomQuote();

    if (inspiration && quote) {
      message.channel.send(printWhoWrote(quote, inspiration));
      return;
    }
  } catch (e) {
    console.error(e);
    message.channel.send(`Something went wrong: ${(e as any).message}`);
  }
}

export async function commandInspirationalQuote(
  message: Discord.Message
): Promise<void> {
  try {
    const inspiration = await getRandomInspirationalQuote();
    message.channel.send(`> ${inspiration?.quote} - ${inspiration?.author}`);
  } catch (e) {
    console.error(e);
    message.channel.send(`Something went wrong: ${(e as any).message}`);
  }
}

export async function commandGetActualSunnyQuote(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  try {
    const quote = await getRandomSunnyQuote();
    message.channel.send(
      `S${leadingZero(quote.season)}E${leadingZero(quote.episode)} - ${
        quote.content
      }`
    );
  } catch (e) {
    console.error(e);
    message.channel.send(`Something went wrong: ${(e as any).message}`);
  }
}

export async function commandGetActualNotSunnyQuote(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  try {
    const words = await getAllReplacementWords();
    const quote = await getRandomSunnyQuote();
    const notSunnyQuote = substituteWords(
      quote.content,
      words.map((w) => w.word)
    );

    message.channel.send(
      `S${leadingZero(quote.season)}E${leadingZero(
        quote.episode
      )} - ${notSunnyQuote}`
    );
  } catch (e) {
    console.error(e);
    message.channel.send(`Something went wrong: ${(e as any).message}`);
  }
}
