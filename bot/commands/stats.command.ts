import {
  getCommandLogStatsByCommand,
  getCommandLogStatsByUsername,
  getDataStats,
} from "../../lib/db/stats";
import { MessageArguments } from "bot/helper";
import * as Discord from "discord.js";

const keywords: { [key: string]: string } = {
  command: "command",
  user: "user",
  data: "data",
  none: "none",
};

export function readCommandArguments(args?: string[]): MessageArguments {
  if (!args || !args.length || !args[0]) {
    return { value: null, type: keywords.none };
  }

  const firstArgument = args[0].toLowerCase();
  const availableKeywords = Object.values(keywords);

  if (!availableKeywords.includes(firstArgument)) {
    return { value: null, type: keywords.none };
  }

  return { value: firstArgument, type: keywords[firstArgument] };
}

export async function commandGetStats(
  message: Discord.Message,
  args: string[]
) {
  const keyword = readCommandArguments(args);

  switch (keyword.type) {
    case "user":
      return await commandGetCommandUserStats(message);
    case "command":
      return await commandGetCommandStats(message);
    case "data":
      return await commandGetDataStats(message);
    default:
      return await message.channel.send("Invalid stat command :(");
  }
}

export async function commandGetCommandStats(message: Discord.Message) {
  const cmdStats = await getCommandLogStatsByCommand();

  const embed = generateEmbed({
    values: cmdStats,
    title: "Command stats by command",
    nameKey: "command",
    valueKey: "command_calls",
  });

  await message.channel.send({ embed: embed });
}

export async function commandGetCommandUserStats(message: Discord.Message) {
  const userStats = await getCommandLogStatsByUsername();

  const embed = generateEmbed({
    values: userStats,
    title: "Command stats by username",
    nameKey: "discord_username",
    valueKey: "command_calls",
  });

  await message.channel.send({ embed: embed });
}

export async function commandGetDataStats(message: Discord.Message) {
  const dataStats = await getDataStats();
  // console.log(dataStats)
}

interface FormattedReplyArgs {
  values: { [key: string]: any }[];
  searchKey: string;
}
function getFormattedReply(args: FormattedReplyArgs) {
  const { values, searchKey } = args;
  const rowWithLongestSearchKey = values.reduce((longest, current) => {
    return longest[searchKey].length > current[searchKey].length
      ? longest
      : current;
  });

  const keyLength = rowWithLongestSearchKey[searchKey].length + 5;

  return values
    .map((row) => {
      return Object.values(row)
        .map((value) => {
          console.log(padStringUntilLength(value, keyLength).length);
          return `${padStringUntilLength(value, keyLength)}`;
        })
        .join("\t\t\t | ");
    })
    .join("\n > ");
}

function padStringUntilLength(str: string, len: number) {
  if (Number(str)) {
    return str.toString().padEnd(len);
  }
  return str.padEnd(len);
}

interface FormattedEmbedArgs {
  values: { [key: string]: any }[];
  title: string;
  nameKey: string;
  valueKey: string;
}

function generateEmbed(args: FormattedEmbedArgs) {
  const { values, title, nameKey, valueKey } = args;
  return {
    title: title,
    fields: values.map((val) => ({
      name: val[nameKey],
      value: val[valueKey],
      // inline: true,
    })),
  };
}
