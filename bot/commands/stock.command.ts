import { addStock, getProfile, removeStock } from "../../lib/db/stock";
import Discord from "discord.js";
import { getOnlineStockProfile, getStockPrice } from "../../bot/helper";
import {
  getStock,
  getStockSummary,
  searchAndGetStockSummary,
} from "../../lib/stock";

export enum StockKeywordEnum {
  Add = "add",
  Remove = "remove",
  Profile = "profile",
  None = "none",
  Get = "get",
}

export type MessageArguments = {
  value: any;
  type: StockKeywordEnum;
  additional?: string[];
};

const readStockArguments = (args?: string[]): MessageArguments => {
  if (!args || !args.length || !args[0]) {
    return { value: null, type: StockKeywordEnum.None };
  }

  const firstArgument = args[0].toLowerCase();
  if (firstArgument == StockKeywordEnum.Add) {
    if (args[1] && args[1].length > 0) {
      return { value: args[1], type: StockKeywordEnum.Add };
    }
  }

  if (firstArgument == StockKeywordEnum.Remove) {
    if (args[1] && args[1].length > 0) {
      return { value: args[1], type: StockKeywordEnum.Remove };
    }
  }

  if (firstArgument == StockKeywordEnum.Profile) {
    return { value: null, type: StockKeywordEnum.Profile };
  }

  return { value: args[0], type: StockKeywordEnum.Get };
};

export async function commandHandleStock(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  const keywords = readStockArguments(args);
  switch (keywords.type) {
    case StockKeywordEnum.Add: {
      await commandAddStock(message, keywords.value);
      return;
    }
    case StockKeywordEnum.Remove: {
      await commandRemoveStock(message, keywords.value);
      return;
    }
    case StockKeywordEnum.Profile: {
      await commandGetProfile(message);
      return;
    }
    default:
    case StockKeywordEnum.Get: {
      await commandGetStock(message, keywords.value);
      return;
    }
  }
}

export async function commandGetCheggStock(
  message: Discord.Message
): Promise<void> {
  const quote = await getStockSummary("CHGG");

  message.channel.send(getStockPrice(quote, ":egg:") ?? "");
}

export async function commandAddStock(
  message: Discord.Message,
  symbol: string
): Promise<void> {
  try {
    const quoteResult = await searchAndGetStockSummary(symbol);
    const result = await addStock(
      message.author.id,
      message.author.username,
      quoteResult.price.symbol,
      quoteResult
    );
    if (!result) {
      message.channel.send(
        `You already have (${quoteResult.price.symbol}) in your profile :(`
      );
      return;
    }
    message.channel.send(
      `Successfully added (${quoteResult.price.symbol}) to your profile!`
    );
  } catch (e) {
    console.error(e);
    message.channel.send(`Something went wrong: ${(e as any).message}`);
  }
}

export async function commandRemoveStock(
  message: Discord.Message,
  symbol: string
): Promise<void> {
  try {
    const result = await removeStock(message.author.id, symbol);

    if (!result) {
      message.channel.send(`You don't have (${symbol}) in your profile :(`);
      return;
    }
    message.channel.send(`Successfully removed (${symbol}) from your profile!`);
  } catch (e) {
    console.error(e);
    message.channel.send(`Failed to remove (${symbol}) from your profile`);
  }
}

export async function commandGetProfile(
  message: Discord.Message
): Promise<void> {
  const msg = await message.channel.send(
    "Fetching live updates for your profile.."
  );
  try {
    const result = [];
    const profile = await getProfile(message.author.id);
    for (const stock of profile) {
      result.push(await getStockSummary(stock.symbol));
    }

    if (!result || !result.length) {
      msg.edit(`You have no stocks in your profile :(`);
      return;
    }

    msg.edit(getOnlineStockProfile(result) ?? "");
  } catch (e) {
    console.error(e);
    msg.edit(`Failed to fetch your stock profile`);
  }
}

export async function commandGetStock(
  message: Discord.Message,
  symbol: string
): Promise<void> {
  try {
    if (!symbol) {
      message.channel.send("You have to pass the identifier for the stock.");
      return;
    }
    const result = await getStock(symbol);

    if (!result || result.count <= 0 || !result.quotes.length) {
      message.channel.send("Couldnt find a stock with that name :(");
      return;
    }

    const quote = await getStockSummary(result.quotes[0].symbol);

    message.channel.send(getStockPrice(quote) ?? "");
  } catch (e) {
    message.channel.send(`Something went wrong: ${(e as any).message}`);
    return;
  }
}
