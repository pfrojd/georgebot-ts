import * as Discord from "discord.js";
import fs from "fs";
import path from "path";
import { nanoid } from "nanoid";
import {
  readArguments,
  KeywordEnum,
  getTalismanFormat,
  MessageArguments,
} from "../helper";
import {
  getRandomImage,
  getLatestImage,
  getImageById,
  insertImage,
  disableImage,
} from "../../lib/db/talisman";
import { fetchImage } from "../../lib/image";

const { readFile, writeFile } = fs.promises;

const imageFolder = process.env.IMAGE_PATH || process.cwd();
const imagePath = path.resolve(imageFolder, "talisman");

export async function writeTalismanImage(
  imageBuffer: Buffer,
  fileName: string
): Promise<string> {
  const filePath = path.resolve(imagePath, fileName);
  await writeFile(filePath, imageBuffer);
  return filePath;
}

export async function handlePossibleTalismanMessage(
  message: Discord.Message
): Promise<boolean> {
  const { content, attachments } = message;
  try {
    const attachment = attachments.first();
    if (!attachment) {
      console.error("No attachment actually found");
      return false;
    }

    const imageBuffer = await fetchImage(attachment.url);
    const fileExtension = path.extname(attachment.url);
    const filePath = await writeTalismanImage(
      imageBuffer,
      `${nanoid()}${fileExtension}`
    );
    await insertImage(content, filePath);
    message.react("✅");
    return true;
  } catch (e) {
    message.react("❌");
    console.error(e);
    return true;
  }
}

export async function commandDisableTalismanImage(
  message: Discord.Message,
  keyword: MessageArguments
): Promise<void> {
  if (!keyword || keyword.type != KeywordEnum.Disable || !keyword.value) {
    message.channel.send("I couldnt parse your disable command :(");
    return;
  }
  try {
    const msg = await disableImage(keyword.value);
    message.channel.send(msg);
  } catch (error) {
    console.error(error);
    message.channel.send(`Something went wrong: ${error}`);
  }
}

export async function commandHandleTalismanCommand(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  const keyword = readArguments(args);
  switch (keyword.type) {
    case KeywordEnum.Disable:
      return commandDisableTalismanImage(message, keyword);
    case KeywordEnum.None:
    case KeywordEnum.Random:
    case KeywordEnum.Specific:
    case KeywordEnum.Latest:
    default:
      return commandGetTalismanImage(message, keyword);
  }
}

export async function commandGetTalismanImage(
  message: Discord.Message,
  keyword: MessageArguments
): Promise<void> {
  let image;
  let msg;
  try {
    switch (keyword.type) {
      case KeywordEnum.None:
      case KeywordEnum.Random: {
        msg = await message.channel.send(
          "Fetching random talisman image, please wait!"
        );
        image = await getRandomImage();
        break;
      }
      case KeywordEnum.Latest: {
        msg = await message.channel.send(
          "Fetching latest talisman image, please wait!"
        );
        image = await getLatestImage();
        break;
      }
      case KeywordEnum.Specific: {
        msg = await message.channel.send(
          "Fetching specific talisman image, please wait!"
        );
        image = await getImageById(keyword.value);
        break;
      }
    }

    if (!image) {
      throw new Error("Could not fetch image");
    }

    const buffer = await readFile(image.path);
    const attachment = new Discord.MessageAttachment(buffer, `${image.id}.png`);
    msg?.delete();
    message.channel.send(
      getTalismanFormat(image.id, image.description, image.created_at),
      attachment
    );
  } catch (e) {
    msg?.delete();
    await message.channel.send("Failed to fetch the image, try again.");
    console.log(keyword);
    console.error(e);
    return;
  }
}
