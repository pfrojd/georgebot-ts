import Discord from "discord.js";
import {
  getValidNoun,
  getValidAdjective,
  getDefinition,
  getDefinitionFormatting,
  getGlossaryDefinitions,
} from "../../lib/words";

async function getNumberOfNouns(n: number) {
  let iteration = 0;
  const nouns = [];

  if (n > 10) n = 10;

  while (iteration < n) {
    nouns.push(await getValidNoun());
    iteration++;
  }

  return nouns;
}

async function getNumberOfAdjectives(n: number) {
  let iteration = 0;
  const adjectives = [];

  if (n > 10) n = 10;

  while (iteration < n) {
    adjectives.push(await getValidAdjective());
    iteration++;
  }

  return adjectives;
}

function validateArgs(args: string[]) {
  return !Number.isNaN(Number(args[0])) && !Number.isNaN(Number(args[1]));
}

export async function commandTheyCallMe(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  try {
    const valid = validateArgs(args);

    const numberOfNouns = valid ? Number(args[1]) : 1;
    const numberOfAdjectives = valid ? Number(args[0]) : 1;

    const nouns = await getNumberOfNouns(numberOfNouns);
    const adjectives = await getNumberOfAdjectives(numberOfAdjectives);

    let hasResponded = false;

    const adjectivesStr = adjectives.map((a) => a.lemma).join(" ");
    const nounStr = nouns.map((n) => n.lemma).join(" ");

    const text = `They call me **${adjectivesStr} ${nounStr}**!`;
    const msg = await message.channel.send(text);

    const messageDefinitionFilter = (
      reaction: Discord.MessageReaction,
      user: Discord.User
    ) => reaction.emoji.name == "❔" && !user.bot;

    const collector = msg.createReactionCollector(messageDefinitionFilter, {
      time: 300000,
    });

    collector.on("collect", () => {
      if (!hasResponded) {
        msg.channel.send(getGlossaryDefinitions(nouns, adjectives));
        hasResponded = true;
      }
    });

    collector.on("end", () => {
      msg.react("❔");
    });

    msg.react("❔");
  } catch (e) {
    message.channel.send("Something went wrong doing lookups");
    return;
  }
}

export async function commandGetDefinition(
  message: Discord.Message,
  args: string[]
): Promise<void> {
  try {
    const words = args.join(" ");
    const defs = await getDefinition(words);
    if (!defs.length) {
      message.channel.send(`I couldn't find a definition for **${words}** :(`);
      return;
    }
    message.channel.send(getDefinitionFormatting(defs));
  } catch (e) {
    console.log(e);
    message.channel.send(
      "Something went wrong when trying to find the definition"
    );
  }
}
