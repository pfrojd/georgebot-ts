import * as Discord from "discord.js";
import {
  commandGetRandomQuote,
  commandGetImageQuote,
  commandAddQuote,
  commandDisableQuote,
  commandGetLatestQuote,
  commandEditQuote,
  commandGetOldImageQuote,
  commandGetCursedQuote,
  commandWhoWrote,
  commandInspirationalQuote,
  commandGetSunnyQuote,
  commandGetActualNotSunnyQuote,
  commandGetActualSunnyQuote,
} from "./commands/quote.command";
import { getHelpText } from "./helper";
import { commandHandleTalismanCommand } from "./commands/talisman.command";
import {
  commandTheyCallMe,
  commandGetDefinition,
} from "./commands/words.command";
import {
  commandWontLikeThis,
  commandRollDice,
  commandRandomPoeBuild,
  commandGetCheggStock,
  commandIsSonicOnSaleYet,
  commandWhatAWeek,
  commandGetRandomCage,
  commandGetCursedMeme,
  commandHandleReplacementWords,
} from "./commands/meme.command";
import { handleIncomingAttachment } from "./attachment";
import {
  commandGetNotToday,
  commandGetRandomTodaysHistory,
} from "./commands/facts.command";
import { commandGetToken } from "./commands/api.command";
import { commandHandleStock } from "./commands/stock.command";
import { logRepeatedMessage, logUsage } from "../lib/usage";

const IS_DEV = process.env.IS_DEV ? true : false;

const COMMAND_PREFIX = IS_DEV ? "?" : "!";
const COMMAND_DELIMITER = " ";

interface IGetCommand {
  args: string[];
  command: string | undefined;
}

export function getCommand(content: string): IGetCommand {
  const args = content
    .slice(COMMAND_PREFIX.length)
    .trim()
    .split(COMMAND_DELIMITER);
  return {
    args: args,
    command: args.shift()?.toLowerCase(),
  };
}

const COMMAND = {
  RANDOM: "random",
  QUOTE: "quote",
  IMAGE: "image",
  MEME: "oldmeme",
  REMOVE: "remove",
  LATEST: "latest",
  HELP: "help",
  EDIT: "edit",
  TALISMAN: "talisman",
  WONT_LIKE_THIS: "wontlikethis",
  THEY_CALL_ME: "theycallme",
  ROLL: "roll",
  DEFINE: "def",
  CURSED: "cursed",
  TODAY: "today",
  SFX: "sfx",
  WHAT_BUILD: "whatbuild",
  TOKEN: "token",
  STOCK: "stock",
  CHEGG: "chegg",
  WHOWROTE: "whowrote",
  INSPIRATIONAL: "inspirational",
  SONIC: "sonic",
  SUNNY: "sunny",
  SUNNY_QUOTE: "sun",
  NOT_SUNNY_QUOTE: "notsunny",
  WHAT_A_WEEK: "week",
  CAGE: "cage",
  CURSED_MEME: "meme",
  ASK: "ask",
  STATS: "stats",
  NOT_TODAY: "nottoday",
  REPLACEMENT: "replacement",
  REPEAT: "r",
};

let lastMessage: Discord.Message | null = null;
let lastCommand: string | undefined;
let lastFn: any;
let lastArgs: any;

export async function messageHandler(
  message: Discord.Message
): Promise<Discord.Message | undefined | void> {
  // If the message is send by a bot, ignore it.
  if (message.author.bot) return;

  await handleIncomingAttachment(message);

  // If the message does not contain the COMMAND_PREFIX, ignore it.
  if (!message.content.startsWith(COMMAND_PREFIX)) return;

  const { args, command } = getCommand(message.content);

  switch (command) {
    case COMMAND.RANDOM: {
      await commandGetRandomQuote(message);
      await logUsage(message, COMMAND.RANDOM);
      return;
    }
    case COMMAND.IMAGE: {
      await commandGetOldImageQuote(message, args);
      await logUsage(message, COMMAND.IMAGE);
      return;
    }
    case COMMAND.QUOTE: {
      await commandAddQuote(message, args);
      await logUsage(message, COMMAND.QUOTE);
      return;
    }
    case COMMAND.REMOVE: {
      await commandDisableQuote(message, args);
      await logUsage(message, COMMAND.REMOVE);
      return;
    }
    case COMMAND.LATEST: {
      await commandGetLatestQuote(message);
      await logUsage(message, COMMAND.LATEST);
      return;
    }
    case COMMAND.EDIT: {
      await commandEditQuote(message, args);
      await logUsage(message, COMMAND.EDIT);
      return;
    }
    case COMMAND.HELP: {
      await message.channel.send(getHelpText());
      await logUsage(message, COMMAND.HELP);
      return;
    }
    case COMMAND.TALISMAN: {
      await commandHandleTalismanCommand(message, args);
      await logUsage(message, COMMAND.TALISMAN);
      recordLastMessage(
        message,
        COMMAND.TALISMAN,
        commandHandleTalismanCommand,
        args
      );
      return;
    }
    case COMMAND.THEY_CALL_ME: {
      await commandTheyCallMe(message, args);
      await logUsage(message, COMMAND.THEY_CALL_ME);
      return;
    }
    case COMMAND.DEFINE: {
      await commandGetDefinition(message, args);
      await logUsage(message, COMMAND.DEFINE);
      return;
    }
    case COMMAND.WONT_LIKE_THIS: {
      await commandWontLikeThis(message);
      await logUsage(message, COMMAND.WONT_LIKE_THIS);
      return;
    }
    case COMMAND.ROLL: {
      await commandRollDice(message);
      await logUsage(message, COMMAND.ROLL);
      return;
    }
    case COMMAND.MEME: {
      await commandGetImageQuote(message, args);
      await logUsage(message, COMMAND.MEME);
      return;
    }
    case COMMAND.CURSED: {
      await commandGetCursedQuote(message, args);
      await logUsage(message, COMMAND.CURSED);
      recordLastMessage(message, COMMAND.CURSED, commandGetCursedQuote, args);
      return;
    }
    case COMMAND.TODAY: {
      await commandGetRandomTodaysHistory(message);
      await logUsage(message, COMMAND.TODAY);
      recordLastMessage(
        message,
        COMMAND.TODAY,
        commandGetRandomTodaysHistory,
        args
      );
      return;
    }
    case COMMAND.WHAT_BUILD: {
      await commandRandomPoeBuild(message);
      await logUsage(message, COMMAND.WHAT_BUILD);
      return;
    }
    case COMMAND.TOKEN: {
      await commandGetToken(message, args);
      await logUsage(message, COMMAND.TOKEN);
      return;
    }
    case COMMAND.CHEGG: {
      await commandGetCheggStock(message);
      await logUsage(message, COMMAND.CHEGG);
      return;
    }
    case COMMAND.STOCK: {
      await commandHandleStock(message, args);
      await logUsage(message, COMMAND.STOCK);
      return;
    }
    case COMMAND.WHOWROTE: {
      await commandWhoWrote(message);
      await logUsage(message, COMMAND.WHOWROTE);
      return;
    }
    case COMMAND.INSPIRATIONAL: {
      await commandInspirationalQuote(message);
      await logUsage(message, COMMAND.INSPIRATIONAL);
      return;
    }
    case COMMAND.SONIC: {
      await commandIsSonicOnSaleYet(message);
      await logUsage(message, COMMAND.SONIC);
      return;
    }
    case COMMAND.SUNNY: {
      await commandGetSunnyQuote(message, args);
      await logUsage(message, COMMAND.SUNNY);
      recordLastMessage(message, COMMAND.SUNNY, commandGetSunnyQuote, args);
      return;
    }
    case COMMAND.WHAT_A_WEEK: {
      await commandWhatAWeek(message);
      await logUsage(message, COMMAND.WHAT_A_WEEK);
      return;
    }
    case COMMAND.CAGE: {
      await commandGetRandomCage(message);
      await logUsage(message, COMMAND.CAGE);
      recordLastMessage(message, COMMAND.CAGE, commandGetRandomCage, args);
      return;
    }
    case COMMAND.CURSED_MEME: {
      await commandGetCursedMeme(message, args);
      await logUsage(message, COMMAND.CURSED_MEME);
      recordLastMessage(
        message,
        COMMAND.CURSED_MEME,
        commandGetCursedMeme,
        args
      );
      return;
    }
    case COMMAND.NOT_TODAY: {
      await commandGetNotToday(message);
      await logUsage(message, COMMAND.NOT_TODAY);
      recordLastMessage(message, COMMAND.NOT_TODAY, commandGetNotToday, args);
      return;
    }
    case COMMAND.REPLACEMENT: {
      await commandHandleReplacementWords(message, args);
      await logUsage(message, COMMAND.REPLACEMENT);
      return;
    }
    case COMMAND.SUNNY_QUOTE: {
      await commandGetActualSunnyQuote(message, args);
      await logUsage(message, COMMAND.SUNNY_QUOTE);
      recordLastMessage(
        message,
        COMMAND.SUNNY_QUOTE,
        commandGetActualSunnyQuote,
        args
      );
      return;
    }
    case COMMAND.NOT_SUNNY_QUOTE: {
      await commandGetActualNotSunnyQuote(message, args);
      await logUsage(message, COMMAND.NOT_SUNNY_QUOTE);
      recordLastMessage(
        message,
        COMMAND.NOT_SUNNY_QUOTE,
        commandGetActualNotSunnyQuote,
        args
      );
      return;
    }
    case COMMAND.REPEAT: {
      if (!lastMessage || !lastCommand || !lastFn || !lastArgs) {
        return;
      }
      await lastFn(message, lastArgs);
      await logRepeatedMessage(message, lastMessage, lastCommand);
      return;
    }
  }
}

function recordLastMessage(
  message: Discord.Message,
  command: string | undefined,
  fn: any,
  args: any
) {
  lastCommand = command;
  lastMessage = message;
  lastFn = fn;
  lastArgs = args;
}
