import { InspirationalQuote, Quote } from "lib/types/quote";
import format from "date-fns/format";
import { Fact } from "lib/types/fact";
import { Stock } from "lib/types/stock";
import { SteamGame } from "lib/types/steam_game";
import { QuoteSummaryResult } from "yahoo-finance2/dist/esm/src/modules/quoteSummary-iface";
// import { QuoteSummaryResult } from "yahoo-finance2/src/modules/quoteSummary-iface";

// const GEORGE_ID = process.env.GEORGE_ID
//   ? process.env.GEORGE_ID.toString()
//   : null;

// GEORGE_ID = '118486265690718215';

export type MessageArguments = {
  value: any;
  type: KeywordEnum | string;
  additional?: string[];
};

export enum KeywordEnum {
  Latest = "latest",
  Random = "random",
  Specific = "specific",
  None = "none",
  Multiple = "multiple",
  Users = "users",
  Word = "word",
  Disable = "disable",
}

const keywords = {
  latest: "latest",
  random: "random",
  specific: "specific",
  none: "none",
  multiple: "multiple",
  users: "users",
  word: "word",
  disable: "disable",
};

export const getImageFormat = (url: string, title: string): string => {
  if (title) {
    return `
**${title}**
${url}
`;
  } else {
    return `
${url}
`;
  }
};

export const readArguments = (args?: string[]): MessageArguments => {
  if (!args || !args.length || !args[0]) {
    return { value: null, type: KeywordEnum.None };
  }

  try {
    // Attempt to parse first argument as an id.
    const id = Number.parseInt(args[0]);
    if (!Number.isNaN(id)) {
      return {
        value: id,
        type: KeywordEnum.Specific,
        additional: args.slice(1),
      };
    }
  } catch (e) {
    // Do nothing.
  }

  const firstArgument = args[0].toLowerCase();

  if (firstArgument == keywords.latest) {
    return { value: null, type: KeywordEnum.Latest };
  }

  if (firstArgument == keywords.random) {
    return { value: null, type: KeywordEnum.Random };
  }

  if (firstArgument == keywords.disable) {
    try {
      // Attempt to parse first argument as an id.
      const id = Number.parseInt(args[1]);
      if (!Number.isNaN(id)) {
        return {
          value: id,
          type: KeywordEnum.Disable,
          additional: args.slice(1),
        };
      }
    } catch (e) {
      // Do nothing.
    }
  }

  if (isUserName(firstArgument)) {
    let user;
    if (args.length > 1) {
      // Check all users.
      user = args.filter((i) => isUserName(i));
    } else {
      user = [firstArgument];
    }

    return { value: user, type: KeywordEnum.Users };
  }

  if (args.length > 1) {
    // Received multiple arguments.
    return { value: args, type: KeywordEnum.Multiple };
  }

  return { value: args, type: KeywordEnum.Word };
};

export const getTextFormat = (
  id: number,
  text: string,
  time: string
): string => {
  return `
**${text}**
[${time}] - *ID: ${id}*`;
};

export const getHelpText = (): string => `\`\`\`
!random - to get a random quote
!quote text - to add a quote (anything after !quote is added)
!latest - to get the latest quote
!remove id - to remove a quote (it only does soft delete), repeat to enable
!image [latest,random,id,string] - to get a random image quote
!oldmeme [latest,random,id,string] - to get the image quote, meme-style
!edit id text - to update an existing quote (no history is kept)
!roll - to roll random numbers for all users in voice channel, or mention users that should roll.
!cursed - [latest,random,id,string] - to get a cursed quote
!meme - [1-7] - to get a cursed meme
--- MEMES ---
!talisman [latest,random,id] - to get a random zaif when talisman meme
To add a talisman meme, just post an attachment containing the word talisman.
!wontlikethis - will tell you what game Ryan won't like
!theycallme - will let you know what they call Ryan
!def - get definition of a word
!today - get a random event from history for today
!stock [symbol,add,remove,profile] - pretend you're not a gambler
!chegg - top meme stocks
!cage - to get a random nicolas cage image
!week - to express your pain
!sunny - to get a random always sunny screenshot
!replacement [add,disable,list] - to manage replacement words used in some memes
!nottoday - get a gribbly fact about today
!sun - get a random sunny quote
!notsunny - get a gribblified sunny quote
!r - repeat previous command
\`\`\``;

export const getContent = (quote: Quote): string => {
  if (!quote || !quote.content) {
    return `This quote (${quote.id}) is malformed, fix it!`;
  }
  return getTextFormat(
    quote.id,
    quote.content,
    format(quote.created_at, "dd/MM/yyyy")
  );
};

export const getTalismanFormat = (
  id: number,
  description: string,
  time: Date
): string => {
  return `
**${description}**
[${format(time, "dd/MM/yyyy")}] - *ID: ${id}*
`;
};

export const getIcon = (val: boolean): string => {
  if (!val) {
    return ":no_entry_sign:";
  } else {
    return ":white_check_mark:";
  }
};

export const getPrices = (val: string): string => {
  if (!val) {
    return ":free:";
  } else {
    return val;
  }
};

export const getListFormat = (list: SteamGame[]): string => {
  const rows = list.map(
    (item: any) =>
      `> ${item.sequence}. ${item.name} - ${getPrices(
        item.price_formatted
      )} | Played: ${getIcon(item.played)} | Early Access: ${getIcon(
        item.is_early_access
      )} | Released: ${getIcon(item.is_released)} | ID: ${item.steam_id}`
  );
  return rows.join("\n");
};

export const getHistoryFact = (fact: Fact): string => {
  const todayString = `${new Date().toLocaleString("default", {
    month: "long",
  })} ${new Date().getDate()}`;
  return "**Today (" + todayString + "):** \n> " + fact.message;
};

export const getStockPrice = (
  quote: QuoteSummaryResult,
  prefix = ""
): string | undefined => {
  if (!quote.price) {
    return;
  }

  if (
    !quote.price.regularMarketChange ||
    !quote.price.regularMarketChangePercent
  ) {
    return;
  }

  const icon =
    Math.sign(quote.price.regularMarketChange) === -1
      ? ":arrow_down_small:"
      : ":arrow_up_small:";
  return `${prefix}${quote.price.shortName} (${
    quote.price.symbol
  })\nCurrent price: ${quote.price.regularMarketPrice} ${
    quote.price.currency
  } \nChange: ${printStockPrice(
    quote.price.regularMarketChange
  )} (${printStockPrice(
    quote.price.regularMarketChangePercent * 100
  )}%) ${icon}`;
};

export const getOnlineStockProfile = (
  stocks: QuoteSummaryResult[]
): string | undefined => {
  const rows = stocks.map((stock) => {
    if (!stock || !stock.price) {
      return;
    }

    if (
      !stock.price.regularMarketChange ||
      !stock.price.regularMarketChangePercent
    ) {
      return;
    }
    const icon =
      Math.sign(stock.price?.regularMarketChange) === -1
        ? ":arrow_down_small:"
        : ":arrow_up_small:";
    return `${stock.price.shortName} (${stock.price.symbol}) - Current price: ${
      stock.price.regularMarketPrice
    } ${stock.price.currency} - Change: ${printStockPrice(
      stock.price.regularMarketChange
    )} (${printStockPrice(
      stock.price.regularMarketChangePercent * 100
    )}%) ${icon}`;
  });
  return rows.join("\n");
};

export const getStockProfile = (stocks: Stock[]): string => {
  const rows = stocks.map((stock) => {
    const icon =
      Math.sign(stock.data.price.regularMarketChange) === -1
        ? ":arrow_down_small:"
        : ":arrow_up_small:";
    return `${stock.data.price.shortName} - Current price: ${
      stock.data.price.regularMarketPrice
    } - Change: ${printStockPrice(
      stock.data.price.regularMarketChange
    )} (${printStockPrice(
      stock.data.price.regularMarketChangePercent * 100
    )}%) ${icon}`;
  });
  return rows.join("\n");
};

export function printStockPrice(value: number): string {
  switch (Math.sign(value)) {
    case -1: {
      return `${value.toFixed(2)}`;
    }
    default: {
      return `+${value.toFixed(2)}`;
    }
  }
}

export const isUserName = (str: string): boolean => {
  if (str.startsWith("<@") && str.endsWith(">")) {
    return true;
  }
  return false;
};

export function printWhoWrote(
  quote: Quote,
  inspirational: InspirationalQuote
): string {
  if (Math.random() > 0.5) {
    return `Which quote did George write?\n> a) ${quote.content}\nOR\n> b) ${inspirational.quote}`;
  }

  return `Which quote did George write?\n> a) ${inspirational.quote}\nOR\n> b) ${quote.content}`;
}
