import * as dotenv from "dotenv";
dotenv.config();
import * as Discord from "discord.js";
import { messageHandler } from "./handler";

const client = new Discord.Client();
const IS_DEV = process.env.IS_DEV ? true : false;

console.log(IS_DEV, "is dev");

const settings = {
  activity: IS_DEV ? "[DEV] Serving wisdom" : "Serving wisdom",
  name: "GeorgeBotts",
  token: process.env.DISCORD_TOKEN,
};

(function startBot() {
  try {
    console.log("Starting GeorgeBotts");
    if (IS_DEV) {
      console.log("[DEV] - Using developer settings");
    }
    client.on("ready", () => {
      console.log("GeorgeBotts connected to Discord!");
      client.user?.setActivity(settings.activity);
    });

    client.on("message", (message) => messageHandler(message));
    client.login(settings.token);
  } catch (e) {
    console.error(e);
    process.exit(0);
  }
})();
