import axios from "axios";

export interface AskDuckDuckGoProps {
  query: string;
}

function sanitizeQuery(query: string) {
  // return encodeURIComponent(query);
  return query;
}

function validateQuery(query: string) {
  return query && query.length > 0;
}

export async function askDuckDuckGo(props: AskDuckDuckGoProps) {
  const validatedQuery = validateQuery(props.query);
  if (!validatedQuery) {
    throw new Error("Failed to validate query");
  }
  const baseURL = "https://api.duckduckgo.com/";
  const response = await axios.get(baseURL, {
    params: {
      q: sanitizeQuery(props.query),
      format: "json",
    },
  });

  console.log(response.data);
}
