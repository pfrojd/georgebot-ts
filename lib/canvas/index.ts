import { Quote } from "../types/quote";

let fontSize = 48;
const defaultFontSize = 48;
const minimumFontSize = 32;
const width = 1024;
const height = 768;

export function isFirefox(): boolean {
  if (typeof window !== "undefined") {
    return (
      window?.navigator && window.navigator.userAgent.indexOf("Firefox") > -1
    );
  } else {
    return false;
  }
}

export function getPadding(): number {
  return defaultFontSize;
}

export function getFooterHeight(numberOfLines = 0): number {
  const padding = getPadding();
  if (numberOfLines === 0) {
    return fontSize + padding;
  } else {
    return fontSize * (numberOfLines + 1) + padding * 2;
  }
}

export function addName(ctx: CanvasRenderingContext2D): void {
  ctx.fillStyle = "white";

  const padding = getPadding();
  const maxWidth = width - padding * 2;
  ctx.font = `${28}px IM Fell Great Primer SC`;

  const textWidth = ctx.measureText("- George").width;

  const x = maxWidth - textWidth;
  const y = height - 8;

  ctx.fillText("- George", x, y);
}

export function getRandomQuote(quotes: Quote[]): Quote {
  const randomQuote = quotes[Math.floor(Math.random() * quotes.length)];
  return randomQuote;
}

export function copyImageToClipboard(canvas: HTMLCanvasElement): void {
  canvas.toBlob(function (blob) {
    // @ts-ignore
    const item = new ClipboardItem({ "image/png": blob });
    // @ts-ignore
    navigator.clipboard.write([item]);
  });
}

export function testLineLength(
  ctx: CanvasRenderingContext2D,
  content: string
): number {
  const totalContentWidth = ctx.measureText(content).width;
  const padding = getPadding();
  const allowedWidth = width - padding - padding;
  if (totalContentWidth > allowedWidth) {
    fontSize = fontSize - 2;
    ctx.font = `${fontSize}px IM Fell Great Primer SC`;
    return testLineLength(ctx, content);
  } else {
    return fontSize;
  }
}

export function drawQuote(
  canvas: HTMLCanvasElement,
  quote: Quote,
  image: HTMLImageElement | null
): void {
  fontSize = 48;
  const ctx = canvas.getContext("2d");
  if (!ctx) {
    return;
  }
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  if (image) {
    ctx.drawImage(image, 0, 0);
  }

  ctx.font = `${fontSize}px IM Fell Great Primer SC`;
  ctx.fillStyle = "white";

  // shadows
  ctx.shadowColor = "#eee";
  ctx.shadowOffsetX = 5;
  ctx.shadowOffsetY = 5;
  ctx.shadowBlur = 0;

  ctx.shadowColor = "#333";
  ctx.shadowOffsetX = 2;
  ctx.shadowOffsetY = 2;
  ctx.shadowBlur = 1;

  // let position = getPosition(ctx, quote);

  // 1. Clear the canvas space.
  // 2. Set up defaults.
  // 3. Check length of the quote
  // 4. If it's too long, lower the fontSize until it does.
  // 4b. If it becomes too small we should wrap text instead.
  // 5. Write the quote to the canvas.

  writeText(ctx, quote);
}

export function writeText(ctx: CanvasRenderingContext2D, quote: Quote): void {
  let actualFontSize = testLineLength(ctx, quote.content);
  ctx.fillStyle = "white";
  ctx.font = `${actualFontSize}px IM Fell Great Primer SC`;

  const padding = getPadding();
  const footerHeight = getFooterHeight();
  const maxWidth = width - padding * 2;
  if (actualFontSize < minimumFontSize) {
    actualFontSize = minimumFontSize;
    ctx.font = `${actualFontSize}px IM Fell Great Primer SC`;
  }

  const isOneLiner = ctx.measureText(quote.content).width < maxWidth;
  if (isOneLiner) {
    const x = padding;
    let y = height - footerHeight + actualFontSize / 2;
    createFooter(ctx, 0);
    y = height - actualFontSize;
    ctx.fillText(quote.content, x, y);
    addName(ctx);
    return;
  }
  // We should wrap text instead.
  const quoteContentByArray = quote.content.split(" ");
  let currentLine = "";
  const lines = [];
  let numberOfLines = 0;
  for (const word of quoteContentByArray) {
    // Add words until you break the max width.
    const testLine = currentLine + word + " ";
    const currentWidth = ctx.measureText(currentLine).width;
    if (currentWidth > maxWidth) {
      lines.push(currentLine);
      numberOfLines++;
      currentLine = word + " ";
    } else {
      currentLine = testLine;
    }
  }
  lines.push(currentLine);
  createFooter(ctx, numberOfLines);
  const x = padding;
  let y = height - footerHeight - actualFontSize / (2 * numberOfLines + 1);
  for (const line of lines) {
    ctx.fillText(line, x, y, maxWidth);
    y += actualFontSize * numberOfLines;
  }
  addName(ctx);
}

export function createFooter(
  ctx: CanvasRenderingContext2D,
  numberOfLines: number
): void {
  const footerHeight = getFooterHeight(numberOfLines);
  ctx.fillStyle = "black";
  ctx.fillRect(0, height - footerHeight, width, footerHeight);
  ctx.fillStyle = "white";
}

export function getNewImage(): any {
  const randomNumber = Math.floor(Math.random() * 10000);
  const image = new Image();

  image.src = `https://picsum.photos/${width}/${height}?q=${randomNumber}`;
  image.width = width;
  image.height = height;
  image.id = "quote-image";
  image.crossOrigin = "anonymous";
  return image;
}

export function createQuote(canvas: HTMLCanvasElement, quote: Quote): any {
  const _image = getNewImage();
  _image.onload = function () {
    canvas.width = _image.width;
    canvas.height = _image.height;

    drawQuote(canvas, quote, _image);
  };

  return _image;
}
