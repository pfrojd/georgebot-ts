import { Quote } from "../types/quote";

let fontSize = 48;
const defaultFontSize = 48;
const minimumFontSize = 32;
const width = 1024;
const height = 868;
const fontFamily = "Times New Roman";

const meme = {
  padding: 80,
};

export function isFirefox(): boolean {
  if (typeof window !== "undefined") {
    return (
      window?.navigator && window.navigator.userAgent.indexOf("Firefox") > -1
    );
  } else {
    return false;
  }
}

export function getPadding(): number {
  return defaultFontSize;
}

export function getFooterHeight(numberOfLines = 0): number {
  const padding = getPadding();
  if (numberOfLines === 0) {
    return fontSize + padding;
  } else {
    return fontSize * (numberOfLines + 1) + padding * 2;
  }
}

export function addName(ctx: CanvasRenderingContext2D): void {
  ctx.fillStyle = "white";

  const padding = getPadding();
  const maxWidth = width - padding * 2;
  ctx.font = `${28}px ${fontFamily}`;

  const textWidth = ctx.measureText("- George").width;

  const x = maxWidth - textWidth;
  const y = height - 48;

  ctx.fillText("- George", x, y);
}

export function getRandomQuote(quotes: Quote[]): Quote {
  const randomQuote = quotes[Math.floor(Math.random() * quotes.length)];
  return randomQuote;
}

export function copyImageToClipboard(canvas: HTMLCanvasElement): void {
  canvas.toBlob(function (blob) {
    // @ts-ignore
    const item = new ClipboardItem({ "image/png": blob });
    // @ts-ignore
    navigator.clipboard.write([item]);
  });
}

export function testLineLength(
  ctx: CanvasRenderingContext2D,
  content: string
): number {
  const totalContentWidth = ctx.measureText(content).width;
  const padding = getPadding();
  const allowedWidth = width - padding - padding;
  if (totalContentWidth > allowedWidth) {
    fontSize = fontSize - 2;
    ctx.font = `${fontSize}px ${fontFamily}`;
    return testLineLength(ctx, content);
  } else {
    return fontSize;
  }
}

export function drawQuote(
  canvas: HTMLCanvasElement,
  quote: Quote,
  image: HTMLImageElement | null
): void {
  fontSize = 48;
  const ctx = canvas.getContext("2d");
  if (!ctx) {
    return;
  }
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, width, height);
  ctx.fillStyle = "white";

  if (image) {
    ctx.drawImage(image, meme.padding, meme.padding);
    ctx.strokeStyle = "white";
    ctx.lineWidth = 4;
    ctx.strokeRect(
      meme.padding,
      meme.padding,
      width - meme.padding * 2,
      height - 100 - meme.padding * 2
    );
  }

  ctx.font = `${fontSize}px ${fontFamily}`;
  ctx.fillStyle = "white";
  ctx.textAlign = "center";
  ctx.textBaseline = "middle";

  // shadows
  ctx.shadowColor = "#eee";
  ctx.shadowOffsetX = 5;
  ctx.shadowOffsetY = 5;
  ctx.shadowBlur = 0;

  ctx.shadowColor = "#333";
  ctx.shadowOffsetX = 2;
  ctx.shadowOffsetY = 2;
  ctx.shadowBlur = 1;

  // let position = getPosition(ctx, quote);

  // 1. Clear the canvas space.
  // 2. Set up defaults.
  // 3. Check length of the quote
  // 4. If it's too long, lower the fontSize until it does.
  // 4b. If it becomes too small we should wrap text instead.
  // 5. Write the quote to the canvas.

  writeText(ctx, quote);
}

export function writeText(ctx: CanvasRenderingContext2D, quote: Quote): void {
  quote.content = quote.content.toUpperCase();
  let actualFontSize = testLineLength(ctx, quote.content);
  ctx.fillStyle = "white";
  ctx.font = `${actualFontSize}px ${fontFamily}`;

  const padding = getPadding();
  const maxWidth = width - meme.padding * 2;
  if (actualFontSize < minimumFontSize) {
    actualFontSize = minimumFontSize;
    ctx.font = `${actualFontSize}px ${fontFamily}`;
  }

  const isOneLiner = ctx.measureText(quote.content).width < maxWidth;
  if (isOneLiner) {
    const x = width / 2; // Text align takes care of this;
    const y = height - meme.padding * 1.5;
    ctx.fillText(quote.content, x, y, maxWidth);
    addName(ctx);
    return;
  }
  // We should wrap text instead.
  const quoteContentByArray = quote.content.split(" ");
  let currentLine = "";
  const lines = [];
  let numberOfLines = 0;
  for (const word of quoteContentByArray) {
    // Add words until you break the max width.
    const testLine = currentLine + word + " ";
    const currentWidth = ctx.measureText(currentLine).width;
    if (currentWidth > maxWidth) {
      lines.push(currentLine);
      numberOfLines++;
      currentLine = word + " ";
    } else {
      currentLine = testLine;
    }
  }
  lines.push(currentLine);
  const x = padding;
  let y = height - meme.padding * 1.5;
  for (const line of lines) {
    ctx.fillText(line, width / 2, y);
    y += actualFontSize * numberOfLines;
  }
  addName(ctx);
}

export function getNewImage(): any {
  const randomNumber = Math.floor(Math.random() * 10000);
  const image = new Image();

  const _width = width - meme.padding * 2;
  const _height = 768 - meme.padding * 2;

  image.src = `https://picsum.photos/${_width}/${_height}?q=${randomNumber}`;
  image.width = _width;
  image.height = _height;
  image.id = "quote-image";
  image.crossOrigin = "anonymous";
  return image;
}

export function createQuote(canvas: HTMLCanvasElement, quote: Quote): any {
  const _image = getNewImage();
  _image.onload = function () {
    canvas.width = width;
    canvas.height = height;

    drawQuote(canvas, quote, _image);
  };

  return _image;
}
