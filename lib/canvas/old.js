const path = require("path");
const Canvas = require("canvas");

let fontPath = path.join("lib", "canvas", "fonts", "IMFeGPsc28P.ttf");
let width = 1024;
let height = 768;
let fontSize = 48;
let padding = width / 16;

function wrapText(text, ctx, x, y, maxWidth, lineHeight, drawLines = true) {
  let words = text.split(" ");
  let line = "";
  let numberOfLines = 1;

  for (let word of words) {
    let testLine = line + word + " ";
    let testWidth = ctx.measureText(testLine).width;
    if (testWidth > maxWidth) {
      if (drawLines) {
        ctx.fillText(line, x, y);
      }
      line = word + " ";
      y += lineHeight;
      numberOfLines++;
    } else {
      line = testLine;
    }
  }

  if (drawLines) {
    ctx.fillText(line, x, y);
    addName(ctx, x, y);
  }

  return numberOfLines * fontSize;
}

function addName(ctx, x, y) {
  let textWidth = ctx.measureText("- George").width;
  ctx.fillText("- George", x + textWidth - 80, y + fontSize);
}

function getPosition(ctx, quote) {
  let textWidth = width / 2;
  let textHeight = wrapText(
    quote,
    ctx,
    width / 2,
    fontSize,
    width / 2,
    fontSize,
    false
  );

  let rnd = Math.floor(Math.random() * 4);
  let position = { x: null, y: null };

  switch (rnd) {
    case 0: // Top left
      position.x = padding;
      position.y = padding + fontSize;
      break;
    case 1: // Top right
      position.x = width - textWidth - padding;
      position.y = padding + fontSize;
      break;
    case 2: // Bottom right
      position.x = width - textWidth - padding;
      position.y = height - textHeight - padding;
      break;
    case 3: // Bottom left
      position.x = padding;
      position.y = height - textHeight - padding;
      break;
  }

  positions = {
    0: { x: padding, y: padding + fontSize },
    1: { x: width + padding - textWidth, y: padding + fontSize },
    2: { x: padding - textWidth, y: -padding - fontSize },
    3: { x: padding, y: -padding - fontSize },
  };

  return position;
}

function drawQuote(canvas, quote) {
  let ctx = canvas.getContext("2d");
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(image, 0, 0);

  ctx.font = `${fontSize}px IM Fell Great Primer SC`;
  ctx.fillStyle = "white";

  // shadows
  ctx.shadowColor = "#eee";
  ctx.shadowOffsetX = 5;
  ctx.shadowOffsetY = 5;
  ctx.shadowBlur = 0;

  ctx.shadowColor = "#333";
  ctx.shadowOffsetX = 2;
  ctx.shadowOffsetY = 2;
  ctx.shadowBlur = 1;

  let position = getPosition(ctx, quote.content);
  wrapText(
    quote.content,
    ctx,
    position.x,
    position.y,
    width / 2,
    fontSize,
    true
  );
}

function getNewImage() {
  let randomNumber = Math.floor(Math.random() * 10000);

  image.src = `https://picsum.photos/${width}/${height}?q=${randomNumber}`;
  image.width = width;
  image.height = height;
  image.id = "quote-image";
  image.crossOrigin = "anonymous";
  return image;
}

function createQuote(canvas, quote) {
  let _image = getNewImage();
  _image.onload = function () {
    canvas.width = image.width;
    canvas.height = image.height;

    drawQuote(canvas, quote);
  };
}

async function createQuoteCanvas(quote) {
  Canvas.registerFont(fontPath, { family: "Quote" });
  let randomNumber = Math.floor(Math.random() * 10000);
  let canvas = Canvas.createCanvas(width, height);
  let ctx = canvas.getContext("2d");

  ctx.clearRect(0, 0, canvas.width, canvas.height);

  let image = await Canvas.loadImage(
    `https://picsum.photos/seed/${randomNumber}/${width}/${height}`
  );
  ctx.drawImage(image, 0, 0);

  ctx.font = `${fontSize}px Quote`;
  ctx.fillStyle = "white";

  ctx.shadowColor = "#eee";
  ctx.shadowOffsetX = 5;
  ctx.shadowOffsetY = 5;
  ctx.shadowBlur = 0;

  ctx.shadowColor = "#333";
  ctx.shadowOffsetX = 2;
  ctx.shadowOffsetY = 2;
  ctx.shadowBlur = 1;

  let position = getPosition(ctx, quote.content);
  wrapText(
    quote.content,
    ctx,
    position.x,
    position.y,
    width / 2,
    fontSize,
    true
  );
  return canvas;
}

module.exports = {
  createQuoteCanvas,
};
