import { Screenshot } from "../types/screenshot";
import Canvas, { loadImage } from "canvas";
import fs from "fs";
import path from "path";
import { Quote } from "../types/quote";
import { addName, writeText as writeMemeText } from "./meme";
import { createQuoteCanvas as oldQuoteCanvas } from "./old";
import _ from "lodash";

const IS_DEV = process.env.IS_DEV ? true : false;

const { readFile } = fs.promises;

// let imgPath = path.resolve(process.cwd(), "img");

const sunnyFont = path.join("lib", "canvas", "fonts", "Textile.ttf");
Canvas.registerFont(sunnyFont, { family: "Textile" });

const fontSize = 48;
// let width = 1024;
// let padding = width / 16;
// let height = 768;

const meme = {
  padding: 80,
};

const cursed = {
  initialFontSize: 48,
  fontSize: 48,
  minimumFontSize: 32,
  width: 1024,
  height: 868,
  padding: 80,
  extraTextArea: 100,
  fontFamily: "Times New Roman",
};

const sunny = {
  initialFontSize: 42,
  fontSize: 42,
  minimumFontSize: 28,
  width: 1024,
  height: 868,
  padding: 80,
  extraTextArea: 100,
  fontFamily: "Textile",
};

function resetFontStates() {
  return;
}

export async function createQuoteCanvas(quote: Quote): Promise<Canvas.Canvas> {
  const canvas = await oldQuoteCanvas(quote);
  return canvas;
}

export function testLineLength(
  ctx: CanvasRenderingContext2D,
  quote: string,
  size: { width: number; padding: number; fontSize: number; fontFamily: string }
): any {
  const totalContentWidth = ctx.measureText(quote).width;
  const allowedWidth = size.width - size.padding - size.padding;
  if (totalContentWidth > allowedWidth) {
    size.fontSize = size.fontSize - 2;
    ctx.font = `${size.fontSize}px ${size.fontFamily}`;
    return testLineLength(ctx, quote, size);
  } else {
    return size.fontSize;
  }
}

// function testSunnyLineLength(
//   ctx: CanvasRenderingContext2D,
//   quote: string,
//   size: any
// ): any {
//   const totalContentWidth = ctx.measureText(quote).width;
//   const allowedWidth = size.width - size.padding - size.padding;
//   if (totalContentWidth > allowedWidth) {
//     size.fontSize = size.fontSize - 2;
//     ctx.font = `${size.fontSize}px ${size.fontFamily}`;
//     return testSunnyLineLength(ctx, quote, size);
//   } else {
//     return size.fontSize;
//   }
// }

export function splitLongQuoteIntoLines(
  ctx: CanvasRenderingContext2D,
  quote: { content: string },
  maxWidth: number
): string[] {
  const quoteContentByArray = quote.content.split(" ");
  let currentLine = "";
  const lines = [];
  for (const word of quoteContentByArray) {
    // Add words until you break the max width.
    const testLine = currentLine + word + " ";
    const currentWidth = ctx.measureText(currentLine).width;
    if (currentWidth > maxWidth) {
      lines.push(currentLine);
      currentLine = word + " ";
    } else {
      currentLine = testLine;
    }
  }
  lines.push(currentLine);
  return lines;
}

export async function createCursedCanvas(
  quote: Quote,
  screenshot: Screenshot
): Promise<Canvas.Canvas> {
  // Set up the canvas with the standard size of the cursed meme.
  const canvas = Canvas.createCanvas(cursed.width, cursed.height);
  const ctx = canvas.getContext("2d");

  // Draw a full black rectangle across the entire canvas, this is the background.
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, cursed.width, cursed.height);

  // Reset the fillStyle back to white, for the image border.
  // ctx.fillStyle = "white";

  // Calculate the box of the image. We have the padding set and just need
  // to add it up for each side of the image.
  const imageBoxWidth = cursed.width - cursed.padding * 2;
  const boxHeight = cursed.height - cursed.extraTextArea - cursed.padding * 2;

  let image = new Canvas.Image();

  // Quick DEV check to avoid loading local files in dev environment.
  if (IS_DEV) {
    const newPath = screenshot.path.replace(
      "/mnt/volume_ams3_01/images/screenshots/",
      "https://zaifon.dev/screenshots/"
    );
    image = await loadImage(newPath);
  } else {
    console.log("Reading file from path", screenshot.path);
    const imageBuffer = await readFile(screenshot.path);
    image.src = imageBuffer;
  }

  // Draw the actual image, starting coordinate for the image is the top right
  // because of the padding we've previously set.
  ctx.drawImage(
    image,
    cursed.padding,
    cursed.padding,
    imageBoxWidth,
    boxHeight
  );

  // Now we need to draw the border of the image, set up the strokeStyle and lineWidth.
  ctx.strokeStyle = "white";
  ctx.lineWidth = 4;

  // Draw a stroke-rectangle (a transparent rectangle with a border)
  ctx.strokeRect(cursed.padding, cursed.padding, imageBoxWidth, boxHeight);

  // Prepare the content by upper-casing it all.
  quote.content = quote.content.toUpperCase();

  // Set up the defaults for the font rendering.
  ctx.fillStyle = "white";
  ctx.textAlign = "center";
  ctx.textBaseline = "middle";
  ctx.font = `${cursed.fontSize}px ${cursed.fontFamily}`;

  // If the quote is a oneliner, we can just print it in the appropriate place.
  const isOneLiner = ctx.measureText(quote.content).width < imageBoxWidth;
  if (isOneLiner) {
    const x = cursed.width / 2; // Text align takes care of this;

    // We know the total height of the image is cursed.height
    // We remove the padding, the extra text area, y will be at the very bottom of the image.
    // Now add the fontSize to account for the size of it (so the text won't clip th eimage)
    const y =
      cursed.height - cursed.padding - cursed.extraTextArea + cursed.fontSize;

    ctx.font = `${cursed.fontSize}px ${cursed.fontFamily}`;
    ctx.fillText(quote.content, x, y, imageBoxWidth);
    addName(ctx);
    return canvas;
  }

  // It's not a one-liner, so we need to split it into lines, we split by word
  // and measure the line with after adding each word (to reach max capacity)
  const lines = splitLongQuoteIntoLines(ctx, quote, imageBoxWidth);

  // We now need to get the longest line, to calculate the actual font size.
  const longestLine = _.maxBy(lines, (line) => line.length);

  // We have the longest line, now test the line length of it, which will continously
  // reduce the fontSize until it can fit all content.
  const actualFontSize = testLineLength(
    ctx,
    longestLine ?? quote.content,
    cursed
  );

  // We know the total height of the image is cursed.height
  // We remove the padding, the extra text area, y will be at the very bottom of the image.
  // Now add the fontSize to account for the size of it (so the text won't clip th eimage)
  let y =
    cursed.height - cursed.padding - cursed.extraTextArea + actualFontSize;

  ctx.font = `${actualFontSize}px ${cursed.fontFamily}`;

  // We have the starting location of the text, now iterate through the lines.
  for (const line of lines) {
    // Print the line, at the center of the image, with the newly calculated y.
    ctx.fillText(line, cursed.width / 2, y);

    // For each line, increment y with the fontSize, adjust this if we want more padding between lines.
    y += actualFontSize;
  }

  // Lastly, we need to add the Name to the quote.
  addName(ctx);

  // Almost lastly, we need to reset the fontSize back to the initial value, because the testLineLength
  // will modify this value.
  cursed.fontSize = cursed.initialFontSize;
  return canvas;
}

export async function createSunnyCanvas(
  quote: Quote,
  sunny_image_path: string
): Promise<Canvas.Canvas> {
  // Set up the canvas with the standard size of the cursed meme.
  const canvas = Canvas.createCanvas(sunny.width, sunny.height);
  const ctx = canvas.getContext("2d");

  // Draw a full black rectangle across the entire canvas, this is the background.
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, sunny.width, sunny.height);

  // Calculate the box of the image. We have the padding set and just need
  // to add it up for each side of the image.
  const imageBoxWidth = sunny.width - sunny.padding * 2;
  const boxHeight = sunny.height - sunny.extraTextArea - sunny.padding * 2;

  const image = new Canvas.Image();
  const imageBuffer = await readFile(sunny_image_path);
  image.src = imageBuffer;

  // Draw the actual image, starting coordinate for the image is the top right
  // because of the padding we've previously set.
  ctx.drawImage(image, sunny.padding, sunny.padding, imageBoxWidth, boxHeight);

  // Now we need to draw the border of the image, set up the strokeStyle and lineWidth.
  ctx.strokeStyle = "white";
  ctx.lineWidth = 4;

  // Draw a stroke-rectangle (a transparent rectangle with a border)
  ctx.strokeRect(sunny.padding, sunny.padding, imageBoxWidth, boxHeight);

  // Prepare the content by upper-casing it all.
  quote.content = quote.content.toUpperCase();

  // Set up the defaults for the font rendering.
  ctx.fillStyle = "white";
  ctx.textAlign = "center";
  ctx.textBaseline = "middle";
  ctx.font = `${sunny.fontSize}px ${sunny.fontFamily}`;

  // If the quote is a oneliner, we can just print it in the appropriate place.
  const isOneLiner = ctx.measureText(quote.content).width < imageBoxWidth;
  if (isOneLiner) {
    const x = sunny.width / 2; // Text align takes care of this;

    // We know the total height of the image is sunny.height
    // We remove the padding, the extra text area, y will be at the very bottom of the image.
    // Now add the fontSize to account for the size of it (so the text won't clip th eimage)
    const y =
      sunny.height - sunny.padding - sunny.extraTextArea + sunny.fontSize;

    ctx.font = `${sunny.fontSize}px ${sunny.fontFamily}`;
    ctx.fillText(quote.content, x, y, imageBoxWidth);
    addName(ctx);

    console.log(imageBoxWidth, boxHeight);
    return canvas;
  }

  // It's not a one-liner, so we need to split it into lines, we split by word
  // and measure the line with after adding each word (to reach max capacity)
  const lines = splitLongQuoteIntoLines(ctx, quote, imageBoxWidth);

  // We now need to get the longest line, to calculate the actual font size.
  const longestLine = _.maxBy(lines, (line) => line.length);

  // We have the longest line, now test the line length of it, which will continously
  // reduce the fontSize until it can fit all content.
  const actualFontSize = testLineLength(
    ctx,
    longestLine ?? quote.content,
    sunny
  );

  // Almost lastly, we need to reset the fontSize back to the initial value, because the testLineLength
  // will modify this value.
  sunny.fontSize = sunny.initialFontSize;

  // We know the total height of the image is sunny.height
  // We remove the padding, the extra text area, y will be at the very bottom of the image.
  // Now add the fontSize to account for the size of it (so the text won't clip th eimage)
  let y = sunny.height - sunny.padding - sunny.extraTextArea + actualFontSize;
  ctx.font = `${actualFontSize}px ${sunny.fontFamily}`;

  // We have the starting location of the text, now iterate through the lines.
  for (const line of lines) {
    // Print the line, at the center of the image, with the newly calculated y.
    ctx.fillText(line, sunny.width / 2, y);

    // For each line, increment y with the fontSize, adjust this if we want more padding between lines.
    // Adjust fontSize with 4 because the Sunny font is too large
    y += actualFontSize + 8;
  }

  // Lastly, we need to add the Name to the quote.
  addName(ctx);

  return canvas;
}

export async function createMemeCanvas(quote: Quote): Promise<Canvas.Canvas> {
  const randomNumber = Math.floor(Math.random() * 10000);
  const canvas = Canvas.createCanvas(1024, 868);
  const ctx = canvas.getContext("2d");

  const _width = 1024 - meme.padding * 2;
  const _height = 768 - meme.padding * 2;

  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = "black";
  ctx.fillRect(0, 0, 1024, 868);
  ctx.fillStyle = "white";

  const image = await Canvas.loadImage(
    `https://picsum.photos/${_width}/${_height}?q=${randomNumber}`
  );
  ctx.drawImage(image, meme.padding, meme.padding);
  ctx.strokeStyle = "white";
  ctx.lineWidth = 4;
  ctx.strokeRect(
    meme.padding,
    meme.padding,
    1024 - meme.padding * 2,
    868 - 100 - meme.padding * 2
  );

  ctx.font = `${fontSize}px Times New Roman`;
  ctx.fillStyle = "white";
  ctx.textAlign = "center";
  ctx.textBaseline = "middle";

  // shadows
  ctx.shadowColor = "#eee";
  ctx.shadowOffsetX = 5;
  ctx.shadowOffsetY = 5;
  ctx.shadowBlur = 0;

  ctx.shadowColor = "#333";
  ctx.shadowOffsetX = 2;
  ctx.shadowOffsetY = 2;
  ctx.shadowBlur = 1;

  writeMemeText(ctx, quote);
  return canvas;
}
