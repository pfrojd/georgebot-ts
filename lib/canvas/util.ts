interface RandomPositionForTextOptions {
  padding: number;
  fontSize: number;
  width: number;
  height: number;
  maxWidth: number;
}

interface Position {
  x: number;
  y: number;
}

export function getRandomPositionForText(
  ctx: CanvasRenderingContext2D,
  text: string,
  options: RandomPositionForTextOptions
): Position {
  const { padding, fontSize, width, height, maxWidth } = options;
  const textWidth = width / 2;

  const lines = splitLongTextIntoLines(ctx, text, maxWidth);
  const textHeight = lines.length * fontSize;

  const rnd = Math.floor(Math.random() * 4);
  const position: Position = { x: 0, y: 0 };

  switch (rnd) {
    case 0: // Top left
      position.x = padding;
      position.y = padding + fontSize;
      break;
    case 1: // Top right
      position.x = width - textWidth - padding;
      position.y = padding + fontSize;
      break;
    case 2: // Bottom right
      position.x = width - textWidth - padding;
      position.y = height - textHeight - padding;
      break;
    case 3: // Bottom left
      position.x = padding;
      position.y = height - textHeight - padding;
      break;
  }

  return position;
}

export function splitLongTextIntoLines(
  ctx: CanvasRenderingContext2D,
  text: string,
  maxWidth: number
): string[] {
  const textContent = text.split(" ");
  let currentLine = "";
  const lines = [];
  for (const word of textContent) {
    // Add words until you break the max width.
    const testLine = currentLine + word + " ";
    const currentWidth = ctx.measureText(currentLine).width;
    if (currentWidth > maxWidth) {
      lines.push(currentLine);
      currentLine = word + " ";
    } else {
      currentLine = testLine;
    }
  }
  lines.push(currentLine);
  return lines;
}

export function addName(
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  fontSize: number
): void {
  const textWidth = ctx.measureText("- George").width;
  ctx.fillText("- George", x + textWidth - 80, y + fontSize);
}
