import { Canvas } from "canvas";

const dimensions = {
  width: 400,
  height: 200,
};

const font = {
  title: 28,
  subtitle: 20,
  family: "Roboto",
  padding: 8,
};

function getDir(radian: number, width: number, height: number) {
  radian += Math.PI;
  const HALF_WIDTH = width * 0.5;
  const HALF_HEIGHT = height * 0.5;
  const lineLength =
    Math.abs(width * Math.sin(radian)) + Math.abs(height * Math.cos(radian));
  const HALF_LINE_LENGTH = lineLength / 2;

  const x0 = HALF_WIDTH + Math.sin(radian) * HALF_LINE_LENGTH;
  const y0 = HALF_HEIGHT - Math.cos(radian) * HALF_LINE_LENGTH;
  const x1 = width - x0;
  const y1 = height - y0;

  return { x0, x1, y0, y1 };
}

export async function createWhatAWeekCanvas(
  dateString: string
): Promise<Canvas> {
  const canvas = new Canvas(dimensions.width, dimensions.height);
  const ctx = canvas.getContext("2d");

  const angle = 135;
  const direction = getDir(angle, dimensions.width, dimensions.height);

  const gradient = ctx.createLinearGradient(
    direction.x0,
    direction.y0,
    direction.x1,
    direction.y1
  );
  gradient.addColorStop(0, "#f5f7fa");
  gradient.addColorStop(1, "#c3cfe2");

  ctx.fillStyle = gradient;
  ctx.fillRect(0, 0, dimensions.width, dimensions.height);

  ctx.fillStyle = "#555";
  ctx.textAlign = "center";
  ctx.textBaseline = "middle";
  ctx.font = `${font.title}px ${font.family}`;

  const x = dimensions.width / 2;
  let y = dimensions.height / 2 - font.title / 2;

  ctx.fillText('"What a fucking week"', x, y);

  // Do next line
  y += font.title + font.padding;
  ctx.font = `${font.subtitle}px ${font.family}`;
  ctx.fillText(dateString, x, y);

  return canvas;
}
