import { sql } from "slonik";
import pool from "./index";
import { Cage } from "../../lib/types/cage";

export interface InsertCageProps {
  alt: string;
  path: string;
  original_url: string;
  filename: string;
  width: number;
  height: number;
}

export async function insertCage({
  alt,
  path,
  original_url,
  filename,
  width,
  height,
}: InsertCageProps): Promise<void> {
  return await pool.connect(async (c) => {
    const statement = sql`
    INSERT INTO cage
    (alt, path, original_url, filename, width, height)
    VALUES
    (${alt}, ${path}, ${original_url}, ${filename}, ${width}, ${height})
  `;

    await c.query(statement);
    return;
  });
}

export async function insertCages(cages: InsertCageProps[]): Promise<void> {
  return await pool.connect(async (c) => {
    const chunk = cages.map((cage: any) => Object.values(cage));
    const statement = sql`
    INSERT INTO cage
    (alt, path, original_url, filename, width, height)
    SELECT *
    FROM ${sql.unnest(chunk, [
      "text",
      "text",
      "text",
      "text",
      "number",
      "number",
    ])}
  `;

    await c.query(statement);
    return;
  });
}

export async function getRandomCage(): Promise<Cage | null> {
  return await pool.connect(async (c) => {
    return await c.maybeOne(
      sql`
        SELECT *
        FROM cage
        ORDER BY RANDOM()
        LIMIT 1
      `
    );
  });
}
