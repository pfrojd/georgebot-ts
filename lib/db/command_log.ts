import { CommandLog } from "../../lib/types/command_log";
import { sql } from "slonik";
import pool from "./index";

export interface InsertCommandLogProps {
  discord_message_id: string;
  discord_user_id: string;
  discord_username: string;
  command: string;
  command_timestamp: Date;
  message_content: string;
}

export async function insertCommandLog({
  discord_message_id,
  discord_user_id,
  discord_username,
  command,
  command_timestamp,
  message_content,
}: InsertCommandLogProps): Promise<void> {
  return await pool.connect(async (c) => {
    const statement = sql`
      INSERT INTO command_log
      (discord_message_id, discord_user_id, discord_username, command, command_timestamp, message_content)
      VALUES
      (${discord_message_id}, ${discord_user_id}, ${discord_username}, ${command}, TO_TIMESTAMP(${Math.round(
      command_timestamp.getTime() / 1000
    )}), ${message_content})
    `;

    await c.query(statement);
  });
}

export async function insertCommandLogs(logs: InsertCommandLogProps[]) {
  return await pool.connect(async (c) => {
    const chunk = logs.map((l) => Object.values(l));
    const timestampedChunks = chunk.map((c) => {
      // c[4] = `TO_TIMESTAMP(${Math.round(c[4].getTime() / 1000)})`;
      c[4] = Math.round(c[4].getTime() / 1000).toString();
      return c;
    });

    const statement = sql`
        INSERT INTO command_log
        (command, discord_message_id, discord_user_id, discord_username, command_timestamp, message_content)
        SELECT *
        FROM ${sql.unnest(timestampedChunks, [
          "text",
          "int8",
          "int8",
          "text",
          "text",
          "text",
        ])}
      `;

    await c.query(statement);
  });
}

export async function getAllCommandLogs() {
  return await pool.connect(async (c) => {
    const statement = sql`
      SELECT *
      FROM command_log
      ORDER by id asc
    `;

    return await c.many(statement);
  });
}
