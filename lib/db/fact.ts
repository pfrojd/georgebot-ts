import { Fact } from "../../lib/types/fact";
import { sql } from "slonik";
import pool from "./index";

export interface InsertFactProps {
  message: string;
  content: string;
  type: string;
  date: string;
}

export async function insertFact({
  message,
  content,
  type,
  date,
}: InsertFactProps): Promise<void> {
  return await pool.connect(async (c) => {
    const statement = sql`
    INSERT INTO fact
    (message, content, type, date)
    VALUES
    (${message}, ${content}, ${type}, ${date})
  `;

    await c.query(statement);
    return;
  });
}

export async function insertFacts(facts: InsertFactProps[]): Promise<void> {
  return await pool.connect(async (c) => {
    const chunk = facts.map((fact: any) => Object.values(fact));
    const statement = sql`
    INSERT INTO fact
    (type, content, message, date)
    SELECT *
    FROM ${sql.unnest(chunk, ["text", "text", "text", "date"])}
  `;

    await c.query(statement);
    return;
  });
}

export async function getRandomFactForDate(
  day: number,
  month: number
): Promise<Fact | null> {
  return await pool.connect(async (c) => {
    return await c.maybeOne(
      sql`
        SELECT *
        FROM fact
        WHERE EXTRACT(MONTH FROM date) = ${month}
        AND EXTRACT(DAY FROM date) = ${day}
        AND type = 'EVENT'
        ORDER BY RANDOM()
        LIMIT 1
      `
    );
  });
}

export async function getRandomFactForToday(): Promise<Fact> {
  const today = new Date();

  const fact = await getRandomFactForDate(
    today.getDate(),
    today.getMonth() + 1
  );

  if (!fact) {
    throw new Error("Failed to get fact for today");
  }

  return fact;
}
