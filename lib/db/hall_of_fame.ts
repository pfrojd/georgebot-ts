import { HallOfFame } from "../../lib/types/hall_of_fame";
import { sql } from "slonik";
import pool from "./index";

export interface InsertHallOfFameProps {
  discord_id: string;
  discord_username: string;
  path: string;
  quote_content: string;
  original_url: string;
}

export async function insertHallOfFame({
  discord_id,
  discord_username,
  quote_content,
  path,
  original_url,
}: InsertHallOfFameProps): Promise<void> {
  return await pool.connect(async (c) => {
    const statement = sql`
    INSERT INTO hall_of_fame
    (discord_id, discord_username, quote_content, path, original_url)
    VALUES
    (${discord_id}, ${discord_username}, ${quote_content}, ${path}, ${original_url})
  `;

    await c.query(statement);
    return;
  });
}

export async function getRandomHallOfFame(): Promise<HallOfFame> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT *
        FROM hall_of_fame
        ORDER BY RANDOM()
        LIMIT 1
      `
    );
  });
}
