import { createPool, createTypeParserPreset } from "slonik";

const database = process.env.PG_DATABASE;
const user = process.env.PG_USER;
const password = process.env.PG_PASSWORD;
const port = process.env.PG_PORT;

export default createPool(
  `postgres://${user}:${password}@localhost:${port}/${database}`,
  {
    typeParsers: [...createTypeParserPreset()],
  }
);
