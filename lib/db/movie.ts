import { Movie } from "../../lib/types/movie";
import { sql } from "slonik";
import pool from "./index";

export interface AddMovieProps {
  omdb_id: string;
  rating: number;
  data: any;
}

export async function addMovie({
  omdb_id,
  rating,
  data,
}: AddMovieProps): Promise<void> {
  return await pool.connect(async (c) => {
    const statement = sql`
      INSERT INTO movie
      (omdb_id, rating, data)
      VALUES
      (${omdb_id}, ${rating}, ${sql.json(data as any)})
    `;

    await c.query(statement);
    return;
  });
}

export async function getMovieByOmdbId(omdb_id: string): Promise<Movie | null> {
  return await pool.connect(async (c) => {
    return await c.maybeOne(
      sql`
        SELECT *
        FROM movie
        WHERE omdb_id = ${omdb_id}
      `
    );
  });
}

export async function getAllMovies(): Promise<Movie[]> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT *
        FROM movie
      `
    );
  });
}

function getImdbRating(movie: Movie) {
  const imdbRating = movie.data.ratings.find(
    (m: any) => m.source == "Internet Movie Database"
  );

  if (!imdbRating) {
    return null;
  }

  return (
    Number(imdbRating.value.substring(0, imdbRating.value.indexOf("/"))) * 10
  );
}

function getMetaCriticRating(movie: Movie) {
  const metaCriticRating = movie.data.ratings.find(
    (m: any) => m.source == "Metacritic"
  );

  if (!metaCriticRating) {
    return null;
  }

  return metaCriticRating.value.substring(
    0,
    metaCriticRating.value.indexOf("/")
  );
}

function getRottenTomatoesRating(movie: Movie) {
  const rottenTomatoesRating = movie.data.ratings.find(
    (m: any) => m.source == "Rotten Tomatoes"
  );

  if (!rottenTomatoesRating) {
    return null;
  }

  return rottenTomatoesRating.value.substring(
    0,
    rottenTomatoesRating.value.indexOf("%")
  );
}

// [{"value": "8.5/10", "source": "Internet Movie Database"}, {"value": "77%", "source": "Rotten Tomatoes"}, {"value": "67/100", "source": "Metacritic"}]
export async function updateRatingsOnMovie(movie: Movie): Promise<void> {
  return await pool.connect(async (c) => {
    const imdbRating = getImdbRating(movie);
    const metaCriticRating = getMetaCriticRating(movie);
    const rottenTomatoesRating = getRottenTomatoesRating(movie);
    console.log(
      movie.data.title,
      imdbRating,
      metaCriticRating,
      rottenTomatoesRating
    );

    await c.query(
      sql`
        UPDATE movie
        SET imdb_rating = ${imdbRating},
        metacritic_rating = ${metaCriticRating},
        rotten_tomatoes_rating = ${rottenTomatoesRating}
        WHERE id = ${movie.id}
      `
    );
  });
}

export async function updateYearOnMovie(movie: Movie): Promise<void> {
  return await pool.connect(async (c) => {
    const year = movie.data.year;

    await c.query(
      sql`
        UPDATE movie
        SET year = ${year}
        WHERE id = ${movie.id}
      `
    );
  });
}
