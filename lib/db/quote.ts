import { sql } from "slonik";
import { InspirationalQuote, Quote } from "../types/quote";
import pool from "./index";

export async function getAllQuotes(): Promise<Quote[]> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT id, created_at, deleted_at, content, views
        FROM quote
        WHERE deleted_at is null
        ORDER BY id DESC;
      `
    );
  });
}

export async function getQuoteById(id: number): Promise<Quote | null> {
  return await pool.connect(async (c) => {
    return convertResultToQuote(
      await c.maybeOne(
        sql`
        SELECT id, created_at, deleted_at, content, views
        FROM quote
        WHERE deleted_at is null
        AND id = ${id};
      `
      )
    );
  });
}

export async function getRandomQuote(): Promise<Quote> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
      SELECT id, created_at, deleted_at, content, views
        FROM quote
        WHERE deleted_at is null
        ORDER BY RANDOM()
        LIMIT 1
      `
    );
  });
}

export async function increaseViewOnQuote(quote: Quote): Promise<void> {
  return await pool.connect(async (c) => {
    await c.query(
      sql`
        UPDATE quote
        SET views = views + 1
        WHERE quote.id = ${quote.id}
      `
    );
  });
}

export async function getActuallyRandomQuote(): Promise<Quote> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT quote.*
        FROM quote,
          (
            SELECT MIN(views) AS view
            FROM quote
            WHERE deleted_at is null
          ) min
        WHERE deleted_at is null
        AND views = min.view
        ORDER BY RANDOM()
        LIMIT 1
      `
    );
  });
}

export async function getLowestViewOfQuotes() {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT min(views)
        WHERE deleted_at is null
        FROM quote
      `
    );
  });
}

export async function getLatestQuote(): Promise<Quote> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT quote.*
        FROM quote
        WHERE deleted_at is null
        ORDER BY id DESC
        LIMIT 1
      `
    );
  });
}

export async function addQuote(quote: string): Promise<Quote> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        INSERT INTO quote
        (content) VALUES (${quote})
        RETURNING *
      `
    );
  });
}

export async function editQuote(quote: Quote): Promise<void> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        UPDATE quote
        SET content = ${quote.content}
        WHERE id = ${quote.id}
        RETURNING *
      `
    );
  });
}

export async function disableQuote(id: number): Promise<string> {
  return await pool.connect(async (c) => {
    const exists = await c.one(
      sql`
        SELECT quote.*
        FROM quote
        WHERE id = ${id};
      `
    );

    if (exists) {
      if (!exists.deleted_at) {
        await c.query(
          sql`
            UPDATE quote
            SET deleted_at = now()
            WHERE id = ${exists.id}
          `
        );
        return `Disabled quote (${exists.id}), to enable it just repeat this command.`;
      } else {
        await c.query(
          sql`
            UPDATE quote
            SET deleted_at = null
            WHERE id = ${exists.id}
          `
        );
        return `Enabled quote (${exists.id})`;
      }
    }

    return `Could not find the quote (${id}) :(`;
  });
}

export async function searchForQuote(query: string): Promise<Quote | null> {
  return await pool.connect(async (c) => {
    const quote = convertResultToQuote(
      await c.maybeOne(sql`
      SELECT *
      FROM quote
      WHERE content ~* ${"(\\m" + query + "\\M)"}
      AND deleted_at is null
      ORDER BY RANDOM()
      LIMIT 1
    `)
    );

    return quote;
  });
}

export async function searchForQuoteWithFullText(
  query: string[]
): Promise<Quote | null> {
  return await pool.connect(async (c) => {
    const completeWithOrTokens =
      query.length > 1 ? query.join(" | ") : query[0];
    const quote = convertResultToQuote(
      await c.maybeOne(
        sql`
          SELECT *
          FROM quote
          WHERE tsv @@ TO_TSQUERY(${completeWithOrTokens})
          AND deleted_at is null
          ORDER BY RANDOM()
          LIMIT 1;
        `
      )
    );

    return quote;
  });
}

function convertResultToQuote(value?: any): Quote | null {
  if (!value) {
    return null;
  }

  return {
    id: value.id,
    content: value.content,
    created_at: value.created_at,
    views: value.views,
  };
}

export async function insertInspirationalQuote(
  quotes: InspirationalQuote[]
): Promise<void> {
  return await pool.connect(async (c) => {
    const chunk = quotes.map((fact: any) => Object.values(fact));
    const statement = sql`
    INSERT INTO inspirational
    (quote, author, slug_quote, slug_author)
    SELECT *
    FROM ${sql.unnest(chunk, ["text", "text", "text", "text"])}
  `;

    await c.query(statement);
  });
}

export async function getRandomInspirationalQuote(): Promise<
  InspirationalQuote | undefined
> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT quote, author
        FROM inspirational
        ORDER BY RANDOM()
        LIMIT 1
      `
    );
  });
}
