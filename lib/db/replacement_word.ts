import { ReplacementWord } from "../../lib/types/replacement_word";
import { sql } from "slonik";
import pool from "./index";

export interface AddReplacementWordProps {
  word: string;
  discord_id: string;
  discord_username: string;
}

export async function addReplacementWord({
  word,
  discord_id,
  discord_username,
}: AddReplacementWordProps): Promise<void> {
  return await pool.connect(async (c) => {
    const statement = sql`
      INSERT INTO replacement_word
      (word, discord_id, discord_username)
      VALUES
      (${word}, ${discord_id}, ${discord_username})
    `;

    await c.query(statement);
  });
}

export async function getAllReplacementWords(): Promise<ReplacementWord[]> {
  return await pool.connect(async (c) => {
    const statement = sql`
      SELECT *
      FROM replacement_word
      WHERE deleted_at is null
    `;

    return await c.many<ReplacementWord>(statement);
  });
}

export async function disableReplacementWord({
  id,
}: {
  id: number;
}): Promise<string> {
  return await pool.connect(async (c) => {
    const exists = await c.maybeOne<ReplacementWord>(sql`
      SELECT *
      FROM replacement_word
      WHERE id = ${id}
    `);

    if (exists) {
      if (!exists.deleted_at) {
        await c.query(sql`
        UPDATE replacement_word
        SET deleted_at = now()
        WHERE id = ${id}
      `);

        return `Disabled word (${exists.word}), to enable it just repeat this command`;
      } else {
        await c.query(sql`
        UPDATE replacement_word
        SET deleted_at = null
        WHERE id = ${id}
      `);

        return `Enabled word (${exists.word})`;
      }
    }
    return `Replacement word with id (${id} does not exist)`;
  });
}
