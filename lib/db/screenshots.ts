import { Screenshot } from "../../lib/types/screenshot";
import { sql } from "slonik";
import pool from "./index";

export interface InsertScreenShotProps {
  discord_id: string;
  discord_username: string;
  path: string;
  comment: string;
  width: number;
  height: number;
  original_url: string;
}

export async function insertScreenshot({
  discord_id,
  discord_username,
  path,
  comment,
  width,
  height,
  original_url,
}: InsertScreenShotProps): Promise<void> {
  return await pool.connect(async (c) => {
    const statement = sql`
    INSERT INTO screenshot
    (discord_id, discord_username, path, comment, width, height, original_url)
    VALUES
    (${discord_id}, ${discord_username}, ${path}, ${comment}, ${width}, ${height}, ${original_url})
  `;

    await c.query(statement);
  });
}

export async function getRandomScreenshot(): Promise<Screenshot> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT id, discord_id, discord_username, created_at, updated_at, comment, width, height, original_url, enabled, path
        FROM screenshot
        WHERE enabled = true
        ORDER BY RANDOM()
        LIMIT 1
      `
    );
  });
}

export async function getScreenshotByPath(
  filePath: string
): Promise<Screenshot | null> {
  return await pool.connect(async (c) => {
    return await c.maybeOne(
      sql`
        SELECT *
        FROM screenshot
        WHERE path = ${filePath}
      `
    );
  });
}

export async function getScreenshotById(
  id: number
): Promise<Screenshot | null> {
  return await pool.connect(async (c) => {
    return await c.maybeOne(
      sql`
        SELECT *
        FROM screenshot
        WHERE id = ${id}
      `
    );
  });
}

export async function removeScreenshotByPath(filePath: string): Promise<void> {
  return await pool.connect(async (c) => {
    c.query(
      sql`
        DELETE FROM screenshot
        WHERE filePath = ${filePath}
      `
    );
  });
}

export async function getAllScreenshots(): Promise<Screenshot[]> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT *
	FROM screenshot
	ORDER BY id
      `
    );
  });
}
