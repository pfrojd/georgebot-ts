import { sql } from "slonik";
import pool from "./index";

export async function getCommandLogStatsByUsername() {
  return pool.connect(async (c) => {
    const statement = sql`
      SELECT cl.discord_username, COUNT(cl.*) as command_calls
      FROM command_log as cl
      GROUP BY cl.discord_username
      ORDER BY command_calls desc
    `;

    return c.many(statement);
  });
}

export async function getCommandLogStatsByCommand() {
  return pool.connect(async (c) => {
    const statement = sql`
      SELECT command, COUNT(*) as command_calls
      FROM command_log
      GROUP BY command
      ORDER BY command_calls desc
    `;

    return c.many(statement);
  });
}

export async function getDataStats() {
  const numberOfImages = await getNumberOfSavedImages();
  const numberOfTalismans = await getNumberOfTalisman();

  console.log(numberOfImages);
}

export async function getNumberOfSavedImages() {
  return pool.connect(async (c) => {
    const statement = sql`
      SELECT COUNT(*)
      FROM screenshot
    `;

    return c.query(statement);
  });
}

export async function getNumberOfTalisman() {
  return pool.connect(async (c) => {
    const statement = sql`
      SELECT COUNT(*)
      FROM images
    `;

    return c.query(statement);
  });
}
