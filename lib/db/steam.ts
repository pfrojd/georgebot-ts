import { sql } from "slonik";
import slugify from "slugify";
import { Game, GameFilter } from "../types/game";
import {
  SteamLibraryEntry,
  SteamLibraryAPIResponse,
} from "../types/steam_library_entry";
import { SteamGame } from "../types/steam_game";
import {
  getEarlyAccessFlag,
  getSteamLibrary,
  getNewGamesFromLibrary,
} from "../steam";
import pool from "./index";

export async function getListOfGames(): Promise<any> {
  return await pool.connect(async (c) => {
    const result = await c.many(
      sql`
        SELECT *
        FROM steam_game
        WHERE deleted_at is null
      `
    );

    if (!result) {
      console.log("nothing to get");
      return;
    }

    return result;
  });
}

export async function insertGame(game: SteamGame): Promise<any> {
  return await pool.connect(async (c) => {
    const exists = await c.maybeOne(
      sql`
        SELECT *
        FROM steam_game
        WHERE steam_id = ${game.steam_appid}
      `
    );

    if (!exists) {
      const price = game.price_overview
        ? game.price_overview.final_formatted
        : null;
      const is_released = !game.release_date.coming_soon;
      const is_early_access = getEarlyAccessFlag(game);
      const slug = slugify(game.name, {
        replacement: "-",
        lower: true,
      });

      await c.query(
        sql`
          INSERT INTO steam_game
          (name, steam_id, is_free, price_formatted, release_date, is_released, is_early_access, data, slug, recommendations, metacritic, played)
          VALUES
          (${game.name}, ${game.steam_appid}, ${game.is_free}, ${price}, ${
          game.release_date.date
        }, ${is_released}, ${is_early_access}, ${sql.json(
          game as any
        )}, ${slug},  0, 0, false);
        `
      );

      return { new: true };
    } else {
      if (!exists.deleted_at) {
        // Do nothing, it exists and is not deleted.
        return {
          new: false,
          updated: false,
        };
      }

      console.log("updating game through link");
      await c.query(
        sql`
          UPDATE steam_game
          SET updated_at = to_timestamp(${Math.round(Date.now() / 1000)}),
          deleted_at = null
          WHERE steam_id = ${game.steam_appid}
        `
      );

      return { new: false, updated: true };
    }
  });
}

export async function disableGame(game_id: number): Promise<void> {
  return await pool.connect(async (c) => {
    await c.query(
      sql`
        UPDATE steam_game
        SET deleted_at = to_timestamp(${Math.round(Date.now() / 1000)})
        WHERE steam_id = ${game_id}
      `
    );
  });
}

export async function getGame(id: number): Promise<SteamGame | null> {
  return await pool.connect(async (c) => {
    return await c.maybeOne(
      sql`
        SELECT *
        FROM steam_game
        WHERE steam_id = ${id}
      `
    );
  });
}

export async function setGameAsPlayed(
  id: number,
  isPlayed: boolean
): Promise<void> {
  return await pool.connect(async (c) => {
    if (isPlayed) {
      await c.query(
        sql`
        UPDATE steam_game
        SET played_at = to_timestamp(${Math.round(Date.now() / 1000)}),
        played = ${isPlayed}
        WHERE steam_id = ${id}
      `
      );
    } else {
      await c.query(
        sql`
          UPDATE steam_game
          SET played_at = null,
          played = ${isPlayed}
          WHERE steam_id = ${id}
        `
      );
    }
  });
}

export async function getRandomGame(): Promise<SteamGame> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT *
        FROM steam_game
        WHERE deleted_at is null
        ORDER BY RANDOM()
        LIMIT 1
      `
    );
  });
}

export async function getListOfNonPlayedGames(): Promise<SteamGame[]> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT *
        FROM steam_game
        WHERE deleted_at is null
        AND played = false
      `
    );
  });
}

export async function getListOfReleasedGames(): Promise<SteamGame[]> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT *
        FROM steam_game
        WHERE deleted_at is null
        AND is_released = true
      `
    );
  });
}

export async function getListOfPlayedGames(): Promise<SteamGame[]> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT *
        FROM steam_game
        WHERE deleted_at is null
        AND played = true
      `
    );
  });
}

export async function updateGame(id: number, game: SteamGame): Promise<any> {
  return await pool.connect(async (c) => {
    const price = game.price_overview
      ? game.price_overview.final_formatted
      : null;
    const is_released = !game.release_date.coming_soon;
    const is_early_access = getEarlyAccessFlag(game);
    return await c.query(
      sql`
          UPDATE steam_game
          SET price_formatted = ${price},
          is_released = ${is_released},
          is_early_access = ${is_early_access},
          updated_at = to_timestamp(${Math.round(Date.now() / 1000)}),
          release_date = ${game.release_date.date},
          data = ${sql.json(game as any)}
          WHERE steam_id = ${id}
        `
    );
  });
}

export async function getGamesByCategory(
  categories: string[]
): Promise<SteamGame[]> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT game.*
        FROM steam_game game, JSONB_ARRAY_ELEMENTS(game.data -> 'categories') cat
        WHERE LOWER(cat->>'description') = ANY(${sql.array(categories, "text")})
        GROUP BY game.id
        ORDER BY name
      `
    );
  });
}

export async function getGamesByGenre(genres: string[]): Promise<SteamGame[]> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT game.*
        FROM steam_game game, JSONB_ARRAY_ELEMENTS(game.data->'genres') gen
        WHERE LOWER(gen->>'description') = ANY(${sql.array(genres, "text")})
        GROUP BY game.id
        ORDER BY name
      `
    );
  });
}

export async function getGamesWithFilter(
  filter: GameFilter
): Promise<SteamGame[] | undefined> {
  if (!filter.categories && !filter.genres) {
    throw new Error("You have to provide categories or genres for the filter");
  }

  if (filter.categories) {
    filter.categories = filter.categories.map((c) => c.toLowerCase());
    return await getGamesByCategory(filter.categories);
  }

  if (filter.genres) {
    filter.genres = filter.genres.map((c) => c.toLowerCase());
    return await getGamesByGenre(filter.genres);
  }
}

export async function getAllGenres(): Promise<any> {
  return await pool.connect(async (c) => {
    return await c.oneFirst(
      sql`
      SELECT ARRAY_AGG(DISTINCT(cat -> 'description')) as categories
      FROM steam_game game, JSONB_ARRAY_ELEMENTS(game.data -> 'categories') cat
      `
    );
  });
}

export async function getRandomSteamLibraryGame(): Promise<SteamLibraryEntry> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT name, steam_id
        FROM steam
        ORDER BY RANDOM()
        LIMIT 1
      `
    );
  });
}

export async function getAllSteamLibraryGames(): Promise<any> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT *
        FROM steam
      `
    );
  });
}

export async function updateSteamLibrary(): Promise<any> {
  const apps: SteamLibraryAPIResponse[] = await getSteamLibrary();
  await pool.connect(async (c) => {
    const list: Game[] = await c.many(
      sql`
        SELECT *
        FROM steam
      `
    );
    const updatedGames = getNewGamesFromLibrary(apps, list);

    if (!updatedGames) {
      return;
    }

    const unnestedGames = updatedGames.map((g: any) => {
      return Object.values(g);
    });

    await c.query(
      sql`
      INSERT INTO steam (steam_id, name)
      SELECT *
      FROM ${sql.unnest(unnestedGames, ["int4", "text"])}
      `
    );
  });
  return await getAllSteamLibraryGames();
}
