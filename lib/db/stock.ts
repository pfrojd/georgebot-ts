import { sql } from "slonik";
import pool from "./index";
import { Stock } from "../types/stock";

export async function addStock(
  discord_id: string,
  discord_username: string,
  symbol: string,
  data: any
): Promise<Stock | undefined> {
  return await pool.connect(async (c) => {
    const exists = await hasStock(discord_id, symbol);
    if (!exists) {
      return await c.one(
        sql`
          INSERT INTO stock
          (discord_id, discord_username, symbol, data) VALUES (${discord_id}, ${discord_username}, ${symbol}, ${sql.json(
          data as any
        )})
          RETURNING *
        `
      );
    }
  });
}

export async function hasStock(
  discord_id: string,
  symbol: string
): Promise<Stock | null> {
  return await pool.connect(async (c) => {
    return await c.maybeOne(
      sql`
        SELECT id, discord_id, symbol
        FROM stock
        WHERE discord_id = ${discord_id}
        AND symbol ILIKE ${symbol}
      `
    );
  });
}

export async function removeStock(
  discord_id: string,
  symbol: string
): Promise<any> {
  return await pool.connect(async (c) => {
    const exists = await hasStock(discord_id, symbol);
    if (exists) {
      return await c.query(
        sql`
          DELETE
          FROM stock
          WHERE Id = ${exists.id}
        `
      );
    }
  });
}

export async function getProfile(discord_id: string): Promise<Stock[]> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT *
        FROM stock
        WHERE discord_id = ${discord_id}
      `
    );
  });
}

export async function getAllStocks(): Promise<Stock[]> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT distinct(symbol)
        FROM stock
      `
    );
  });
}

export async function updateStockData(
  symbol: string,
  data: any
): Promise<void> {
  return await pool.connect(async (c) => {
    await c.query(
      sql`
        UPDATE stock
        SET data = ${sql.json(data as any)}
        WHERE symbol = ${symbol}
      `
    );
  });
}
