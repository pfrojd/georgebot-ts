import { SunnyImage, SunnyQuote } from "../../lib/types/sunny";
import { sql } from "slonik";
import pool from "./index";

export interface InsertSunnyProps {
  season: number;
  episode: number;
  path: string;
}

export interface InsertSunnyQuoteProps {
  season: number;
  episode: number;
  content: string;
}

export async function insertSunnyImages(
  files: InsertSunnyProps[]
): Promise<void> {
  return await pool.connect(async (c) => {
    console.log(files);
    const chunk = files.map((file: any) => Object.values(file));
    const statement = sql`
    INSERT INTO sunny
    (path, season, episode)
    SELECT *
    FROM ${sql.unnest(chunk, ["text", "int4", "int4"])}
  `;

    await c.query(statement);
    return;
  });
}

export async function getRandomSunnyImage(): Promise<SunnyImage> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT *
        FROM sunny
        ORDER BY RANDOM()
        LIMIT 1
        `
    );
  });
}

export async function insertSunnyQuotes(
  files: InsertSunnyQuoteProps[]
): Promise<void> {
  return await pool.connect(async (c) => {
    const chunk = files.map((file: any) => Object.values(file));
    const statement = sql`
    INSERT INTO sunny_quote
    (content, season, episode)
    SELECT *
    FROM ${sql.unnest(chunk, ["text", "int4", "int4"])}
  `;

    await c.query(statement);
    return;
  });
}

export async function getRandomSunnyQuote(): Promise<SunnyQuote> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT *
        FROM sunny_quote
        ORDER BY RANDOM()
        LIMIT 1
        `
    );
  });
}
