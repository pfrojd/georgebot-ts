// import "../types/slonik";
import { TalismanImage } from "../../lib/types/image";
import { sql } from "slonik";
import pool from "./index";

export async function insertImage(
  description: string,
  path: string
): Promise<void> {
  return await pool.connect(async (c) => {
    await c.query(
      sql`
        INSERT INTO images
        (description, path) VALUES
        (${description}, ${path})
      `
    );
  });
}

export async function getAllImages(): Promise<TalismanImage[]> {
  return await pool.connect(async (c) => {
    return await c.many(
      sql`
        SELECT *
        FROM images
        WHERE deleted_at is null;
      `
    );
  });
}

export async function getImageById(id: number): Promise<TalismanImage> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT *
        FROM images
        WHERE id = ${id}
        AND deleted_at is null
        LIMIT 1;
      `
    );
  });
}

export async function getLatestImage(): Promise<TalismanImage> {
  return await pool.connect(async (c) => {
    return await c.one(
      sql`
        SELECT *
        FROM images
        WHERE deleted_at is null
        ORDER BY id DESC
        LIMIT 1;
      `
    );
  });
}

export async function getRandomImage(): Promise<TalismanImage> {
  return await pool.connect(async (c) => {
    const list = await c.one(
      sql`
        SELECT id
        FROM images
        WHERE deleted_at is null
        ORDER BY RANDOM()
        LIMIT 1;
      `
    );

    const { id } = list;

    return await c.one(
      sql`
        SELECT *
        FROM images
        WHERE id = ${id}
      `
    );
  });
}

export async function updateImage(
  image: TalismanImage,
  newFilePath: string
): Promise<void> {
  return await pool.connect(async (c) => {
    await c.query(
      sql`
        UPDATE images
        SET path = ${newFilePath}
        WHERE id = ${image.id}
      `
    );
  });
}

export async function disableImage(id: number): Promise<string> {
  return await pool.connect(async (c) => {
    const exists: TalismanImage = await c.one(
      sql`
        SELECT *
        FROM images
        WHERE id = ${id}
      `
    );

    if (exists) {
      if (!exists.deleted_at) {
        await c.query(
          sql`
            UPDATE images
            SET deleted_at = now()
            WHERE id = ${exists.id}
          `
        );
        return `Disabled talisman (${exists.id}), to enable it just repeat this command.`;
      } else {
        await c.query(
          sql`
            UPDATE images
            SET deleted_at = null
            WHERE id = ${exists.id}
          `
        );
        return `Enabled talisman image (${exists.id})`;
      }
    }

    return `Could not find the talisman image (${id})`;
  });
}
