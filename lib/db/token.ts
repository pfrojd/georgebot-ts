import { sql } from "slonik";
import jwt from "jsonwebtoken";
import { Token } from "../types/token";
import pool from "./index";

const secret = process.env.SECRET;

export async function authenticate(token: string): Promise<Token | null> {
  return await pool.connect(async (c) => {
    try {
      return await c.one(
        sql`
          SELECT *
          FROM api_token t
          WHERE t.token = ${token}
        `
      );
    } catch (e) {
      return null;
    }
  });
}

export async function insertToken(
  discord_id: string,
  comment: string
): Promise<Token> {
  return await pool.connect(async (c) => {
    const token = createToken(discord_id);
    return await c.one(
      sql`
        INSERT INTO api_token
        (token, comment) VALUES (${token}, ${comment})
        RETURNING *
      `
    );
  });
}

export async function reportTokenUsage(token: string): Promise<void> {
  return await pool.connect(async (c) => {
    await c.query(
      sql`
        UPDATE api_token
        SET usage = usage + 1,
        last_updated = to_timestamp(${Math.round(Date.now() / 1000)})
        WHERE token = ${token}
      `
    );
  });
}

export function createToken(id: string): string {
  if (!secret) {
    throw new Error("Can't generate a token without a secret");
  }
  return jwt.sign(
    {
      id: id,
    },
    secret,
    {}
  );
}
