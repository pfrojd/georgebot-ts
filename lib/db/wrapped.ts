import { sql } from "slonik";
import _ from "lodash";
import pool from "./index";

export interface AllCommands {
  command: string;
  count: number;
}

export async function getAllCommands(): Promise<Array<AllCommands>> {
  return pool.connect(async (c) => {
    const statement = sql`
    select command, count(command) as count
    from command_log
    group by command
    order by count desc
    limit 6
        `;

    return c.many<AllCommands>(statement);
  });
}

export interface GetCommandCount {
  count: number;
}

export async function getCommandCount(): Promise<GetCommandCount> {
  return pool.connect(async (c) => {
    const statement = sql`
    select count(command) as count
    from command_log`;

    return c.one<GetCommandCount>(statement);
  });
}

export interface GetDailyCommands {
  count: number;
  commandsperday: number;
  since: number;
}

export async function getDailyCommands(): Promise<GetDailyCommands> {
  return pool.connect(async (c) => {
    const statement = sql`
    select count(*),
    round(count(*) / extract(days from now() - (
      select command_timestamp
      from command_log
      order by id
      limit 1
    ))::numeric, 2) commandsperday,
      (
        select command_timestamp
        from command_log
        order by id
        limit 1
    ) since
    from command_log cl
    `;

    return c.one<GetDailyCommands>(statement);
  });
}

export interface GetDailyScreenshots {
  count: number;
  screenshotsperday: number;
  since: number;
}
export async function getDailyScreenshots(): Promise<GetDailyScreenshots> {
  return pool.connect(async (c) => {
    const statement = sql`
    select count(*),
    round(count(*) / extract(days from now() - to_date('17/04/2018', 'DD/MM/YYYY'))::numeric, 2) screenshotsperday,
    to_date('17/04/2018', 'DD/MM/YYYY') as since
    from screenshot s`;

    return c.one<GetDailyScreenshots>(statement);
  });
}

export interface GetCommandsPerWeekday {
  dayofweek: string;
  count: number;
}
export async function getCommandsPerWeekday(): Promise<
  Array<GetCommandsPerWeekday>
> {
  return pool.connect(async (c) => {
    const statement = sql`
  select day_name(extract(isodow from command_log.command_timestamp)) as dayofweek,
  count(command) as count
  from command_log
  group by extract(isodow from command_log.command_timestamp)
  order by extract(isodow from command_log.command_timestamp) asc`;

    return c.many<GetCommandsPerWeekday>(statement);
  });
}

export interface GetEventsNotSeen {
  count: number;
  numberofevents: number;
  percenteventsseen: number;
}
export async function getEventsNotSeen(): Promise<GetEventsNotSeen> {
  return pool.connect(async (c) => {
    const statement = sql`
    select count(*), (select count(*) from fact where type = 'EVENT') as numberofevents,
    round(count(*) / (select count(*) from fact where type = 'EVENT')::numeric, 2) * 100 as percenteventsseen
    from command_log cl
    where cl.command in ('today', 'nottoday')`;

    return c.one<GetEventsNotSeen>(statement);
  });
}

export interface GetEarliestweekCommand {
  username: string;
  dayname: string;
  houroftheday: number;
  timestamp: number;
}
export async function getEarliestWeekCommand(): Promise<GetEarliestweekCommand> {
  return pool.connect(async (c) => {
    const statement = sql`
    select discord_username as username,
    command_timestamp as timestamp,
    day_name(extract(isodow from cl.command_timestamp)) as dayname,
    extract(hour from cl.command_timestamp) as houroftheday
    from command_log cl
    where cl.command = 'week'
    and extract(isodow from cl.command_timestamp) = 1
    order by extract(hour from cl.command_timestamp)
    limit 1`;

    return c.one<GetEarliestweekCommand>(statement);
  });
}

export interface GetFavouriteMeme {
  meme: string;
  count: number;
}
export async function getFavouriteMemes(): Promise<Array<GetFavouriteMeme>> {
  return pool.connect(async (c) => {
    const statement = sql`
    select unnest(case when values <> '{}' then values ELSE '{-}' END) as meme,
    count(*)
    from command_log cl,
    regexp_match(cl.message_content, '(\\d+)') as values
    WHERE cl.command = 'meme'
    group by meme
    order by count desc`;

    return c.many<GetFavouriteMeme>(statement);
  });
}

export interface GetCheggStats {
  count: number;
  cheggsperday: number;
  since: number;
}
export async function getCheggStats(): Promise<GetCheggStats> {
  return pool.connect(async (c) => {
    const statement = sql`  
    select count(*), 
    round(count(*) / extract(days from now() - days_since_command('chegg'))::numeric, 2) cheggsperday,
    (select command_timestamp from command_log where command = 'chegg' order by id limit 1) since
    from command_log cl
    where cl.command = 'chegg'`;

    return c.one<GetCheggStats>(statement);
  });
}

export interface GetScreenshotsContribution {
  username: string;
  count: number;
}

export async function getScreenshotsContribution(): Promise<
  Array<GetScreenshotsContribution>
> {
  return pool.connect(async (c) => {
    const statement = sql`
    select s.discord_username as username,
      count(*)
    from screenshot s
    group by s.discord_username
    order by count desc
    limit 3
  `;

    return c.many<GetScreenshotsContribution>(statement);
  });
}

export interface GetPopularTalisman {
  meme: string;
  count: number;
}

export async function getMostPopularTalisman(): Promise<
  Array<GetPopularTalisman>
> {
  return pool.connect(async (c) => {
    const statement = sql`
    select unnest(case when values <> '{}' then values ELSE '{-}' END) as meme,
    count(*)
    from command_log cl,
    regexp_match(cl.message_content, '(\\d+)') as values
    WHERE cl.command = 'talisman'
    group by meme
    order by count desc
    limit 10
  `;

    return c.many<GetPopularTalisman>(statement);
  });
}

export interface GetTalismanStatistics {
  count: number;
  talismansperday: number;
  since: number;
}

export async function getTalismanStats(): Promise<GetTalismanStatistics> {
  return pool.connect(async (c) => {
    const statement = sql`  
    select count(*), 
    round(count(*) / extract(days from now() - days_since_command('talisman'))::numeric, 2) talismansperday,
    (select command_timestamp from command_log where command = 'talisman' order by id limit 1) since
    from command_log cl
    where cl.command = 'talisman'`;

    return c.one<GetTalismanStatistics>(statement);
  });
}

/**
 * * * * * * * * * * * * * * * *
 *
 * Personalized areas
 *
 * * * * * * * * * * * * * * * *
 */

export interface CommandsPerUser {
  command: string;
  count: number;
}
export async function getCommandsPerUser(): Promise<Array<CommandsPerUser>> {
  return pool.connect(async (c) => {
    const statement = sql`
    select command, count(command) as count
    from command_log
    group by command
    order by count desc
        `;

    return c.many<CommandsPerUser>(statement);
  });
}

export interface TotalCommandsPerUser {
  userid: string;
  command: number;
  count: number;
}
export async function getTotalCommandsPerUserThisYear(): Promise<
  Array<TotalCommandsPerUser>
> {
  return pool.connect(async (c) => {
    const statement = sql`
    select discord_user_id::text as userId, command, count(command) as count
from command_log
where extract(year from command_timestamp) = 2023
group by command, discord_user_id
order by command desc
        `;

    return c.many<TotalCommandsPerUser>(statement);
  });
}

export interface ActiveMonthsThisYear {
  userid: string;
  month: number;
  count: number;
}

export async function getActiveMonthsThisYear(): Promise<
  Array<ActiveMonthsThisYear>
> {
  return pool.connect(async (c) => {
    const statement = sql`
    select discord_user_id::text as userid, date_trunc('month', command_log.command_timestamp) as month, count(command) as count
from command_log
where extract(year from command_timestamp) = 2023
group by discord_user_id, month
order by month asc
        `;

    return c.many<ActiveMonthsThisYear>(statement);
  });
}

export interface ActiveWeekdaysInThisYear {
  userid: string;
  dayofweek: number;
  dayvalue: number;
  count: number;
}

export async function getActiveWeekdaysInThisYear(): Promise<
  Array<ActiveWeekdaysInThisYear>
> {
  return pool.connect(async (c) => {
    const statement = sql`
    select discord_user_id::text as userid,
    day_name(extract(isodow from cl.command_timestamp)) as dayofweek,
    extract(isodow from cl.command_timestamp) as dayvalue,
    count(cl.command) as count
    from command_log cl
    where extract(year from command_timestamp) = 2023
    group by discord_user_id, dayofweek, dayvalue
    order by discord_user_id asc, dayvalue asc
    `;

    return c.many<ActiveWeekdaysInThisYear>(statement);
  });
}

export interface MostActiveDayThisYear {
  userid: string;
  day: string;
  count: number;
}

export interface MostActiveDayCommands {
  count: number;
  command: string;
}

export interface MostActiveDayResult {
  days: Array<MostActiveDayThisYear>;
  users: CmdPerUser;
}

interface CmdPerUser {
  [key: string]: Array<MostActiveDayCommands>;
}

export async function getMostActiveDayThisYear(): Promise<MostActiveDayResult> {
  return pool.connect(async (c) => {
    const statement = sql`
    select discord_user_id::text as userid,
    date_trunc('day', command_log.command_timestamp)::date as day,
    count(command) as count
    from command_log
    where extract(year from command_timestamp) = 2023
    and discord_user_id in (118486265690718215, 133328817342251010, 138307196541730816)
    group by discord_user_id, day
    order by userid, count desc
    `;

    const activeDays = await c.many<MostActiveDayThisYear>(statement);

    const users = _.uniqBy(activeDays, "userid").map((c) => c.userid);
    const topDayPerUser = users.map((user) => {
      return activeDays
        .filter((c) => c.userid == user)
        .map((c) => ({ userid: c.userid, day: c.day, count: c.count }))
        .slice(0, 1)[0];
    });

    const returnUsers: CmdPerUser = {};

    for (const { userid, day } of topDayPerUser) {
      const secondStatement = sql`
      select command, count(*)
      from command_log
      where command_timestamp::date = ${day}::date
      and discord_user_id = ${userid}
      group by command
      `;

      try {
        const value = await c.many<MostActiveDayCommands>(secondStatement);
        returnUsers[userid] = value;
      } catch (e) {
        continue;
      }
    }

    return { days: activeDays, users: returnUsers };
  });
}

export interface FavouriteTalisman {
  userid: string;
  meme: string;
  count: number;
}

export async function getFavouriteTalismanOfTheYear(): Promise<
  Array<FavouriteTalisman>
> {
  return pool.connect(async (c) => {
    const statement = sql`
    select discord_user_id as userid, unnest(case when values <> '{}' then values ELSE '{-}' END) as meme, count(*)
    from command_log cl,
    regexp_match(cl.message_content, '(\\d+)') as values
    WHERE cl.command = 'talisman'
    and extract(year from cl.command_timestamp) = 2023
    group by meme, userid
    order by userid, count desc
    `;

    return c.many<FavouriteTalisman>(statement);
  });
}

export async function getFavouriteMemesOfTheYear(): Promise<
  Array<FavouriteTalisman>
> {
  return pool.connect(async (c) => {
    const statement = sql`
    select discord_user_id as userid, unnest(case when values <> '{}' then values ELSE '{-}' END) as meme, count(*)
    from command_log cl,
    regexp_match(cl.message_content, '(\\d+)') as values
    WHERE cl.command = 'meme'
    and extract(year from cl.command_timestamp) = 2023
    group by meme, userid
    order by userid, count desc
    `;

    return c.many<FavouriteTalisman>(statement);
  });
}
