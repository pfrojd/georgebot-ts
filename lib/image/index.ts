import got from "got";

export async function fetchImage(url: string) {
  const image = await got.get(url, {
    responseType: "buffer",
  });

  return image.body;
}
