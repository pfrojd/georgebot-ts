import { getRandomScreenshot } from "../db/screenshots";
import Discord from "discord.js";
import Canvas, { loadImage } from "canvas";
import fs from "fs";
import templates, { MemeTemplate } from "./templates";
import { getRandomQuote } from "../db/quote";
import _ from "lodash";
import { testLineLength, splitLongQuoteIntoLines } from "../canvas/server";
import * as Random from "random-js";

const engine = Random.nodeCrypto;

const { readFile } = fs.promises;

const IS_DEV = process.env.IS_DEV ? true : false;

export const replacementWords = [
  "Gribbly",
  "Gordon Bennett",
  "Pooprat",
  "Toilet",
  "PooGoat",
  "Sleeping Bag",
  "Sticky",
  "Poo",
  "Legs",
  "Physically Disabled",
  "Human Earth",
  "PHP",
  "Beans",
  "Spicy Accidente",
  "[GROWLING]",
  "nosh",
  "Chegg",
];

export async function getRandomMeme(
  message: Discord.Message,
  args: string[]
): Promise<Canvas.Canvas> {
  const meme = selectMemeTemplate(args);

  switch (meme.type) {
    default:
    case "DRAKE": {
      return await createDrakeMeme(meme);
    }
    case "POOH": {
      return await createPoohMeme(meme);
    }
    case "SEAGULL": {
      return await createSeagullMeme(meme);
    }
    case "MIND": {
      return await createMindMeme(meme);
    }
    case "WOLVERINE": {
      return await createWolverineMeme(meme);
    }
    case "BOBBY": {
      return await createBobbyMeme(meme);
    }
    case "FINALLY": {
      return await createFinallyMeme(meme);
    }
  }
}

const possibleNumberOfMemes = 7;

export function selectMemeTemplate(args: string[]): MemeTemplate {
  if (args[0] && Number(args[0])) {
    // It's a valid integer
    const value = Number(args[0]);

    if (value == 0 || value > possibleNumberOfMemes) {
      return randomMemeTemplate();
    }

    return templates[value - 1];
  }

  return randomMemeTemplate();
}

function randomMemeTemplate(): MemeTemplate {
  const rnd = Random.integer(0, possibleNumberOfMemes - 2);
  return templates[rnd(engine)];
}

function splitLineIntoWordSegments(text: string): string[] {
  const lines = text.split(" ");
  const chunks = _.chunk(lines, Math.ceil(lines.length / 4));
  return chunks.map((c) => c.join(" "));
}

async function createMindMeme(meme: MemeTemplate): Promise<Canvas.Canvas> {
  const canvas = Canvas.createCanvas(meme.width, meme.height);
  const ctx = canvas.getContext("2d");

  const image = new Canvas.Image();
  const imageBuffer = await readFile(meme.templateSrc);
  image.src = imageBuffer;

  ctx.drawImage(image, 0, 0, meme.width, meme.height);

  const fontSize = 24;

  for (const instruction of meme.instructions) {
    ctx.fillStyle = "black";
    // ctx.textBaseline = "middle";
    ctx.font = `${fontSize}px Arial`;

    const quote = await getRandomQuote();

    // It's not a one-liner, so we need to split it into lines, we split by word
    // and measure the line with after adding each word (to reach max capacity)
    const lines = splitLongQuoteIntoLines(ctx, quote, instruction.width);

    // We now need to get the longest line, to calculate the actual font size.
    const longestLine = _.maxBy(lines, (line) => line.length);

    // We have the longest line, now test the line length of it, which will continously
    // reduce the fontSize until it can fit all content.
    const actualFontSize = testLineLength(ctx, longestLine ?? quote.content, {
      width: instruction.width,
      padding: instruction.padding,
      fontSize: 24,
      fontFamily: "Arial",
    });

    // We know the total height of the image is cursed.height
    // We remove the padding, the extra text area, y will be at the very bottom of the image.
    // Now add the fontSize to account for the size of it (so the text won't clip th eimage)
    let y =
      instruction.drawPositionY - instruction.padding + actualFontSize + 50;

    ctx.font = `${actualFontSize}px ${"Arial"}`;

    // We have the starting location of the text, now iterate through the lines.
    for (const line of lines) {
      ctx.save();
      ctx.translate(instruction.drawPositionX + instruction.padding, y);
      ctx.rotate((7 * -Math.PI) / 180);
      ctx.fillText(line, 0, 0);
      ctx.restore();

      // For each line, increment y with the fontSize, adjust this if we want more padding between lines.
      y += actualFontSize;
    }
  }
  return canvas;
}

async function createSeagullMeme(meme: MemeTemplate): Promise<Canvas.Canvas> {
  const canvas = Canvas.createCanvas(meme.width, meme.height);
  const ctx = canvas.getContext("2d");

  const image = new Canvas.Image();
  const imageBuffer = await readFile(meme.templateSrc);
  image.src = imageBuffer;

  ctx.drawImage(image, 0, 0, meme.width, meme.height);

  const fontSize = 18;

  ctx.fillStyle = "white";
  ctx.lineWidth = fontSize / 4;
  ctx.lineJoin = "round";
  ctx.strokeStyle = "black";
  ctx.textAlign = "center";

  const quote = await getRandomQuote();

  const lines = splitLineIntoWordSegments(quote.content);

  let index = 0;
  for (const instruction of meme.instructions) {
    ctx.font = `${fontSize}pt bold impact`;
    ctx.strokeText(
      lines[index]?.toUpperCase() || "",
      instruction.width / 2,
      instruction.drawPositionY - fontSize / 2,
      instruction.width
    );
    ctx.fillText(
      lines[index]?.toUpperCase() || "",
      instruction.width / 2,
      instruction.drawPositionY - fontSize / 2,
      instruction.width
    );
    index++;
  }

  return canvas;
}

async function createPoohMeme(meme: MemeTemplate): Promise<Canvas.Canvas> {
  const canvas = Canvas.createCanvas(meme.width, meme.height);
  const ctx = canvas.getContext("2d");

  const image = new Canvas.Image();
  const imageBuffer = await readFile(meme.templateSrc);
  image.src = imageBuffer;

  ctx.drawImage(image, 0, 0, meme.width, meme.height);

  const fontSize = 48;

  for (const instruction of meme.instructions) {
    ctx.fillStyle = "black";
    ctx.textBaseline = "middle";
    ctx.font = `${fontSize}px Arial`;

    if (!instruction.content) {
      throw new Error("Bad content callback");
    }
    const content = await instruction.content();
    const quote = { content };

    // It's not a one-liner, so we need to split it into lines, we split by word
    // and measure the line with after adding each word (to reach max capacity)
    const lines = splitLongQuoteIntoLines(ctx, quote, instruction.width);

    // We now need to get the longest line, to calculate the actual font size.
    const longestLine = _.maxBy(lines, (line) => line.length);

    // We have the longest line, now test the line length of it, which will continously
    // reduce the fontSize until it can fit all content.
    const actualFontSize = testLineLength(ctx, longestLine ?? quote.content, {
      width: instruction.width,
      padding: instruction.padding,
      fontSize: 48,
      fontFamily: "Arial",
    });

    // We know the total height of the image is cursed.height
    // We remove the padding, the extra text area, y will be at the very bottom of the image.
    // Now add the fontSize to account for the size of it (so the text won't clip th eimage)
    let y =
      instruction.drawPositionY - instruction.padding + actualFontSize + 50;

    ctx.font = `${actualFontSize}px ${"Arial"}`;

    // We have the starting location of the text, now iterate through the lines.
    for (const line of lines) {
      // Print the line, at the center of the image, with the newly calculated y.
      ctx.fillText(line, instruction.drawPositionX + instruction.padding, y);

      // For each line, increment y with the fontSize, adjust this if we want more padding between lines.
      y += actualFontSize;
    }
  }
  return canvas;
}

async function createDrakeMeme(meme: MemeTemplate): Promise<Canvas.Canvas> {
  const canvas = Canvas.createCanvas(meme.width, meme.height);
  const ctx = canvas.getContext("2d");

  const image = new Canvas.Image();
  const imageBuffer = await readFile(meme.templateSrc);
  image.src = imageBuffer;

  ctx.drawImage(image, 0, 0, meme.width, meme.height);

  for (const instruction of meme.instructions) {
    const screenshot = await getRandomScreenshot();
    let screenshotImage = new Canvas.Image();

    const screenshotPath = screenshot.path.replace(
      "/mnt/volume_ams3_01/images/screenshots/",
      "https://zaifon.dev/screenshots/"
    );
    screenshotImage = await loadImage(screenshotPath);

    ctx.drawImage(
      screenshotImage,
      instruction.drawPositionX,
      instruction.drawPositionY,
      instruction.width,
      instruction.height
    );
  }
  return canvas;
}

async function createWolverineMeme(meme: MemeTemplate): Promise<Canvas.Canvas> {
  const canvas = Canvas.createCanvas(meme.width, meme.height);
  const ctx = canvas.getContext("2d");

  for (const instruction of meme.instructions) {
    if (!instruction.content) {
      throw new Error("Bad content callback");
    }
    let imagePath = await instruction.content();

    if (IS_DEV) {
      imagePath = "data/tmp/talisman.png";
    }
    const image = new Canvas.Image();
    const imageBuffer = await readFile(imagePath);
    image.src = imageBuffer;

    ctx.save();
    ctx.translate(instruction.drawPositionX, instruction.drawPositionY);
    ctx.rotate((7 * -Math.PI) / 180);

    ctx.drawImage(image, 0, 0, instruction.width, instruction.height);

    ctx.restore();
  }

  const image = new Canvas.Image();
  const imageBuffer = await readFile(meme.templateSrc);
  image.src = imageBuffer;

  ctx.drawImage(image, 0, 0, meme.width, meme.height);
  return canvas;
}

async function createBobbyMeme(meme: MemeTemplate): Promise<Canvas.Canvas> {
  const canvas = Canvas.createCanvas(meme.width, meme.height);
  const ctx = canvas.getContext("2d");

  const image = new Canvas.Image();
  const imageBuffer = await readFile(meme.templateSrc);
  image.src = imageBuffer;

  ctx.drawImage(image, 0, 0, meme.width, meme.height);

  const fontSize = 96;

  for (const instruction of meme.instructions) {
    ctx.fillStyle = "black";
    // ctx.textBaseline = "middle";
    ctx.font = `${fontSize}px Arial`;

    if (!instruction.content) {
      throw new Error("Bad content callback");
    }
    const content = await instruction.content();
    const quote = { content };

    // It's not a one-liner, so we need to split it into lines, we split by word
    // and measure the line with after adding each word (to reach max capacity)
    const lines = splitLongQuoteIntoLines(ctx, quote, instruction.width);

    // We now need to get the longest line, to calculate the actual font size.
    const longestLine = _.maxBy(lines, (line) => line.length);

    // We have the longest line, now test the line length of it, which will continously
    // reduce the fontSize until it can fit all content.
    const actualFontSize = testLineLength(ctx, longestLine ?? quote.content, {
      width: instruction.width,
      padding: instruction.padding,
      fontSize: 48,
      fontFamily: "Arial",
    });

    // We know the total height of the image is cursed.height
    // We remove the padding, the extra text area, y will be at the very bottom of the image.
    // Now add the fontSize to account for the size of it (so the text won't clip th eimage)
    let y = instruction.drawPositionY + instruction.padding + actualFontSize;

    ctx.font = `${actualFontSize}px ${"Arial"}`;

    // We have the starting location of the text, now iterate through the lines.
    for (const line of lines) {
      // Print the line, at the center of the image, with the newly calculated y.
      ctx.fillText(line, instruction.drawPositionX + instruction.padding, y);

      // For each line, increment y with the fontSize, adjust this if we want more padding between lines.
      y += actualFontSize;
    }
  }

  return canvas;
}

async function createFinallyMeme(meme: MemeTemplate): Promise<Canvas.Canvas> {
  const canvas = Canvas.createCanvas(meme.width, meme.height);
  const ctx = canvas.getContext("2d");

  const image = new Canvas.Image();
  const imageBuffer = await readFile(meme.templateSrc);
  image.src = imageBuffer;

  ctx.drawImage(image, 0, 0, meme.width, meme.height);

  const fontSize = 48;

  ctx.fillStyle = "white";
  ctx.lineWidth = fontSize / 4;
  ctx.lineJoin = "round";
  ctx.strokeStyle = "black";
  ctx.textAlign = "center";

  for (const instruction of meme.instructions) {
    let content = "";
    if (instruction.content) {
      content = await instruction.content();
    }

    ctx.font = `${instruction.fontSize || fontSize}pt bold impact`;
    ctx.strokeText(
      content,
      instruction.width / 2,
      instruction.drawPositionY - fontSize / 2,
      instruction.width - 20
    );
    ctx.fillText(
      content,
      instruction.width / 2,
      instruction.drawPositionY - fontSize / 2,
      instruction.width - 20
    );
  }

  return canvas;
}
