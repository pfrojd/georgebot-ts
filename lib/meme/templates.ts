import { getRandomImage } from "../db/talisman";
import { getRandomInspirationalQuote, getRandomQuote } from "../db/quote";
import { getRandomSteamLibraryGame } from "../db/steam";
import { substituteWord, substituteWords } from "../util";
import { getAllReplacementWords } from "lib/db/replacement_word";

export interface MemeTemplate {
  type: MemeTypes;
  templateSrc: string;
  height: number;
  width: number;
  instructions: MemeInstruction[];
}

export interface MemeInstruction {
  drawPositionX: number;
  drawPositionY: number;
  width: number;
  height: number;
  padding: number;
  content?: () => Promise<string>;
  fontSize?: number;
}

export type MemeTypes =
  | "DRAKE"
  | "POOH"
  | "SEAGULL"
  | "MIND"
  | "WOLVERINE"
  | "BOBBY"
  | "FINALLY";

const templates: MemeTemplate[] = [
  {
    type: "DRAKE",
    templateSrc: "templates/meme/drake.png",
    width: 717,
    height: 717,
    instructions: [
      {
        padding: 0,
        drawPositionX: 346,
        drawPositionY: 0,
        width: 371,
        height: 368,
      },
      {
        padding: 0,
        drawPositionX: 346,
        drawPositionY: 368,
        width: 371,
        height: 368,
      },
    ],
  },
  {
    type: "POOH",
    templateSrc: "templates/meme/Tuxedo-Winnie-The-Pooh.png",
    width: 800,
    height: 582,
    instructions: [
      {
        padding: 20,
        drawPositionX: 345,
        drawPositionY: 0,
        width: 455,
        height: 296,
        content: async (): Promise<string> => {
          const quote = await getRandomInspirationalQuote();

          if (!quote) {
            return "";
          }
          return quote?.quote;
        },
      },
      {
        padding: 20,
        drawPositionX: 345,
        drawPositionY: 296,
        width: 455,
        height: 296,
        content: async (): Promise<string> => {
          const quote = await getRandomQuote();

          if (!quote) {
            return "";
          }
          return quote?.content;
        },
      },
    ],
  },
  {
    type: "SEAGULL",
    templateSrc: "templates/meme/seagull.png",
    width: 317,
    height: 706,
    instructions: [
      {
        padding: 0,
        drawPositionX: 0,
        drawPositionY: 176,
        width: 317,
        height: 60,
      },
      {
        padding: 0,
        drawPositionX: 0,
        drawPositionY: 352,
        width: 317,
        height: 60,
      },
      {
        padding: 0,
        drawPositionX: 0,
        drawPositionY: 529,
        width: 317,
        height: 60,
      },
      {
        padding: 0,
        drawPositionX: 0,
        drawPositionY: 700,
        width: 317,
        height: 60,
      },
    ],
  },
  {
    type: "MIND",
    templateSrc: "templates/meme/Change-My-Mind.jpg",
    width: 482,
    height: 361,
    instructions: [
      {
        padding: 0,
        drawPositionX: 225,
        drawPositionY: 175,
        width: 180,
        height: 160,
      },
    ],
  },
  {
    type: "WOLVERINE",
    templateSrc: "templates/meme/wolverine.png",
    width: 595,
    height: 870,
    instructions: [
      {
        padding: 0,
        drawPositionX: 161,
        drawPositionY: 480,
        width: 280,
        height: 375,
        content: async (): Promise<string> => {
          const image = await getRandomImage();

          return image?.path;
        },
      },
    ],
  },
  {
    type: "BOBBY",
    templateSrc: "templates/meme/bobby.png",
    width: 960,
    height: 1078,
    instructions: [
      {
        padding: 20,
        drawPositionX: 325,
        drawPositionY: 60,
        width: 265,
        height: 375,
        content: async (): Promise<string> => {
          const quote = await getRandomQuote();

          if (!quote) {
            return "";
          }
          return quote?.content;
        },
      },
    ],
  },
  {
    type: "FINALLY",
    templateSrc: "templates/meme/finally.jpg",
    width: 680,
    height: 457,
    instructions: [
      {
        padding: 20,
        drawPositionX: 0,
        drawPositionY: 100,
        width: 680,
        height: 375,
        fontSize: 48,
        content: async (): Promise<string> => {
          return "FINALLY";
        },
      },
      {
        padding: 20,
        drawPositionX: 0,
        drawPositionY: 450,
        width: 680,
        height: 375,
        fontSize: 32,
        content: async (): Promise<string> => {
          const game = await getRandomSteamLibraryGame();
          const replacementWords = await getAllReplacementWords();

          if (!game) {
            return "";
          }

          const newSentence = substituteWords(
            game.name,
            replacementWords.map((rw) => rw.word),
            6
          );
          return newSentence;
        },
      },
    ],
  },
];

export default templates;
