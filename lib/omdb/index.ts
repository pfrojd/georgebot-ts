import { OmdbMovie } from "../../lib/types/movie";
import axios from "axios";

const client = axios.create({
  baseURL: "http://www.omdbapi.com/",
  params: {
    apikey: process.env.OMDB_API,
  },
});

export async function getMovieByName(
  name: string
): Promise<OmdbMovie | undefined> {
  try {
    const result = await client.get("", {
      params: {
        t: name,
        r: "json",
        apikey: process.env.OMDB_API,
        type: "movie",
      },
    });
    return result.data;
  } catch (e) {
    console.error(e);
  }
}
