import fs from 'fs';
import path from 'path';
import * as Random from "random-js";

const engine = Random.nodeCrypto;

const pathToFolder = path.resolve(process.cwd(), "data");

const { readFile } = fs.promises;

export async function readRawFile(fileName: string) {
  try {
    return await readFile(path.resolve(pathToFolder, `${fileName}.json`), "utf-8");
  } catch (e) {
    console.error(e);
    console.error("Failed to read raw file");
  }
}

export async function getRandomAscendancy() {
  const file = await readRawFile("ascendancies");
  if (file) {
    const ascendancies = JSON.parse(file);
    const listOfAscendancies = ascendancies.map((asc: any) => asc.id);
    const rnd = Random.integer(0, listOfAscendancies.length);
    const chosenAscendancy = listOfAscendancies[rnd(engine)];
    return ascendancies.find((asc: any) => asc.id == chosenAscendancy);
  }

  return null;
}

export async function getRandomKeystones(limit = 2) {
  const file = await readRawFile("keystones");
  const chosenKeystones: any[] = [];
  if (file) {
    const keystones = JSON.parse(file);
    const listOfKeystones = keystones.map((key: any) => key.name);
    Array.from({ length: limit }).map((_: any) => {
      const rnd = Random.integer(0, listOfKeystones.length);
      const chosenKeystone = listOfKeystones[rnd(engine)];
      const keystone = keystones.find((key: any) => key.name == chosenKeystone);
      chosenKeystones.push(keystone);
    })
  }

  return chosenKeystones;
}

export async function getRandomSkillGems(limit = 3) {
  const file = await readRawFile("gem_active_skills");
  const chosenGems: any[] = [];
  if (file) {
    const gems = JSON.parse(file);
    const listOfGems = gems.map((gem: any) => gem.id);
    Array.from({ length: limit }).map((_: any) => {
      const rnd = Random.integer(0, listOfGems.length);
      const chosenGem = listOfGems[rnd(engine)];
      const gem = gems.find((key: any) => key.id == chosenGem);
      chosenGems.push(gem);
    })
  }

  return chosenGems;
}
