import { Game } from "../types/game";
import { SteamGame } from "../types/steam_game";
import { SteamLibraryAPIResponse } from "../types/steam_library_entry";
import axios from "axios";
import _ from "lodash";
import ratelimit from "axios-rate-limit";
import { isTruthy } from "../util";

const STEAM_GAME_URL = `https://store.steampowered.com/app`;

const client = ratelimit(
  axios.create({
    baseURL: "https://store.steampowered.com/api/appdetails/",
  }),
  {
    maxRequests: 1,
    perMilliseconds: 1000,
  }
);

export async function getSteamGame(id: number) {
  const { data } = await client.get(`?appids=${id}`);

  return data[id].data;
}

export async function getSteamGames(ids: number[]) {
  const { data } = await client.get(`appids=${ids.join(",")}`);

  return data;
}

export function getEarlyAccessFlag(game: SteamGame) {
  let isEarlyAccess = false;
  game.genres.map((genre) => {
    if (genre.id === "70" || genre.description === "Early Access") {
      isEarlyAccess = true;
      return;
    }
  });
  return isEarlyAccess;
}

export const hasURL = new RegExp(/https?:\/\/store.steampowered.com/);
export const gameData = new RegExp(/(\/app\/)(\d*)/);

export async function getSteamLibrary() {
  const { data } = await axios.get(
    "https://api.steampowered.com/ISteamApps/GetAppList/v2/"
  );

  if (!data || !data.applist.apps.length) {
    throw new Error("Could not fetch all apps from Steam");
  }
  return data.applist.apps;
}

export function getNewGamesFromLibrary(
  applist: SteamLibraryAPIResponse[],
  dblist: Game[]
) {
  const newIds = applist.map((a) => a.appid);
  const dbIds = dblist.map((d) => d.steam_id);

  console.log(`Number of games found in current database: ${dbIds.length}`);
  console.log(`Number of games found in steam: ${newIds.length}`);

  const newEntriesIds = _.difference(newIds, dbIds);

  if (!newEntriesIds) {
    return [];
  }

  const newGames = applist.filter((game: SteamLibraryAPIResponse) => {
    if (newEntriesIds.includes(game.appid)) {
      return [game.appid, game.name];
    }
    return null;
  });

  return newGames.filter(isTruthy);
}

export function getSteamLink(id: string | number) {
  return `${STEAM_GAME_URL}/${id}`;
}
