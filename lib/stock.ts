import fin from "yahoo-finance2";
import { getAllStocks, updateStockData } from "./db/stock";

export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export async function getStock(symbol: string) {
  let result;
  try {
    result = await fin.search(
      symbol,
      {
        lang: "en-US",
        region: "US",
        quotesCount: 2,
        newsCount: 0,
        enableFuzzyQuery: true,
      },
      { validateResult: false }
    );
  } catch (e) {
    result = e.result;
  }

  if (!result) {
    throw new Error("Stock was not found");
  }

  return result;
}

export async function getStockSummary(symbol: string) {
  let result;
  try {
    result = await fin.quoteSummary(symbol, {}, { validateResult: false });
  } catch (e) {
    result = e.result;
  }

  if (!result) {
    throw new Error("Stock was not found");
  }

  return result;
}

export async function searchAndGetStockSummary(symbol: string) {
  const result = await getStock(symbol);
  if (!result?.quotes[0]?.symbol) {
    throw new Error("No stock was found with that symbol :(");
  }
  const quote = await getStockSummary(result.quotes[0].symbol);
  return quote;
}

export async function updateAllStocks() {
  const stockSymbols = await getAllStocks();
  for (const { symbol } of stockSymbols) {
    await sleep(500);
    const stockData = await getStockSummary(symbol as string);
    await updateStockData(symbol as string, stockData);
    console.log(`-- Updated (${symbol as string} from API) --`);
  }
}
