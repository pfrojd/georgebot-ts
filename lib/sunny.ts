import { promises } from "fs";
import path from "path";
import { insertSunnyImages } from "./db/sunny";
import { SunnyImage } from "./types/sunny";
const { readdir, stat } = promises;

const IS_DEV = process.env.IS_DEV ? true : false;

const LOCAL_SUNNY_PATH = path.resolve(
  "D:",
  "extra-dev",
  "sunny-screenshots",
  "build"
);

const REMOTE_SUNNY_PATH = path.resolve(
  "/mnt",
  "volume_ams3_01",
  "images",
  "sunny"
);

async function walk(dir: string) {
  let files: any = await readdir(dir);
  files = await Promise.all(
    files.map(async (file: any) => {
      const filePath = path.join(dir, file);
      const stats = await stat(filePath);
      if (stats.isDirectory()) return walk(filePath);
      else if (stats.isFile()) return filePath;
    })
  );

  return files.reduce(
    (all: any, folderContents: any) => all.concat(folderContents),
    []
  );
}

export async function getRandomSunnyFile() {
  const start = new Date();
  const pathToWalk = IS_DEV ? LOCAL_SUNNY_PATH : REMOTE_SUNNY_PATH;
  const files = await walk(pathToWalk);
  const end = new Date();

  const timeToProcess = Math.abs((end.getTime() - start.getTime()) / 1000);
  const randomImage = files[Math.floor(Math.random() * files.length)];
  return { path: randomImage, name: path.basename(randomImage) };
}

function getSeasonAndEpisodeFromPath(file: string) {
  const re = new RegExp(/S(?<season>\d*)E(?<episode>\d*)/g);
  const result = re.exec(file);
  if (result) {
    return {
      path: file,
      season: Number(result?.groups?.season),
      episode: Number(result?.groups?.episode),
    };
  }
  return;
}

export async function insertSunnyFiles(): Promise<void> {
  const pathToWalk = IS_DEV ? LOCAL_SUNNY_PATH : REMOTE_SUNNY_PATH;
  const files = await walk(pathToWalk);

  const sunnyFiles = files.map((file: string) =>
    getSeasonAndEpisodeFromPath(file)
  );
  await insertSunnyImages(sunnyFiles);
}
