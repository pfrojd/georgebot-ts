export interface Cage {
  id: number;
  alt: string;
  path: string;
  original_url: string;
  filename: string;
  width: number;
  height: number;
}
