export interface CommandLog {
  id: number;
  discord_message_id: string;
  discord_user_id: string;
  discord_username: string;
  command: string;
  command_timestamp: Date;
  message_content: string;
}
