export interface Fact {
  id: number;
  content: string;
  message: string;
  date: Date;
  type: string;
}
