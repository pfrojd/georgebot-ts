export interface Game {
  id: number;
  created_at: Date;
  deleted_at: Date;
  updated_at: Date;
  name: string;
  steam_id: number;
  price_formatted: string;
  is_free: boolean;
  release_date: Date;
  is_released: boolean;
  data: any;
  played_at: Date;
  played: boolean;
  slug?: string;
  recommendations?: number;
  metacritic?: number;
}

export interface GameFilter {
  categories?: string[];
  genres?: string[];
}
