export interface HallOfFame {
  id: number;
  created_at: Date;
  discord_id: string;
  discord_username: string;
  path?: string;
  url?: string;
  type: HallOfFameType;
  quote_content: string;
  original_url: string;
}

export enum HallOfFameType {
  Cursed = 1,
  Image = 2,
  Meme = 3
}
