export interface TalismanImage {
  id: number;
  description: string;
  data: Buffer;
  created_at: Date;
  deleted_at?: Date;
  path: string;
}
