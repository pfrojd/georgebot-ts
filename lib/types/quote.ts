export interface Quote {
  id: number;
  content: string;
  created_at: Date;
  deleted_at?: Date;
  context_id?: number;
  views: number;

  // Extras
  formattedTime?: string;
}

export interface InspirationalQuote {
  quote: string;
  author: string;
  slug_quote: string;
  slug_author: string;
}
