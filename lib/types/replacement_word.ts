export interface ReplacementWord {
  id: number;
  word: string;
  deleted_at: Date;
  discord_id: string;
  discord_username: string;
}
