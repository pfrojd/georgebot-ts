export interface Screenshot {
  id: number;
  discord_id: string;
  discord_username: string;
  path: string;
  comment: string;
  width: number;
  height: number;
  imported: boolean;
  original_url: string;
  enabled: boolean;
}
