declare module "slonik" {
  interface SqlTaggedTemplateType {
    binary: (data: Buffer) => BinarySqlTokenType;
  }
}

export {};
