export interface Genre {
  id: string;
  description: string;
}

export interface Mp4 {
  480: string;
  max: string;
}

export interface Webm {
  480: string;
  max: string;
}

export interface Movie {
  id: number;
  mp4: Mp4;
  name: string;
  webm: Webm;
  highlight: boolean;
  thumbnail: string;
}

export interface Platforms {
  mac: boolean;
  linux: boolean;
  windows: boolean;
}

export interface Category {
  id: number;
  description: string;
}

export interface Screenshot {
  id: number;
  path_full: string;
  path_thumbnail: string;
}

export interface Highlighted {
  name: string;
  path: string;
}

export interface Achievements {
  total: number;
  highlighted: Highlighted[];
}

export interface ReleaseDate {
  date: string;
  coming_soon: boolean;
}

export interface SupportInfo {
  url: string;
  email: string;
}

export interface Sub {
  packageid: number;
  option_text: string;
  is_free_license: boolean;
  percent_savings: number;
  option_description: string;
  can_get_free_license: string;
  percent_savings_text: string;
  price_in_cents_with_discount: number;
}

export interface PackageGroup {
  name: string;
  subs: Sub[];
  title: string;
  save_text: string;
  description: string;
  display_type: number;
  selection_text: string;
  is_recurring_subscription: string;
}

export interface PriceOverview {
  final: number;
  initial: number;
  currency: string;
  final_formatted: string;
  discount_percent: number;
  initial_formatted: string;
}

export interface PcRequirements {
  minimum: string;
  recommended: string;
}

export interface Recommendations {
  total: number;
}

export interface ContentDescriptors {
  ids: any[];
  notes?: any;
}

export interface SteamGame {
  dlc: number[];
  name: string;
  type: string;
  genres: Genre[];
  movies: Movie[];
  is_free: boolean;
  website: string;
  packages: number[];
  platforms: Platforms;
  background: string;
  categories: Category[];
  developers: string[];
  publishers: string[];
  screenshots: Screenshot[];
  steam_appid: number;
  achievements: Achievements;
  header_image: string;
  legal_notice: string;
  release_date: ReleaseDate;
  required_age: number;
  support_info: SupportInfo;
  about_the_game: string;
  package_groups: PackageGroup[];
  price_overview: PriceOverview;
  pc_requirements: PcRequirements;
  recommendations: Recommendations;
  mac_requirements: any[];
  short_description: string;
  linux_requirements: any[];
  content_descriptors: ContentDescriptors;
  supported_languages: string;
  detailed_description: string;
}
