export interface SteamLibraryEntry {
  steam_id: number;
  name: string;
  updated_at: Date;
}

export interface SteamLibraryAPIResponse {
  appid: number;
  name: string;
}
