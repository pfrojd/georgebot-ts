export interface Stock {
  id: number;
  discord_id: string;
  discord_username: string;
  symbol: string;
  created_at: Date;
  updated_at: Date;
  data: any;
}
