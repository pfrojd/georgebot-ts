export interface SunnyImage {
  id?: number;
  season: number;
  episode: number;
  path: string;
}

export interface SunnyQuote {
  id?: number;
  season: number;
  episode: number;
  content: string;
}
