export interface Token {
  id: number;
  last_updated: Date;
  token: string;
  comment: string;
  active: boolean;
  usage: number;
}
