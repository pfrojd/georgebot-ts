declare module 'wordpos' {
  export = WordPOS;

  interface WordPOSResult {
    nouns: string[];
    verbs: string[];
    adjectives: string[];
    adverbs: string[];
    rest: string[];
  }

  interface WordPOSLookupResult {
    synsetOffset: string;
    lexFilenum: number;
    lexName: string;
    pos: string;
    wCnt: number;
    lemma: string;
    synonyms: string[];
    lexId: string;
    ptrs: WordPOSPointer[];
    gloss: string;
    def: string;
  }

  interface WordPOSOptions {
    startsWith: string;
    count: number;
  }

  interface WordPOSPointer {
    pointerSymbol: string;
    synsetOffset: string;
    pos: string;
    sourceTarget: string;
  }

  class WordPOS {
    constructor();
    getPOS(text: string, callback?: (result: WordPOSResult) =>  void) : Promise<WordPOSResult>;
    isNoun(word: string, callback?: (result: boolean, word: string) => void) : Promise<boolean>;
    isVerb(word: string, callback?: (result: boolean, word: string) => void) : Promise<boolean>;
    isAdjective(word: string, callback?: (result: boolean, word: string) => void) : Promise<boolean>;
    isAdverb(word: string, callback?: (result: boolean, word: string) => void) : Promise<boolean>;
    lookup(str: string, callback?: (noun: WordPOSLookupResult[]) => void) : Promise<WordPOSLookupResult[]>;
    lookupVerb(str: string, callback?: (verb: WordPOSLookupResult[]) => void) : Promise<WordPOSLookupResult[]>;
    lookupNoun(str: string, callback?: (noun: WordPOSLookupResult[]) => void) : Promise<WordPOSLookupResult[]>;
    lookupAdverb(str: string, callback?: (adverb: WordPOSLookupResult[]) => void) : Promise<WordPOSLookupResult[]>;
    lookupAdjective(str: string, callback?: (adjective: WordPOSLookupResult[]) => void) : Promise<WordPOSLookupResult[]>;
    rand(options?: WordPOSOptions, callback?: (randomResult: string[], startsWith: string) => void) : Promise<string[]>;
    randNoun(options?: WordPOSOptions, callback?: (noun: string[], startsWith: string) => void) : Promise<string[]>;
    randVerb(options?: WordPOSOptions, callback?: (verb: string[], startsWith: string) => void) : Promise<string[]>;
    randAdjective(options?: WordPOSOptions, callback?: (adjective: string[], startsWith: string) => void) : Promise<string[]>;
    randAdverb(options?: WordPOSOptions, callback?: (adverb: string[], startsWith: string) => void) : Promise<string[]>;
    seek(offset: number, pos: string, callback?: (seekResult: WordPOSLookupResult[]) => void) : Promise<WordPOSLookupResult[]>;
  }
}
