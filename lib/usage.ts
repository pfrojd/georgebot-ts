import * as Discord from "discord.js";
import { sql } from "slonik";
import { insertCommandLog } from "./db/command_log";

const IS_DEV = process.env.IS_DEV ? true : false;

export async function logRepeatedMessage(
  currentMessage: Discord.Message,
  repeatedMessage: Discord.Message,
  command: string
): Promise<void> {
  if (IS_DEV) {
    return;
  }

  try {
    const { id, createdAt, author } = currentMessage;
    const { content } = repeatedMessage;
    await insertCommandLog({
      command: command,
      discord_message_id: id,
      discord_user_id: author.id,
      discord_username: author.username,
      command_timestamp: createdAt,
      message_content: content,
    });
  } catch (e) {
    console.error(e);
    console.error("Failed to add command log at");
  }
}

export async function logUsage(
  message: Discord.Message,
  command: string
): Promise<void> {
  if (IS_DEV) {
    return;
  }

  try {
    const { id, author, createdAt, content } = message;
    await insertCommandLog({
      command: command,
      discord_message_id: id,
      discord_user_id: author.id,
      discord_username: author.username,
      command_timestamp: createdAt,
      message_content: content,
    });
  } catch (e) {
    console.error(e);
    console.error("Failed to add command log at");
  }
}

const dataToCollect = sql`
-- Count commands per user
select command, count(command) as count
from command_log
where discord_username = 'zaifon'
group by command
order by count desc

-- commands per month
select date_trunc('month', command_log.command_timestamp), count(command) as count
from command_log
group by date_trunc('month', command_log.command_timestamp)
order by date_trunc desc

-- total commands per weekday for user
select day_name(extract(isodow from cl.command_timestamp)) as dayofweek, count(cl.command) as count
from command_log cl
where cl.discord_user_id = 118486265690718215
group by extract(isodow from cl.command_timestamp)
order by extract(isodow from cl.command_timestamp) asc

-- try to extract users favourite meme
select unnest(case when values <> '{}' then values ELSE '{-}' END) as meme_value, count(*)
from command_log cl,
regexp_match(cl.message_content, '(\d+)') as values
WHERE cl.command = 'meme'
and cl.discord_user_id = 138307196541730816
group by meme_value
order by count

-- number of screenshots contributed
select s.discord_username, count(*)
from screenshot s
where extract(year from s.created_at) = 2023
group by s.discord_username


-- try to extract users favourite talisman
select unnest(case when values <> '{}' then values ELSE '{}' END) as meme_value, count(*)
from command_log cl,
regexp_match(cl.message_content, '(\d+)') as values
WHERE cl.command = 'talisman'
AND cl.discord_user_id = 138307196541730816
group by meme_value
order by count desc
limit 10

-- extract most popular talisman this year
select unnest(case when values <> '{}' then values ELSE '{}' END) as meme_value, count(*)
from command_log cl,
regexp_match(cl.message_content, '(\d+)') as values
WHERE cl.command = 'talisman'
and extract(year from cl.command_timestamp) = 2023
group by meme_value
order by count desc
limit 10

-- number of quotes created this year
select count(*)
from quote q
where extract(year from q.created_at) = 2023

-- total numbers of quotes
select count(*), round(count(*) / extract(days from now() - (select created_at from quote order by id asc limit 1))::numeric, 2) quotesPerDay, (select created_at from quote order by id asc limit 1) since
from quote q


-- number of times required inspiration
select cl.command, count(*)
from command_log cl
where cl.command in ('theycallme', 'whatbuild', 'wontlikethis')
group by cl.command


`;

const done = sql`
CREATE OR REPLACE FUNCTION day_name(day_index double precision)
RETURNS text LANGUAGE sql STRICT IMMUTABLE PARALLEL SAFE AS $$
  SELECT CASE day_index
    WHEN 1 THEN 'Monday'
    WHEN 2 THEN 'Tuesday'
    WHEN 3 THEN 'Wednesday'
    WHEN 4 THEN 'Thursday'
    WHEN 5 THEN 'Friday'
    WHEN 6 THEN 'Saturday'
    WHEN 7 THEN 'Sunday'
    END;
$$;

select days_since_command('chegg')

CREATE OR REPLACE FUNCTION days_since_command(command_string text)
RETURNS timestamp without time zone LANGUAGE sql STRICT IMMUTABLE PARALLEL SAFE AS $$
  select command_timestamp from command_log where command = command_string order by id limit 1
$$;

-- total number of screenshots
select count(*), round(count(*) / extract(days from now() - (select created_at from screenshot order by id asc limit 1))::numeric, 2) screenshotsperday,  (select created_at from screenshot order by id asc limit 1) since
from screenshot s

-- calculate amount of events seen
select count(*), (select count(*) from fact where type = 'EVENT') as numberofevents, round(count(*) / (select count(*) from fact where type = 'EVENT')::numeric, 2) * 100 as percenteventsseen
from command_log cl
where cl.command in ('today', 'nottoday')

-- earliest week command
select *, day_name(extract(isodow from cl.command_timestamp)), extract(hour from cl.command_timestamp)
from command_log cl
where cl.discord_user_id = 138307196541730816 and cl.command = 'week'
and extract(isodow from cl.command_timestamp) = 1
order by extract(hour from cl.command_timestamp)

-- total commands per weekday
select day_name(extract(isodow from command_log.command_timestamp)) as dayofweek, count(command) as count
from command_log
group by extract(isodow from command_log.command_timestamp)
order by extract(isodow from command_log.command_timestamp) asc

-- try to extract favourite memes
select unnest(case when values <> '{}' then values ELSE '{-}' END) as meme_value, count(*)
from command_log cl,
regexp_match(cl.message_content, '(\d+)') as values
WHERE cl.command = 'meme'
group by meme_value
order by count

-- chegg counter
select count(*), round(count(*) / extract(days from now() - days_since_command('chegg'))::numeric, 2) cheggsperday, (select command_timestamp from command_log where command = 'chegg' order by id limit 1) since
from command_log cl
where cl.command = 'chegg'

-- number of screenshots contributed over all time
select s.discord_username, count(*)
from screenshot s
group by s.discord_username

-- extract most popular talisman of all time
select unnest(case when values <> '{}' then values ELSE '{}' END) as meme_value, count(*)
from command_log cl,
regexp_match(cl.message_content, '(\d+)') as values
WHERE cl.command = 'talisman'
group by meme_value
order by count desc
limit 10
`;
