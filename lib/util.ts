import { Truthy, words } from "lodash";

import * as Random from "random-js";
const engine = Random.nodeCrypto;

export function isTruthy<T>(value: T): value is Truthy<T> {
  return !!value;
}

const stopWords = [
  "i",
  "a",
  "about",
  "after",
  "an",
  "and",
  "are",
  "as",
  "at",
  "be",
  "before",
  "by",
  "com",
  "for",
  "from",
  "how",
  "in",
  "is",
  "it",
  "of",
  "on",
  "or",
  "that",
  "the",
  "this",
  "to",
  "was",
  "what",
  "when",
  "where",
  "who",
  "will",
  "with",
  "the",
  "www",
  ":",
  "&",
  "-",
];

export function substituteWord(
  sentence: string,
  replacementWords: string[],
  separator = " "
): string {
  // console.log("Original Sentence", sentence);
  // Create a structure that we can rebuild later with our stop words.
  const wordsToSubstitute = sentence
    .split(separator)
    .map((match, i) => ({ index: i, match: match }));

  // Now get a list of all indices that can be replaced.
  const validReplacementIndices = wordsToSubstitute
    .map(findValidReplacementMatches)
    .filter((r) => r !== null);

  // To pick a random index to replace, we roll a random number between 0 and the maximum
  // number of replacement indices. We then grab a valid index with our randomly
  // rolled index.
  const randomReplacementIndex =
    validReplacementIndices[getRandom(0, validReplacementIndices.length - 1)];

  // Get a random replacement word that we will use.
  const replacementIndex = getRandom(0, replacementWords.length - 1);

  // Now we rebuild the sentence with the original sentence, and order.
  return wordsToSubstitute
    .map(({ index, match }) => {
      if (index == randomReplacementIndex) {
        return replacementWords[replacementIndex];
      }

      return match;
    })
    .join(separator);
}

export function substituteWords(
  sentence: string,
  replacementWords: string[],
  lengthPerSubstitution = 12,
  separator = " "
): string {
  // Create a structure that we can rebuild later with our stop words.
  const wordsToSubstitute = sentence
    .split(separator)
    .map((match, i) => ({ index: i, match: match }));

  // Now get a list of all indices that can be replaced.
  const validReplacementIndices = wordsToSubstitute
    .map(findValidReplacementMatches)
    .filter((r) => r !== null);

  const numberOfWordsToReplace = Math.round(
    validReplacementIndices.length / lengthPerSubstitution + 1
  );

  Array.from({ length: numberOfWordsToReplace }).map((_) => {
    const firstRoll = getRandom(0, validReplacementIndices.length - 1);
    const randomReplacementIndex = validReplacementIndices[firstRoll];

    // Get a random replacement word that we will use.
    const replacementIndex = getRandom(0, replacementWords.length - 1);

    if (randomReplacementIndex !== null) {
      wordsToSubstitute[randomReplacementIndex].match = wordsToSubstitute[
        randomReplacementIndex
      ].match.replace(/\w+/, replacementWords[replacementIndex]);
    }
  });

  // Now we rebuild the sentence with the original sentence, and order.
  return wordsToSubstitute
    .map(({ match }) => {
      return match;
    })
    .join(separator);
}

function findValidReplacementMatches({
  index,
  match,
}: {
  index: number;
  match: string;
}) {
  const trimmedMatch = match.trim();

  if (!trimmedMatch) {
    return null;
  }

  if (stopWords.includes(trimmedMatch.toLowerCase())) {
    return null;
  }

  const numberRe = new RegExp(/\d+/);
  if (numberRe.test(trimmedMatch)) {
    return null;
  }

  return index;
}

export function getRandom(min: number, max: number): number {
  const rnd = Random.integer(min, max);
  return rnd(engine);
}

export type LootLevel = "LEGENDARY" | "EPIC" | "RARE" | "MAGIC" | "COMMON";

export function getLootLevel(): LootLevel {
  const rnd = getRandom(0, 100000);

  if (rnd <= 100) {
    return "LEGENDARY";
  }

  if (rnd <= 500) {
    return "EPIC";
  }

  if (rnd <= 2000) {
    return "RARE";
  }

  if (rnd <= 5000) {
    return "MAGIC";
  }

  return "COMMON";
}

export function leadingZero(num: number): string {
  if (num > 0 && num < 10) {
    return `0${num}`;
  }

  return `${num}`;
}
