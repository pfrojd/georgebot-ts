import WordPOS from 'wordpos';
const words = new WordPOS();
const MAX_TRIES = 10;

const UNDERSCORE_RE = new RegExp(/_/, 'g');

interface WordPOSLookupResult {
  synsetOffset: string;
  lexFilenum: number;
  lexName: string;
  pos: string;
  wCnt: number;
  lemma: string;
  synonyms: string[];
  lexId: string;
  ptrs: WordPOSPointer[];
  gloss: string;
  def: string;
}

interface WordPOSPointer {
  pointerSymbol: string;
  synsetOffset: string;
  pos: string;
  sourceTarget: string;
}

export async function getNoun(): Promise<WordPOSLookupResult> {
  return new Promise(async (resolve, reject) => {
    const noun = await words.randNoun();
    const nounDefinition = await words.lookupNoun(noun[0]);

    resolve(nounDefinition[0]);
  });
}

export async function getAdjective(): Promise<WordPOSLookupResult> {
  return new Promise(async (resolve, reject) => {
    const adjective = await words.randAdjective();
    const adjectiveDefinition = await words.lookupAdjective(adjective[0]);

    resolve(adjectiveDefinition[0]);
  });
}

export function isValid(str: string): boolean {
  const DIGIT_RE = new RegExp(/\d{1,3}/, 'g');
  const valid = true;
  if (str.length <= 3) {
    return false;
  }

  if (DIGIT_RE.test(str)) {
    return false;
  }

  return valid;
}

export async function getValidNoun() : Promise<WordPOSLookupResult> {
  return new Promise(async (resolve, reject) => {
    let tries = 0;
    let noun;
    while (tries <= MAX_TRIES) {
      noun = await getNoun();
      tries++;
      if (isValid(noun.lemma)) {
        break;
      }
    }

    if (!noun) {
      return reject("Could not find a valid noun");
    }

    noun.lemma = noun.lemma.replace(UNDERSCORE_RE, ' ');

    resolve(noun);
  });
}

export async function getValidAdjective()  : Promise<WordPOSLookupResult> {
  return new Promise(async (resolve, reject) => {
    let tries = 0;
    let adj;
    while (tries <= MAX_TRIES) {
      adj = await getAdjective();
      tries++;
      if (isValid(adj.lemma)) {
        break;
      }
    }

    if (!adj) {
      return reject("Could not find a valid adjective");
    }
    adj.lemma = adj.lemma.replace(UNDERSCORE_RE, ' ');
    resolve(adj);
  });
}

export async function getDefinition(args: string) : Promise<WordPOSLookupResult[]> {
  return words.lookup(args);
}

export const getGlossaryDefinition = (noun: WordPOSLookupResult, adj: WordPOSLookupResult): string => {
  return `
  > **${noun.lemma}**: ${noun.gloss}
  > **${adj.lemma}**: ${adj.gloss}
  `;
};

export const getGlossaryDefinitions = (noun: WordPOSLookupResult[], adj: WordPOSLookupResult[]) => {
  const nouns = noun.map(n => `> **${n.lemma}**: ${n.gloss}`)
  const adjectives = adj.map(a => `> **${a.lemma}**: ${a.gloss}`)

  return adjectives.concat(nouns).join('\n');
}

export const getDefinitionFormatting = (defs: WordPOSLookupResult[]) => {
  return defs.map(def => `> **${def.lemma}** (${def.pos}): ${def.gloss}`);
};
