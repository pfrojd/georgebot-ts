import * as dotenv from "dotenv";
dotenv.config();

import { createCursedCanvas } from "../lib/canvas/server";
import { getQuoteById } from "../lib/db/quote";
import { getAllScreenshots, getScreenshotById } from "../lib/db/screenshots";

(async function () {
  let numberOfImages = 0;
  const screenshot = await getScreenshotById(38);
  if (!screenshot) {
    console.error("Failed to get screenshot");
    process.exit(0);
  }
  const screenshots = await getAllScreenshots();
  // const screenshots = [screenshot];

  const quote = await getQuoteById(58);
  if (!quote) {
    console.error("Failed to get quote");
    process.exit(0);
  }
  for (const scr of screenshots) {
    numberOfImages++;
    console.log("Attempting to create canvas of screenshot", scr.id);
    console.log("amount of images", numberOfImages);

    try {
      await createCursedCanvas(quote, scr);
    } catch (e) {
      console.error(e);
    }
  }
})();
