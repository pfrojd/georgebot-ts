import * as dotenv from "dotenv";
dotenv.config();
import fs from "fs";
import path from "path";
import { fetchImage } from "../lib/image";
import { removeScreenshot, writeFileToDisk } from "../bot/attachment";
import { getAllScreenshots, insertScreenshot } from "../lib/db/screenshots";
import _ from "lodash";

const { readFile } = fs.promises;

const sleep = (delay: any) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, delay);
  });
};

(async () => {
  const screenshots = await getAllScreenshots();

  const imported_screenshots = screenshots.map(s => {
    if (s.imported) {
      return s;
    }
    return null;
  }).filter(Boolean);

  console.log('Removing', imported_screenshots.length, 'imported screenshots')

  for (const screenshot of imported_screenshots) {
    try {
      if (screenshot)
        await removeScreenshot(screenshot.path);
    } catch (e) {
      console.error("Failed to clean up screenshots");
    }
  }

})();
