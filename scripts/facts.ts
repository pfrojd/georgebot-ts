import * as dotenv from "dotenv";
dotenv.config();

import wiki from "wikijs";
import eachDayOfInterval from "date-fns/eachDayOfInterval";
import setHours from "date-fns/setHours";
import setYear from "date-fns/setYear";
import format from "date-fns/format";
import { InsertFactProps, insertFacts } from "../lib/db/fact";
import _ from "lodash";

const TYPES = {
  EVENTS: "EVENT",
  DEATHS: "DEATH",
  BIRTHS: "BIRTH",
};

const START_OF_YEAR = new Date(2020, 0, 1);
const END_OF_YEAR = new Date(2020, 0, 31);

const sleep = (delay: any) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, delay);
  });
};

function checkForLeapYearDay(date: Date) {
  if (date.getMonth() === 1 && date.getDay() === 29) {
    console.log("fixing leap day");
    date.setDate(28);
  }
  return date;
}

function getWikipediaCompatibleDateString(date: Date) {
  return `${date.toLocaleString("en", {
    month: "long",
  })} ${date.getDate()}`;
}

function parseYear(line: string) {
  const re = new RegExp(/(\d*).*–/g);
  const result = re.exec(line);
  if (result) {
    return Number(result[1]);
  } else {
    return Number("0000");
  }
}

function getBirths(sections: any, actualDate: Date) {
  const births = sections.filter((s: any) => s.title === "Births")[0];
  const rawBirths = births.content.split("\n");
  return rawBirths.map((r: string) => {
    const year = parseYear(r);
    let date = setYear(actualDate, year);
    date = setHours(date, 12);
    return {
      type: TYPES.BIRTHS,
      content: r,
      message: `${r} was born`,
      date: format(checkForLeapYearDay(date), "yyyyMMdd"),
    };
  });
}

function getDeaths(sections: any, actualDate: Date) {
  const deaths = sections.filter((s: any) => s.title === "Deaths")[0];
  const rawDeaths = deaths.content.split("\n");
  return rawDeaths.map((r: string) => {
    const year = parseYear(r);
    let date = setYear(actualDate, year);
    date = setHours(date, 12);
    return {
      type: TYPES.DEATHS,
      content: r,
      message: `${r} died`,
      date: format(checkForLeapYearDay(date), "yyyyMMdd"),
    };
  });
}

function getEvents(sections: any, actualDate: Date) {
  const rawEvents = sections.filter((s: any) => s.title === "Events")[0];
  let events: any = [];

  if ("items" in rawEvents) {
    for (const item of rawEvents.items) {
      const _rawEvents = item.content.split("\n");
      events = events.concat(
        _rawEvents.map((re: any) => {
          const year = parseYear(re);
          let date = setYear(actualDate, year);
          date = setHours(date, 12);
          return {
            type: TYPES.EVENTS,
            content: re,
            message: `${re}`,
            date: format(checkForLeapYearDay(date), "yyyyMMdd"),
          };
        })
      );
    }
  } else {
    const _rawEvents = rawEvents.content.split("\n");
    events = events.concat(
      _rawEvents.map((re: any) => {
        const year = parseYear(re);
        let date = setYear(actualDate, year);
        date = setHours(date, 12);
        return {
          type: TYPES.EVENTS,
          content: re,
          message: `${re}`,
          date: format(checkForLeapYearDay(date), "yyyyMMdd"),
        };
      })
    );
  }

  return events;
}

async function getFactsFromWikipedia(dateString: string, actualDate: Date) {
  try {
    const searchResult = await wiki().search(dateString, 1);
    const page = await wiki().page(searchResult.results[0]);
    // @ts-ignore
    const sections = await page.sections();
    const parsedBirths = getBirths(sections, actualDate);
    const parsedDeaths = getDeaths(sections, actualDate);
    const parsedEvents = getEvents(sections, actualDate);

    const all = [].concat.apply([], [parsedBirths, parsedDeaths, parsedEvents]);

    const chunks = _.chunk(all, 500);

    for (const chunk of chunks) {
      await insertFacts(chunk as InsertFactProps[]);
    }
    await sleep(3000);
    console.log("Finished with day ", actualDate.toString());
  } catch (e) {
    console.error(e);
    process.exit(0);
  }
}

async function iteratePerDay() {
  const days = eachDayOfInterval({ start: START_OF_YEAR, end: END_OF_YEAR });
  for (const day of days) {
    const date = getWikipediaCompatibleDateString(day);
    await getFactsFromWikipedia(date, day);
  }
}

(async function () {
  await iteratePerDay();
})();
