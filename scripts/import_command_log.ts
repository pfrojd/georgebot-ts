import * as dotenv from "dotenv";
dotenv.config();

import * as Discord from "discord.js";
import _ from "lodash";
import fs from "fs";
import path from "path";
import {
  insertCommandLog,
  insertCommandLogs,
  InsertCommandLogProps,
  getAllCommandLogs,
} from "../lib/db/command_log";
const { readFile, writeFile } = fs.promises;

const COMMAND = {
  RANDOM: "random",
  QUOTE: "quote",
  IMAGE: "image",
  MEME: "oldmeme",
  REMOVE: "remove",
  LATEST: "latest",
  HELP: "help",
  EDIT: "edit",
  TALISMAN: "talisman",
  WONT_LIKE_THIS: "wontlikethis",
  THEY_CALL_ME: "theycallme",
  ROLL: "roll",
  DEFINE: "def",
  CURSED: "cursed",
  TODAY: "today",
  SFX: "sfx",
  WHAT_BUILD: "whatbuild",
  TOKEN: "token",
  STOCK: "stock",
  CHEGG: "chegg",
  WHOWROTE: "whowrote",
  INSPIRATIONAL: "inspirational",
  SONIC: "sonic",
  SUNNY: "sunny",
  WHAT_A_WEEK: "week",
  CAGE: "cage",
  CURSED_MEME: "meme",
  ASK: "ask",
};

const availableCommands = Object.values(COMMAND);

export function isCommand(content: string) {
  const found = availableCommands
    .map((cmd) => {
      if (content.startsWith(`!${cmd}`)) {
        return { command: true, content: content, cmd: cmd };
      }

      return { command: false, content: null, cmd: null };
    })
    .find((f) => f.command == true);

  if (found) {
    return found;
  }
}

(async function () {
  // const pathToFile = path.resolve(
  //   process.cwd(),
  //   "data",
  //   "tmp",
  //   "data_dump.json"
  // );
  // const file = await readFile(pathToFile, "utf-8");
  // const blob = JSON.parse(file);
  // const messages = blob["messages"];

  // const filtered: InsertCommandLogProps[] = messages
  //   .map((message: any) => {
  //     const { content } = message;
  //     const cmd = isCommand(content);
  //     if (cmd && cmd.command == true) {
  //       const { id, author, content } = message;
  //       const createdAt = new Date(message["timestamp"]);
  //       return {
  //         command: cmd.cmd,
  //         discord_message_id: id,
  //         discord_user_id: author.id,
  //         discord_username: author.nickname,
  //         command_timestamp: createdAt,
  //         message_content: content,
  //       } as InsertCommandLogProps;
  //     }

  //     return null;
  //   })
  //   .filter(Boolean);

  // const chunks = _.chunk(filtered, 500);

  // for (const chunk of chunks) {
  //   await insertCommandLogs(chunk);
  // }
  const logs = await getAllCommandLogs();
  console.log(logs[0]);
})();
