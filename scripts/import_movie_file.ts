import * as dotenv from "dotenv";
dotenv.config();
import fs from "fs";
import path from "path";
import _ from "lodash";
import { getMovieByName } from "../lib/omdb";
import {
  addMovie,
  getAllMovies,
  updateRatingsOnMovie,
  updateYearOnMovie,
} from "../lib/db/movie";
import copy from "copy-to-clipboard";
import { writeFileToDisk } from "bot/attachment";

const { readFile, writeFile } = fs.promises;

const sleep = (delay: any) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, delay);
  });
};

const camelizeKeys = (obj: any): any => {
  if (Array.isArray(obj)) {
    return obj.map((v) => camelizeKeys(v));
  } else if (obj !== null && obj.constructor === Object) {
    return Object.keys(obj).reduce(
      (result, key) => ({
        ...result,
        [_.camelCase(key)]: camelizeKeys(obj[key]),
      }),
      {}
    );
  }
  return obj;
};

// (async () => {
//   const pathToFile = path.resolve(process.cwd(), "data", "movies.json");
//   let movieFile = await readFile(pathToFile, "utf-8");

//   for (let line of movieFile.split("\n")) {
//     let re = new RegExp(/\*\s(.*)\s-\s(.*)\/.*/);
//     const match = re.exec(line);
//     console.log(match);
//     if (match) {
//       let movieTitle = match[1];
//       let movieRating = Number.parseFloat(match[2]);
//       let movieData = await getMovieByName(movieTitle);
//       let modifiedMovie = camelizeKeys(movieData);
//       if (modifiedMovie?.imdbId) {
//         await addMovie({
//           omdb_id: modifiedMovie?.imdbId,
//           rating: movieRating * 10,
//           data: modifiedMovie,
//         });
//         console.log(`Added ${movieTitle} to database`);
//       } else {
//         console.error("Could not find movie for title", movieTitle);
//       }
//       await sleep(500);
//     }
//   }
// })();

(async () => {
  const movies = await getAllMovies();
  for (const movie of movies) {
    await updateYearOnMovie(movie);
  }
})();
