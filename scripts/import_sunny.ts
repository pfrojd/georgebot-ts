import * as dotenv from "dotenv";
dotenv.config();
import { insertSunnyFiles } from "../lib/sunny";

(async function () {
  await insertSunnyFiles();
})();
