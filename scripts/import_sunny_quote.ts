import * as dotenv from "dotenv";
dotenv.config();

import { promises } from "fs";
import { InsertSunnyQuoteProps, insertSunnyQuotes } from "../lib/db/sunny";
const { readFile } = promises;

(async function () {
  const json = await readFile("scripts/sunny-quotes.json", {
    encoding: "utf-8",
  });

  const sunnyQuotes: InsertSunnyQuoteProps[] = JSON.parse(json);
  await insertSunnyQuotes(sunnyQuotes);
})();
