import * as dotenv from "dotenv";
dotenv.config();
import fs from "fs";
import path from "path";
import axios from 'axios';
import _ from "lodash";
import { insertInspirationalQuote } from "../lib/db/quote";

const { readFile, writeFile } = fs.promises;

const sleep = (delay: any) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, delay);
  });
};

const tags: string[] = [
  'Beauty',
  'Dating',
  'Design',
  'Anger',
  'Diet',
  'Attitude',
  'Business',
  'Birthday',
  'Alone',
  'Age',
  'Dreams',
  'Equality',
  'Environmental',
  'Experience',
  'Failure',
  'Faith',
  'Fitness',
  'Anniversary',
  'Dad',
  'Finance',
  'Courage',
  'Fear',
  'Christmas',
  'Freedom',
  'Change',
  'Death',
  'Funny',
  'Cool',
  'Forgiveness',
  'Chance',
  'Communication',
  'Future',
  'Graduation',
  'God',
  'Government',
  'History',
  'Happiness',
  'Hope',
  'Health',
  'Home',
  'Imagination',
  'Independence',
  'Learning',
  'Inspirational',
  'Intelligence',
  'Leadership',
  'Jealousy',
  'Knowledge',
  'Music',
  'Poetry'
];

export async function getInspirationalQuotesByTag(tag: string) {
  return await axios.post('https://gapicf.reportdark.com/graphql', {
    operationName: 'GET_QUOTES_BY_TAG',
    variables: {
      "slug_tag": tag.toLocaleLowerCase(),
    },
    query: "query GET_QUOTES_BY_TAG($slug_tag: String!) {\n  quotedark_tags(where: {slug_tag: {_eq: $slug_tag}}, limit: 1) {\n    tag\n    quotes_jsonb_array\n    slug_tag\n    __typename\n  }\n}\n"
  })
}

export async function getInspirationalQuotes() {
  return await axios.post('https://gapicf.reportdark.com/graphql', {
    operationName: 'GET_SEARCH',
    variables: {
      "searchString": "%%",
    },
    query:"query GET_SEARCH($searchString: String!) {\n  quotedark_search(limit: 200000, where: {search_text: {_ilike: $searchString}}) {\n    result_type\n    search_slug\n    search_text\n    __typename\n  }\n}\n"
  })
}

export function getQuotesFromRequest(request: any) {
  return request.data.quotedark_tags[0].quotes_jsonb_array;
}

(async () => {
  try {
    const uniqueQuotes = new Map();
    for (const tag of tags) {

      const rawFile = await readFile(path.resolve(process.cwd(), "data", "inspirational", `${tag}.json`), "utf-8");
      const currentQuotes = JSON.parse(rawFile);
      for (const quote of getQuotesFromRequest(currentQuotes)) {
        if (!uniqueQuotes.has(quote.slug_quote)) {
          uniqueQuotes.set(quote.slug_quote, quote);
        }
      }
    }

    const array = Array.from(uniqueQuotes.values());
    let chunksFinished = 0;

    const chunks = _.chunk(array, 1000);
    for (const chunk of chunks) {
      await insertInspirationalQuote(chunk);
      chunksFinished++;
      console.log(`Chunk ${chunksFinished} of ${chunks.length} finished`);
    }


  } catch (e) {
    console.error(e);
  }

})();
