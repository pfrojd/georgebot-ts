import * as dotenv from "dotenv";
dotenv.config();
import * as Discord from "discord.js";
import got from "got";
import { URLSearchParams } from "url";
import fs from "fs";
import path from "path";
import next from "next";
import _ from "lodash";

const { writeFile } = fs.promises;

const btoa = (str: string) => Buffer.from(str, "binary").toString("base64");

const sleep = (delay: any) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, delay);
  });
};

const baseURL = "https://discord.com/api/v6";
const client_id = process.env.DISCORD_CLIENT_ID || "";
const client_secret = process.env.DISCORD_CLIENT_SECRET || "";
const channel_id = "183332758809083904";
const starting_point = "183338511959719936";
const exempted_users = ["GeorgeBotts"];
// let end_date = new Date(2020, 7, 16);
const end_date = new Date();

const temp_token =
  "mfa.gD45Heo_WqOHLDFvgktDPRf3flmi5LLoyFTPzz1dqW8cVGW7YYSr9pCzNnlItc8O-oQxbpoVFz7AuszT7sLX";

function isMessage(messages: any): messages is Discord.Message[] {
  return messages;
}

async function getChannelMessages(
  id = "183338511959719936",
  limit = 100
): Promise<Discord.Message[] | undefined> {
  try {
    const url = `${baseURL}/channels/${channel_id}/messages?limit=${limit}&after=${id}`;
    const response = await got
      .get(url, {
        headers: {
          authorization: `${temp_token}`,
        },
      })
      .json();

    if (response && isMessage(response)) {
      return response;
    }

    return;
  } catch (e) {
    console.error(e);
  }
}

function getAttachments(messages: Discord.Message[]) {
  return messages
    .map((m) => {
      const collection = Array.from(m.attachments);
      if (collection.length > 0) {
        const attachment: any = collection[0];
        if (exempted_users.includes(m.author.username)) {
          return null;
        }
        return {
          discord_id: m.id,
          discord_username: m.author.username,
          comment: m.content,
          url: attachment.url,
          width: attachment.width || 0,
          height: attachment.height || 0,
        };
      }
      return null;
    })
    .filter(Boolean);
}

function getNextSnowflake(messages: Discord.Message[]) {
  const collection = Array.from(messages);
  return collection[0].id;
}

function checkForEnd(messages: Discord.Message[]) {
  const collection = Array.from(messages);
  const lastMessageInCollection: any = collection[0];
  const dateOfLastMessage = new Date(lastMessageInCollection.timestamp);
  return end_date < dateOfLastMessage;
}

async function getChannelMessagesAfter(snowflake: string) {
  return await getChannelMessages(snowflake);
}

async function writeFileToDisk(json: string) {
  const filePath = path.resolve(process.cwd(), "scripts", "attachments.json");
  await writeFile(filePath, json, "utf-8");
  return filePath;
}

function verifyUniqueness(messages: any) {
  const ids = messages.map((m: any) => m.discord_id);
  const set = new Set(ids);
  if (ids.length !== set.size) {
    return false;
  }

  return true;
}

(async () => {
  const initial = await getChannelMessages();
  let keepSearching = true;
  let tries = 0;
  let newAttachments;
  let messagesProcessed = 0;
  console.log(initial);
  // if (initial) {
  //   let attachments = getAttachments(initial);
  //   let nextSnowflake = getNextSnowflake(initial);
  //   let channelMessages;

  //   while (keepSearching && tries < 2000) {
  //     await sleep(500);

  //     channelMessages = await getChannelMessagesAfter(nextSnowflake);
  //     if (channelMessages) {
  //       newAttachments = getAttachments(channelMessages);
  //       nextSnowflake = getNextSnowflake(channelMessages);

  //       attachments = attachments.concat(newAttachments);
  //       if (checkForEnd(channelMessages)) {
  //         console.log("Bailing out, end_date reached");
  //         keepSearching = false;
  //       }
  //     }
  //     if (!verifyUniqueness(attachments)) {
  //       console.log('"There are non-unique values in the collection"');
  //       process.exit(0);
  //     }
  //     tries++;
  //     messagesProcessed += 100;
  //     console.log("Next snowflake is ", nextSnowflake);
  //     console.log(messagesProcessed, "number of messages processed");
  //   }

  //   console.log("Finished mining, writing to file");
  //   console.log("Writing ", attachments.length, "attachments to file");
  //   await writeFileToDisk(JSON.stringify(attachments));
  // }
})();
