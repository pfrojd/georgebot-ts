import * as dotenv from "dotenv";
dotenv.config();
import fs from "fs";
import path from "path";
import _ from "lodash";
import { nanoid } from "nanoid";
import { getAllImages, updateImage } from "../lib/db/talisman";

const { readFile, writeFile } = fs.promises;

const sleep = (delay: any) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, delay);
  });
};

const pathToWriteTo = path.resolve(process.cwd(), "tmp");
// /mnt/volume_ams3_01/images/talisman
// const pathToWriteTo = "/mnt/volume_ams3_01/images/talisman";

async function writeFileToDisk(fileName: string, buffer: Buffer) {
  const filePath = path.resolve(pathToWriteTo, fileName);
  await writeFile(filePath, buffer);
  return filePath;
}

(async () => {
  const images = await getAllImages();
  for (const image of images) {
    const filePath = `${nanoid()}.jpg`;
    const newFilePath = await writeFileToDisk(filePath, image.data);
    // await updateImage(image, newFilePath);
    console.log(`Successfully wrote ${image.id} to disk`);
  }
})();
