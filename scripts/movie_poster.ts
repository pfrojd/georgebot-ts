import * as dotenv from "dotenv";
dotenv.config();
import fs from "fs";
import path from "path";
import _ from "lodash";
import { getMovieByName } from "../lib/omdb";
import { addMovie, getAllMovies } from "../lib/db/movie";
import copy from "copy-to-clipboard";
import { writeFileToDisk } from "bot/attachment";
import Canvas from "canvas";
import { fetchImage } from "../lib/image";
import { closestIndexTo } from "date-fns";
import { Movie } from "../lib/types/movie";

const { readFile, writeFile } = fs.promises;

const sleep = (delay: any) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, delay);
  });
};

// (async () => {
//   let movies = await getAllMovies();
//   const postersPerRow = Math.ceil(movies.length / 6);
//   const totalColumns = Math.ceil(movies.length / postersPerRow) + 1;
//   console.log(postersPerRow, totalColumns);
//   const widthPerPoster = 300 / 2;
//   const heightPerPoster = 444 / 2;
//   let canvas = Canvas.createCanvas(
//     postersPerRow * widthPerPoster,
//     totalColumns * heightPerPoster + heightPerPoster
//   );
//   console.log(postersPerRow * widthPerPoster, totalColumns * heightPerPoster);
//   let ctx = canvas.getContext("2d");
//   let position = { x: 0, y: 0 };
//   let numberOfImages = 0;

//   for (let movie of movies) {
//     let image = new Canvas.Image();
//     let imageBuffer = await fetchImage(movie.data.poster);
//     image.src = imageBuffer;
//     if (numberOfImages % 12 == 0) {
//       position.x = 0;
//       position.y += heightPerPoster;
//     }
//     numberOfImages++;

//     ctx.drawImage(
//       image,
//       position.x,
//       position.y,
//       widthPerPoster,
//       heightPerPoster
//     );
//     position.x += widthPerPoster;
//     console.log(position.x, position.y, numberOfImages, movie.data.title);
//   }

//   let filePath = path.resolve(process.cwd(), "data", "poster.png");
//   await writeFile(filePath, canvas.toBuffer());
// })();

function getMoviesWithGenre(movies: Movie[], genre: string) {
  return movies.filter((movie) => {
    const genres = movie.data.genre.split(", ");
    const hasActionGenre = genres?.find((g: any) => g == genre);
    if (hasActionGenre) {
      return movie;
    }
    return null;
  });
}

function getMoviesWithActors(movies: Movie[], actor: string) {
  return movies.filter((movie) => {
    const actors = movie.data.actors.split(", ");
    const hasActor = actors?.find((g: any) => g == actor);
    if (hasActor) {
      return movie;
    }
    return null;
  });
}

function getAverageGeorgeRating(movies: Movie[]) {
  return (
    movies.reduce((acc, value) => {
      return acc + value.rating;
    }, 0) / movies.length
  );
}

function gatherMovieGenres(movies: Movie[]): Set<string> {
  const movieGenres = new Set<string>();
  for (const movie of movies) {
    const genres = movie.data.genre.split(", ");
    for (const genre of genres) {
      movieGenres.add(genre);
    }
  }
  return movieGenres;
}

function gatherMovieActors(movies: Movie[]): Set<string> {
  const movieActors = new Set<string>();
  for (const movie of movies) {
    const actors = movie.data.actors.split(", ");
    for (const actor of actors) {
      movieActors.add(actor);
    }
  }
  return movieActors;
}

(async () => {
  const movies = await getAllMovies();
  const movieGenres = gatherMovieGenres(movies);

  for (const genre of Array.from(movieGenres)) {
    const genreMovies = getMoviesWithGenre(movies, genre);
    const avgRating = getAverageGeorgeRating(genreMovies);
    console.log(
      `Average rating of genre: ${genre} is ${(avgRating / 10).toFixed(2)}`
    );
  }

  const movieActorRatings = [];
  const movieActors = gatherMovieActors(movies);
  for (const actor of Array.from(movieActors)) {
    const actorMovies = getMoviesWithActors(movies, actor);
    if (actorMovies.length <= 1) {
      console.log(`Actor (${actor}) was in only one movie`);
      continue;
    }
    const avgRating = getAverageGeorgeRating(actorMovies);
    movieActorRatings.push({
      actor: actor,
      avg: avgRating,
      movies: actorMovies.map((a) => a.data.title),
    });
  }

  const sorted = _.sortBy(movieActorRatings, ["avg"]);
  for (const actor of sorted) {
    console.log(
      `Average rating of actor: ${actor.actor} is ${(actor.avg / 10).toFixed(
        2
      )} (in movies: ${actor.movies.join(", ")})`
    );
  }
})();
