import * as dotenv from "dotenv";
dotenv.config();
import fs from "fs";
import path from "path";
import { fetchImage } from "../lib/image";
import { writeFileToDisk } from "../bot/attachment";
import { insertScreenshot } from "../lib/db/screenshots";
import _ from "lodash";

const { readFile } = fs.promises;

const sleep = (delay: any) => {
  return new Promise(function (resolve) {
    setTimeout(resolve, delay);
  });
};

(async () => {
  const pathToFile = path.resolve(process.cwd(), "scripts", "attachments.json");
  const fileBlob = await readFile(pathToFile, "utf-8");
  const attachments: any = JSON.parse(fileBlob);
  let currentFile = 1;

  for (const attachment of attachments) {
    const extension = path.extname(attachment.url);
    let filePath;
    let imageBuffer;

    if (![".jpg", ".jpeg", ".png"].includes(extension.toLowerCase())) {
      console.log("Skipping file with invalid extension", extension);
      continue;
    }

    try {
      imageBuffer = await fetchImage(attachment.url);
    } catch (e) {
      console.error(e);
      console.error("Failed to fetch image");
      return;
    }

    if (!imageBuffer || !attachment) {
      console.error("No imagebuffer found, fetching image probably failed");
      return;
    }

    console.log("Downloaded file", attachment.url);

    try {
      filePath = await writeFileToDisk(imageBuffer, attachment.url || "temp");
    } catch (e) {
      console.error(e);
      console.error("Failed to write image to disk");
      return;
    }

    console.log("Wrote file to disk", filePath);

    try {
      if (!filePath) {
        console.error("No filepath was found");
        return;
      }

      await insertScreenshot({
        discord_id: attachment.discord_id,
        discord_username: attachment.discord_username,
        path: filePath,
        comment: attachment.comment,
        width: attachment.width || 0,
        height: attachment.height || 0,
        original_url: attachment.url
      });
    } catch (e) {
      console.error(e);
      console.error("Failed to save screenshot to db", filePath);
    }

    console.log("Sleeping 100ms for throttling");
    console.log(`Finished saving ${currentFile} of ${attachments.length} attachments`)
    console.log('----------------');
    currentFile++;
    await sleep(100);
  }
})();
