import fs from 'fs';
import path from 'path';

const pathToFolder = path.resolve(process.cwd(), "scripts", "poe");
const rawFilePath = path.resolve(pathToFolder);
const pathToDataFolder = path.resolve(process.cwd(), "data");

const { writeFile, readFile } = fs.promises;

export async function readRawFile(fileName: string) {
  try {
    return await readFile(path.resolve(rawFilePath, `${fileName}.json`), "utf-8");
  } catch (e) {
    console.error(e);
    console.error("Failed to read raw file");
  }
}

export async function writeDataFile(fileName: string, content: string) {
  try {
    return await writeFile(path.resolve(pathToDataFolder, `${fileName}.json`), content, "utf-8");
  } catch (e) {
    console.error(e);
    console.error("Failed to read raw file");
  }
}

export async function writeAscendancyClasses() {
  const file = await readRawFile("data");
  if (file) {
    const data = JSON.parse(file);
    const content = [];
    for (const item of data.classes) {
      for (const asc of item.ascendancies) {
        content.push(asc);
      }
    }

    await writeDataFile("ascendancies", JSON.stringify(content));
  }
}

export async function writeKeystones() {
  const file = await readRawFile("data");
  if (file) {
    const data = JSON.parse(file);
    const content = [];
    for (const item of Object.values(data.nodes)) {
      if ((item as any).isKeystone) {
        content.push(item);
      }
    }

    await writeDataFile("keystones", JSON.stringify(content));
  }
}

export async function writeGems() {
  const file = await readRawFile("gems");
  if (file) {
    const data = JSON.parse(file);
    const content = [];
    for (const item of Object.values(data)) {
      if ((item as any).is_support) {
        continue;
      }
      content.push((item as any).active_skill)
    }

    await writeDataFile("gem_active_skills", JSON.stringify(content));
  }
}
