import { useSpringRef, useTransition, animated } from "@react-spring/web";
import { useEffect, useRef, useState } from "react";

export interface OneByOneProps {
  items: Array<LineItem>;
  currentOffset: number;
  onDone: () => void;
  isActive: boolean;
}

export interface LineItem {
  value: string;
  margin?: LineMargin;
}

export interface LineMargin {
  top?: number;
  right?: number;
  bottom?: number;
  left?: number;
}

function getMarginString(line: LineMargin | undefined) {
  if (!line) {
    return `0 0 0 0`;
  }

  return `${line.top || 0}px ${line.right || 0}px ${line.bottom || 0}px ${
    line.left || 0
  }px`;
}

export default function OneByOne({
  items,
  currentOffset,
  onDone,
  isActive,
}: OneByOneProps) {
  const ref = useSpringRef();
  const transitions = useTransition(currentOffset, {
    ref: ref,
    from: { opacity: 1 },
    to: { opacity: 1 },
    leave: { opacity: 1 },
  });

  useEffect(() => {
    ref.start();
    if (!isActive) {
      return;
    }

    if (currentOffset > items.length) {
      onDone();
    }
  }, [currentOffset, items]);

  return (
    <animated.div>
      {transitions((style, item) => {
        return items.map((x, i) => {
          if (i < Number(item)) {
            const line = items[i];
            return (
              <animated.div
                key={i}
                style={{
                  ...style,
                  ...{ margin: `${getMarginString(line.margin)}` },
                }}
              >
                {line.value}
              </animated.div>
            );
          }
        });
      })}
    </animated.div>
  );
}
