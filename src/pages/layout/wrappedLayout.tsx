import Head from "next/head";

const WrappedLayout = () => {
  return (
    <Head>
      <title>GeorgeBotts Wrapped 2023</title>
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Abel|Unbounded&display=swap"
      />
    </Head>
  );
};

export default WrappedLayout;
