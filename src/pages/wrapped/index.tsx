// import { getCommandsPerUser } from "../../../lib/db/wrapped";
import { Parallax, ParallaxLayer, IParallax } from "@react-spring/parallax";
import { useHotkeys } from "react-hotkeys-hook";
import React, { useCallback, useRef, useState } from "react";
import styles from "./wrapped.module.css";
import OneByOne from "src/components/oneByOne";
import WrappedLayout from "../layout/wrappedLayout";
import {
  AllCommands,
  CommandsPerUser,
  getAllCommands,
  GetCheggStats,
  getCheggStats,
  GetCommandCount,
  getCommandCount,
  getCommandsPerUser,
  GetCommandsPerWeekday,
  getCommandsPerWeekday,
  GetDailyCommands,
  getDailyCommands,
  GetDailyScreenshots,
  getDailyScreenshots,
  GetEarliestweekCommand,
  getEarliestWeekCommand,
  GetEventsNotSeen,
  getEventsNotSeen,
  GetFavouriteMeme,
  getFavouriteMemes,
  getMostPopularTalisman,
  GetPopularTalisman,
  GetScreenshotsContribution,
  getScreenshotsContribution,
  GetTalismanStatistics,
  getTalismanStats,
} from "../../../lib/db/wrapped";
import { getImageById } from "../../../lib/db/talisman";
import { format, parse } from "date-fns";
import { GetServerSideProps } from "next";
import path from "path";
import Link from "next/link";
import { Abel } from "next/font/google";

const abel = Abel({ weight: "400", subsets: ["latin"] });

interface PageProps {
  offset: number;
  gradient: string;
  component: React.ReactNode;
}

function convert(timestamp?: number, formatString = "hh:mm"): string {
  if (!timestamp) {
    throw new Error(`Bad timestamp: ${timestamp}`);
  }
  return format(parse(timestamp.toString(), "T", new Date()), formatString);
}

function convertDate(timestamp: string, formatString = "hh:mm"): string {
  return format(
    parse(timestamp.toString(), "dd/MM/yyyy", new Date()),
    formatString
  );
}

const SingleWrappedPage = ({ offset, gradient, component }: PageProps) => (
  <>
    <ParallaxLayer offset={offset} speed={0.2}>
      <div className={styles.slopeBegin}></div>
    </ParallaxLayer>
    <ParallaxLayer offset={offset} speed={0.4}>
      <div className={`${styles.slopeEnd} ${styles[gradient]}`}></div>
    </ParallaxLayer>
    <ParallaxLayer
      className={`${styles.text} ${styles.number} ${styles.textContainer}`}
      offset={offset}
      speed={0.3}
    >
      {component}
    </ParallaxLayer>
  </>
);

export interface WrappedPageProps {
  stats?: Array<CommandsPerUser>;
  allCommands?: Array<AllCommands>;
  commandCount?: GetCommandCount;
  dailyCommands?: GetDailyCommands;
  dailyScreenshots?: GetDailyScreenshots;
  perWeekday?: Array<GetCommandsPerWeekday>;
  eventsNotSeen?: GetEventsNotSeen;
  earliestWeek?: GetEarliestweekCommand;
  favouriteMemes?: Array<GetFavouriteMeme>;
  cheggStats?: GetCheggStats;
  screenshotContribution?: Array<GetScreenshotsContribution>;
  mostPopularTalisman?: Array<GetPopularTalisman>;
  talismanStats?: GetTalismanStatistics;
  talisman?: string;
}

const WrappedPage = ({
  stats,
  allCommands,
  commandCount,
  dailyCommands,
  dailyScreenshots,
  perWeekday,
  eventsNotSeen,
  earliestWeek,
  favouriteMemes,
  cheggStats,
  screenshotContribution,
  mostPopularTalisman,
  talismanStats,
  talisman,
}: WrappedPageProps) => {
  const [page, setPage] = useState(0);
  const [index, set] = useState(0);
  const parallax = useRef<IParallax>(null);

  const scroll = useCallback(
    (to: number) => {
      setPage(to);
      set(0);
      parallax?.current?.scrollTo(to);
    },
    [set, page, setPage]
  );

  const previous = useCallback(() => {
    if (index <= 0) {
      parallax?.current?.scrollTo(page - 1);
      setPage(page - 1);
    }
  }, [page, index]);

  useHotkeys(
    "ArrowLeft",
    () => {
      set((state) => state - 1);
      previous();
    },
    [index]
  );

  useHotkeys(
    "ArrowRight",
    () => {
      set((state) => state + 1);
    },
    []
  );

  if (
    !stats ||
    !allCommands ||
    !dailyCommands ||
    !dailyScreenshots ||
    !perWeekday ||
    !eventsNotSeen ||
    !earliestWeek ||
    !favouriteMemes ||
    !cheggStats ||
    !screenshotContribution ||
    !mostPopularTalisman ||
    !talismanStats ||
    !talisman
  ) {
    throw new Error("Bad data, check logs");
  }

  const favourite = favouriteMemes ? favouriteMemes[0] : {};

  return (
    <div className={`${abel.className} ${styles.root}`}>
      <Parallax
        className={styles.container}
        ref={parallax}
        pages={12}
        horizontal
      >
        <SingleWrappedPage
          offset={0}
          gradient="gradient1"
          component={
            <div className={styles.center}>
              <p>Welcome to GeorgeBotts Wrapped!</p>
              <small className={styles.small}>
                Use the left/right arrow keys to navigate through the stats.
              </small>
              <small className={styles.smaller}>
                Timings is difficult, so instead you have to do it manually
              </small>

              <button className={styles.button} onClick={() => scroll(1)}>
                Click me to start!
              </button>
            </div>
          }
        />
        <SingleWrappedPage
          offset={1}
          gradient="gradient2"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>Five years of statistics</span>
              <h1 className={styles.title}>All Five years of GeorgeBotts</h1>
              <small className={styles.subtitle}>
                Some numbers on what commands are most popular
              </small>
              <OneByOne
                isActive={page == 1}
                currentOffset={index}
                items={allCommands
                  .map((stat) => ({
                    value: `!${stat.command} - ${stat.count}`,
                  }))
                  .concat([
                    {
                      value: `For a grand total of: ${commandCount?.count} commands`,
                    },
                  ])}
                onDone={() => scroll(2)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={2}
          gradient="gradient3"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>Five years of statistics</span>
              <h1 className={styles.title}>On a daily basis</h1>
              <OneByOne
                currentOffset={index}
                isActive={page == 2}
                items={[
                  {
                    value: `${dailyCommands?.commandsperday} commands per day`,
                  },
                  {
                    value: dailyCommands
                      ? `since the very first command, on ${convert(
                          dailyCommands.since,
                          "dd/MM/yyyy"
                        )}.`
                      : "",
                  },
                  {
                    value: `${dailyScreenshots?.screenshotsperday} attached images per day`,
                    margin: {
                      top: 40,
                    },
                  },
                  {
                    value: dailyCommands
                      ? `since the very first screenshot, on ${convert(
                          dailyCommands.since,
                          "dd/MM/yyyy"
                        )}.`
                      : "",
                  },
                ]}
                onDone={() => scroll(3)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={3}
          gradient="gradient4"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>Five years of statistics</span>
              <h1 className={styles.title}>Speaking of days</h1>
              <small className={styles.subtitle}>
                Here are our commands, split over the weekdays
              </small>
              <OneByOne
                isActive={page == 3}
                currentOffset={index}
                items={perWeekday.map((day) => ({
                  value: `${day.dayofweek}- ${day.count}`,
                }))}
                onDone={() => scroll(4)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={4}
          gradient="gradient1"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>Five years of statistics</span>
              <h1 className={styles.title}>Good and bad days</h1>
              <small className={styles.subtitle}>
                I wish I had a better segue
              </small>
              <OneByOne
                currentOffset={index}
                isActive={page == 4}
                items={[
                  {
                    value: `${eventsNotSeen?.count} times have we asked for a daily event`,
                  },
                  {
                    value: `out of a staggering ${eventsNotSeen?.numberofevents} total events`,
                  },
                  {
                    value: `with my sweet math skills, this means we've only seen ${eventsNotSeen?.percenteventsseen}% out of the total`,
                    margin: { top: 20 },
                  },
                ]}
                onDone={() => scroll(5)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={5}
          gradient="gradient2"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>Five years of statistics</span>
              <h1 className={styles.title}>Good weeks?</h1>
              <small className={styles.subtitle}>
                Some weeks are good, most are bad.
              </small>
              <OneByOne
                currentOffset={index}
                isActive={page == 5}
                items={[
                  {
                    value: `the earliest !week of them all happened on a ${earliestWeek?.dayname}`,
                  },
                  {
                    value: `contributed by none other than ${earliestWeek?.username}`,
                  },
                  {
                    value: `at ${convert(
                      earliestWeek?.timestamp,
                      "HH:mm 'at' dd/MM/yyyy"
                    )}`,
                  },
                ]}
                onDone={() => scroll(6)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={6}
          gradient="gradient3"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>Five years of statistics</span>
              <h1 className={styles.title}>Memes, not dreams</h1>
              <small className={styles.subtitle}>a wise man once said.</small>
              <OneByOne
                currentOffset={index}
                isActive={page == 6}
                items={[
                  {
                    value: `Turns out the new !meme command is much appreciated`,
                  },
                  {
                    value: `with the top requested format being ${favouriteMemes[0].meme}`,
                  },
                  {
                    value: `for a total of ${favouriteMemes[0].count} times`,
                  },
                ]}
                onDone={() => scroll(7)}
              />
              {page == 6 && index > 2 && (
                <img
                  className={`${styles.sideImage} `}
                  width={600}
                  style={{ marginTop: "80px" }}
                  src="https://i.kym-cdn.com/photos/images/newsfeed/001/670/182/785.jpg"
                />
              )}
            </div>
          }
        />
        <SingleWrappedPage
          offset={7}
          gradient="gradient4"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>Five years of statistics</span>
              <h1 className={styles.title}>Moments worth saving</h1>
              <small className={styles.subtitle}>
                At least that's what I thought until someone started posting
                nsfw pictures
              </small>
              <OneByOne
                currentOffset={index}
                isActive={page == 7}
                items={[
                  {
                    value: `The long sought after leaderboards for screeenshots have arrived`,
                    margin: { bottom: 40 },
                  },
                  ...screenshotContribution.map((contrib) => ({
                    value: `${contrib.username} - ${contrib.count}`,
                  })),
                ]}
                onDone={() => scroll(8)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={8}
          gradient="gradient1"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>Five years of statistics</span>
              <h1 className={styles.title}>Year of stonks</h1>
              <OneByOne
                currentOffset={index}
                isActive={page == 8}
                items={[
                  {
                    value: `speaking of memes, !chegg has been called ${cheggStats.count} times`,
                  },
                  {
                    value: `averaging a total of ${cheggStats.cheggsperday} per day since implemented`,
                  },
                  {
                    value: `if only that increased the value of the stonks`,
                    margin: { top: 20 },
                  },
                ]}
                onDone={() => scroll(9)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={9}
          gradient="gradient2"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>Five years of statistics</span>
              <h1 className={styles.title}>Sometimes we show emotions</h1>
              <OneByOne
                currentOffset={index}
                isActive={page == 9}
                items={[
                  {
                    value: `and the best way of doing that, is reliving the anger of the boardgame`,
                  },
                  {
                    value: `averaging a total of ${talismanStats.talismansperday} per day since implemented`,
                  },
                  {
                    value: `with the first registered face taking place on ${convert(
                      talismanStats.since,
                      "dd/MM/yyyy"
                    )}`,
                  },
                  {
                    value: `for a total of ${talismanStats.count} times`,
                  },
                ]}
                onDone={() => scroll(10)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={10}
          gradient="gradient3"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>Five years of statistics</span>
              <h1 className={styles.title}>our favourite ways</h1>
              <OneByOne
                currentOffset={index}
                isActive={page == 10}
                items={[
                  {
                    value: `we do like certain talismans, but we also like randomness`,
                  },
                  {
                    value: `we've asked for a random face a total of ${
                      mostPopularTalisman.find((t) => t.meme == "-")?.count
                    } times out of a total of ${talismanStats.count}`,
                    margin: { top: 20 },
                  },
                  {
                    value: `but our specifically requested favourite was ${
                      mostPopularTalisman.find((t) => t.meme != "-")?.meme
                    }`,
                    margin: { top: 20 },
                  },
                ]}
                onDone={() => scroll(11)}
              />
              {page == 10 && index > 2 && (
                <img
                  className={`${styles.sideImage} `}
                  width={400}
                  src={talisman}
                />
              )}
            </div>
          }
        />
        <SingleWrappedPage
          offset={11}
          gradient="gradient4"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>Moving on</span>
              <h1 className={styles.title}>2023 has been a year</h1>
              <OneByOne
                currentOffset={index}
                isActive={page == 11}
                items={[
                  {
                    value: `so lets move on with the statistics`,
                  },
                  {
                    value: `from now on, you'll have to pick who you are, for your personalized stats`,
                  },
                ]}
                onDone={() => scroll(12)}
              />

              {page == 11 && index > 1 && (
                <div className={styles.users}>
                  <Link href="/wrapped/user">
                    <button className={styles.button}>
                      Click to go to personalized stats
                    </button>
                  </Link>
                </div>
              )}
            </div>
          }
        />
      </Parallax>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps<
  WrappedPageProps
> = async () => {
  try {
    const userStats = await getCommandsPerUser();
    const allCommands = await getAllCommands();
    const commandCount = await getCommandCount();
    const dailyCommands = await getDailyCommands();
    const dailyScreenshots = await getDailyScreenshots();
    const perWeekday = await getCommandsPerWeekday();
    const eventsNotSeen = await getEventsNotSeen();
    const earliestWeek = await getEarliestWeekCommand();
    const favouriteMemes = await getFavouriteMemes();
    const cheggStats = await getCheggStats();
    const screenshotContribution = await getScreenshotsContribution();
    const mostPopularTalisman = await getMostPopularTalisman();
    const talismanStats = await getTalismanStats();

    const mostPopular = mostPopularTalisman.find((t) => t.meme != "-");
    const talisman = await getImageById(Number(mostPopular?.meme));
    return {
      props: {
        stats: userStats,
        allCommands: allCommands,
        commandCount,
        dailyCommands,
        dailyScreenshots,
        perWeekday,
        eventsNotSeen,
        earliestWeek,
        favouriteMemes,
        cheggStats,
        screenshotContribution,
        mostPopularTalisman,
        talismanStats,
        talisman: `https://zaifon.dev/talisman/${path.basename(talisman.path)}`,
      },
    };
  } catch (e) {
    console.error(e);
    return {
      props: {
        stats: [],
        allCommands: [],
        perWeekday: [],
        favouriteMemes: [],
        screenshotContribution: [],
      },
    };
  }
};

WrappedPage.getLayout = function getLayout(page: any) {
  return (
    <>
      <WrappedLayout />
      {page}
    </>
  );
};

export default WrappedPage;
