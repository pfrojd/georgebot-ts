import { Parallax, ParallaxLayer, IParallax } from "@react-spring/parallax";
import { useHotkeys } from "react-hotkeys-hook";
import React, { useCallback, useEffect, useRef, useState } from "react";
import styles from "./wrapped.module.css";
import OneByOne from "src/components/oneByOne";
import WrappedLayout from "../layout/wrappedLayout";
import {
  ActiveMonthsThisYear,
  ActiveWeekdaysInThisYear,
  FavouriteTalisman,
  getActiveMonthsThisYear,
  getActiveWeekdaysInThisYear,
  getFavouriteMemesOfTheYear,
  getFavouriteTalismanOfTheYear,
  getMostActiveDayThisYear,
  getTotalCommandsPerUserThisYear,
  MostActiveDayResult,
  TotalCommandsPerUser,
} from "../../../lib/db/wrapped";
import { format, parse } from "date-fns";
import { GetServerSideProps } from "next";
import { Abel } from "next/font/google";

const abel = Abel({ weight: "400", subsets: ["latin"] });

interface PageProps {
  offset: number;
  gradient: string;
  component: React.ReactNode;
}

function convert(timestamp: number, formatString = "hh:mm"): string {
  return format(parse(timestamp.toString(), "T", new Date()), formatString);
}

function convertDate(timestamp: string, formatString = "hh:mm"): string {
  return format(
    parse(timestamp.toString(), "yyyy-MM-dd", new Date()),
    formatString
  );
}

const SingleWrappedPage = ({ offset, gradient, component }: PageProps) => (
  <>
    <ParallaxLayer offset={offset} speed={0.2}>
      <div className={styles.slopeBegin}></div>
    </ParallaxLayer>
    <ParallaxLayer offset={offset} speed={0.4}>
      <div className={`${styles.slopeEnd} ${styles[gradient]}`}></div>
    </ParallaxLayer>
    <ParallaxLayer
      className={`${styles.text} ${styles.number} ${styles.textContainer}`}
      offset={offset}
      speed={0.3}
    >
      {component}
    </ParallaxLayer>
  </>
);

export interface WrappedUserPageProps {
  commandsPerUser?: Array<TotalCommandsPerUser>;
  activeMonthsThisYear?: Array<ActiveMonthsThisYear>;
  activeWeekdaysThisYear?: Array<ActiveWeekdaysInThisYear>;
  mostActiveDayThisYear?: MostActiveDayResult;
  favouriteTalismans?: Array<FavouriteTalisman>;
  favouriteMemes?: Array<FavouriteTalisman>;
}

const WrappedUserPage = ({
  commandsPerUser,
  activeMonthsThisYear,
  activeWeekdaysThisYear,
  mostActiveDayThisYear,
  favouriteTalismans,
  favouriteMemes,
}: WrappedUserPageProps) => {
  const [page, setPage] = useState(0);
  const [index, set] = useState(0);
  const [user, setUser] = useState<{ id: string; username: string } | null>(
    null
  );
  const parallax = useRef<IParallax>(null);

  if (
    !commandsPerUser ||
    !activeMonthsThisYear ||
    !activeWeekdaysThisYear ||
    !mostActiveDayThisYear ||
    !favouriteTalismans ||
    !favouriteMemes
  ) {
    throw new Error("Bad data, check logs");
  }

  const scroll = useCallback(
    (to: number) => {
      if (page == 7) {
        return;
      }
      setPage(to);
      set(0);
      parallax?.current?.scrollTo(to);
    },
    [set, page, setPage]
  );

  const previous = useCallback(() => {
    if (index <= 0) {
      parallax?.current?.scrollTo(page - 1);
      setPage(page - 1);
    }
  }, [page, index]);

  useHotkeys(
    "ArrowLeft",
    () => {
      set((state) => state - 1);
      previous();
    },
    [index]
  );

  useHotkeys(
    "ArrowRight",
    () => {
      if (page == 7) {
        return;
      }
      set((state) => state + 1);
    },
    []
  );

  useEffect(() => {
    if (!user && page != 0) {
      parallax.current?.scrollTo(0);
      setPage(0);
    }
  }, [user, page]);

  return (
    <div className={`${abel.className} ${styles.root}`}>
      <Parallax
        className={styles.container}
        ref={parallax}
        pages={8}
        horizontal
      >
        <SingleWrappedPage
          offset={0}
          gradient="gradient1"
          component={
            <div className={styles.center}>
              <p>These are the personalized stats!</p>

              <small className={styles.small}>Pick your user</small>

              <div className={styles.users}>
                <button
                  className={styles.button}
                  onClick={() => {
                    scroll(1);
                    setUser({ id: "118486265690718215", username: "zaifon" });
                  }}
                >
                  zaifon
                </button>

                <button
                  className={styles.button}
                  onClick={() => {
                    scroll(1);
                    setUser({ id: "133328817342251010", username: "Wolfpup" });
                  }}
                >
                  Wolfpup
                </button>

                <button
                  className={styles.button}
                  onClick={() => {
                    scroll(1);
                    setUser({ id: "138307196541730816", username: "George" });
                  }}
                >
                  George
                </button>
              </div>
              <small className={styles.smaller}>
                Use the left/right arrow keys to navigate through the stats.
              </small>
            </div>
          }
        />
        <SingleWrappedPage
          offset={1}
          gradient="gradient2"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>2023 statistics for you</span>
              <span className={styles.currentUser}>{user?.username}</span>
              <h1 className={styles.title}>Your habits and favourites</h1>
              <OneByOne
                isActive={page == 1}
                currentOffset={index}
                items={[
                  {
                    value: `2023 you executed ${commandsPerUser
                      .filter((c) => c.userid == user?.id)
                      .reduce((a, b) => a + b.count, 0)} commands`,
                    margin: { top: 20, bottom: 40 },
                  },
                  {
                    value: `Here are your top 5 commands this year`,
                    margin: { bottom: 20 },
                  },
                  ...commandsPerUser
                    .filter((c) => c.userid == user?.id)
                    .sort((a, b) => b.count - a.count)
                    .slice(0, 5)
                    .map((c) => ({
                      value: `${c.command} - ${c.count}`,
                    })),
                ]}
                onDone={() => scroll(2)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={2}
          gradient="gradient3"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>2023 statistics for you</span>
              <span className={styles.currentUser}>{user?.username}</span>
              <h1 className={styles.title}>This year has been a ride</h1>
              <small className={styles.subtitle}>
                and some months require more spam than others
              </small>
              <OneByOne
                isActive={page == 2}
                currentOffset={index}
                items={[
                  {
                    value: `Here are your top 3 months this year`,
                    margin: { top: 40, bottom: 20 },
                  },
                  ...activeMonthsThisYear
                    .filter((c) => c.userid == user?.id)
                    .sort((a, b) => b.count - a.count)
                    .slice(0, 3)
                    .map((c) => ({
                      value: `${convert(c.month, "MMMM")} - ${c.count}`,
                    })),
                ]}
                onDone={() => scroll(3)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={3}
          gradient="gradient4"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>2023 statistics for you</span>
              <span className={styles.currentUser}>{user?.username}</span>
              <h1 className={styles.title}>Weekdays</h1>
              <small className={styles.subtitle}>
                ...what a bunch of bastards
              </small>
              <OneByOne
                isActive={page == 3}
                currentOffset={index}
                items={[
                  {
                    value: `Here are your top 3 weekdays this year`,
                    margin: { top: 20, bottom: 40 },
                  },
                  ...activeWeekdaysThisYear
                    .filter((c) => c.userid == user?.id)
                    .sort((a, b) => b.count - a.count)
                    .slice(0, 3)
                    .map((c) => ({
                      value: `${c.dayofweek} - ${c.count}`,
                    })),
                ]}
                onDone={() => scroll(4)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={4}
          gradient="gradient4"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>2023 statistics for you</span>
              <span className={styles.currentUser}>{user?.username}</span>
              <h1 className={styles.title}>What happened?</h1>
              <OneByOne
                isActive={page == 4}
                currentOffset={index}
                items={
                  user?.id != null
                    ? [
                        ...mostActiveDayThisYear.days
                          .filter((c) => c.userid == user?.id)
                          .sort((a, b) => b.count - a.count)
                          .slice(0, 1)
                          .map((c) => ({
                            value: `Your most active day was ${convertDate(
                              c.day,
                              "dd/MM/yyyy"
                            )} where you spammed a total of ${
                              c.count
                            } commands`,
                          })),
                        ...mostActiveDayThisYear.users[user.id]
                          .sort((a, b) => b.count - a.count)
                          .map((c) => ({ value: `${c.command} - ${c.count}` })),
                      ]
                    : []
                }
                onDone={() => scroll(5)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={5}
          gradient="gradient1"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>2023 statistics for you</span>
              <span className={styles.currentUser}>{user?.username}</span>
              <h1 className={styles.title}>
                Here are your favourite talismans
              </h1>
              <OneByOne
                isActive={page == 5}
                currentOffset={index}
                items={
                  user?.id != null
                    ? [
                        ...favouriteTalismans
                          .filter((c) => c.userid == user?.id)
                          .sort((a, b) => b.count - a.count)
                          .slice(0, 1)
                          .map((c) => ({
                            value: `Your favourite talisman was ${
                              c.meme != "-" ? c.meme : "<random>"
                            }, you requested this ${c.count} times`,
                          })),
                        {
                          value: `Followed by`,
                          margin: { top: 40, bottom: 20 },
                        },
                        ...favouriteTalismans
                          .filter((c) => c.userid == user?.id)
                          .sort((a, b) => b.count - a.count)
                          .slice(1, 4)
                          .map((c) => ({
                            value: `${c.meme == "-" ? "<random>" : c.meme} - ${
                              c.count
                            } times`,
                          })),
                      ]
                    : []
                }
                onDone={() => scroll(6)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={6}
          gradient="gradient2"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>2023 statistics for you</span>
              <span className={styles.currentUser}>{user?.username}</span>
              <h1 className={styles.title}>Here are your favourite memes</h1>
              <OneByOne
                isActive={page == 6}
                currentOffset={index}
                items={
                  user?.id != null
                    ? [
                        ...favouriteMemes
                          .filter((c) => c.userid == user?.id)
                          .sort((a, b) => b.count - a.count)
                          .slice(0, 1)
                          .map((c) => ({
                            value: `Your favourite meme template was ${
                              c.meme != "-" ? c.meme : "<random>"
                            }, you requested this ${c.count} times`,
                          })),
                        {
                          value: `Followed by`,
                          margin: { top: 40, bottom: 20 },
                        },
                        ...favouriteMemes
                          .filter((c) => c.userid == user?.id)
                          .sort((a, b) => b.count - a.count)
                          .slice(1, 4)
                          .map((c) => ({
                            value: `${c.meme == "-" ? "<random>" : c.meme} - ${
                              c.count
                            } times`,
                          })),
                      ]
                    : []
                }
                onDone={() => scroll(7)}
              />
            </div>
          }
        />
        <SingleWrappedPage
          offset={7}
          gradient="gradient3"
          component={
            <div className={styles.pageContainer}>
              <span className={styles.section}>2023 statistics for you</span>
              <span className={styles.currentUser}>{user?.username}</span>
              <h1 className={styles.title}>This is the end</h1>
              <OneByOne
                isActive={page == 7}
                currentOffset={index}
                items={[
                  { value: `Thanks for clicking through` },
                  {
                    value:
                      "Hoping next year I've learned more about statistics",
                  },
                ]}
                onDone={() => {
                  return;
                }}
              />
            </div>
          }
        />
      </Parallax>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps<
  WrappedUserPageProps
> = async () => {
  try {
    const commandsPerUser = await getTotalCommandsPerUserThisYear();
    const activeMonthsThisYear = await getActiveMonthsThisYear();
    const activeWeekdaysThisYear = await getActiveWeekdaysInThisYear();
    const mostActiveDayThisYear = await getMostActiveDayThisYear();
    const favouriteTalismans = await getFavouriteTalismanOfTheYear();
    const favouriteMemes = await getFavouriteMemesOfTheYear();

    return {
      props: {
        commandsPerUser,
        activeMonthsThisYear,
        activeWeekdaysThisYear,
        mostActiveDayThisYear,
        favouriteTalismans,
        favouriteMemes,
      },
    };
  } catch (e) {
    console.error(e);
    return {
      props: {},
    };
  }
};

WrappedUserPage.getLayout = function getLayout(page: any) {
  return (
    <>
      <WrappedLayout />
      {page}
    </>
  );
};

export default WrappedUserPage;
