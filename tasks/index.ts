import * as dotenv from "dotenv";
dotenv.config();
import * as cron from "node-cron";
import { updateSteamLibraryJob } from "./updateSteamLibrary";
import { updateStockPricesJob } from "./updateStockPrices";

const schedules = {
  UPDATE_STEAM_LIBRARY: "0 2 * * *",
  UPDATE_STOCK_PRICE: "0 * * * *",
};

const updateLibraryTask = cron.schedule(
  schedules.UPDATE_STEAM_LIBRARY,
  updateSteamLibraryJob,
  {
    scheduled: true,
  }
);

const updateStockTasks = cron.schedule(
  schedules.UPDATE_STOCK_PRICE,
  updateStockPricesJob,
  {
    scheduled: true,
  }
);

(async () => {
  try {
    updateLibraryTask.start();
    updateStockTasks.start();
  } catch (e) {
    console.error(e);
    console.error("Failed to start up cron-task");
  }
})();
