import { updateSteamLibrary } from "../lib/db/steam";

export async function updateSteamLibraryJob() {
  try {
    console.log("-- Starting updateSteamLibrary Task --");
    await updateSteamLibrary();
    console.log("-- Finished updateSteamLibrary Task --");
  } catch (e) {
    console.error(e);
    console.error("Failed to run update steam library job");
  }
}
