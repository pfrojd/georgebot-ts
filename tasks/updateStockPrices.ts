import { updateSteamLibrary } from "../lib/db/steam";

export async function updateStockPricesJob() {
  try {
    console.log(`-- Starting updateStockPricesJob Task at ${new Date().toTimeString()} --`);
    await updateSteamLibrary();
    console.log(`-- Finished updateStockPricesJob Task at ${new Date().toTimeString()} --`);
  } catch (e) {
    console.error(e);
    console.error("Failed to run updateStockPricesJob");
  }
}
